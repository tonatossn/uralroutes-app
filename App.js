import React, { useEffect, useState } from 'react';
import Constants from 'expo-constants'
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { StyleSheet, SafeAreaView, View } from 'react-native';
import { NavigationContainer, DefaultTheme, useNavigationContainerRef } from '@react-navigation/native';
import { AppearanceProvider } from 'react-native-appearance';
import { GestureHandlerRootView } from 'react-native-gesture-handler';

import { DEFAULT_COLOR, BACKGROUND_COLOR } from './src/constance/styles';
import { RootNavigator } from './src/navigation/RootNavigator';
import { store, persistore } from './src/redux/store'
import GeoWrapper from './src/containers/GeoWrapper';
import { AnimatedAppLoader } from './src/components/AnimatedAppLoader';


const UralRouteTheme = {
	...DefaultTheme,
	colors: {
	    ...DefaultTheme.colors,
		background: BACKGROUND_COLOR,
	    primary: DEFAULT_COLOR,
	},
};  

export default App = () => {
	const navigationRef = useNavigationContainerRef();
	const [ headerColored, setHeaderColored ] = useState(true);

	const checkHeaderColored = () => {
		if (navigationRef.isReady()) {
			if ( navigationRef.getCurrentRoute().name == 'IndexPage' ) {
				setHeaderColored(true);
			} else {
				setHeaderColored(false);
			}
		}
	}

	return (
		<AppearanceProvider >
			<Provider store={store}>
				<PersistGate loading={null} persistor={persistore}>
					<AnimatedAppLoader>
						<GeoWrapper>
							<GestureHandlerRootView style={{flex: 1}}>
								<View style={{
									...styles.headerStyle,
									...(headerColored ? {backgroundColor: DEFAULT_COLOR} : {})
								}}>
								</View>
								<SafeAreaView style={styles.container} >
									<NavigationContainer
										theme={UralRouteTheme}
										ref={navigationRef}
										onStateChange={checkHeaderColored}
										onReady={checkHeaderColored}
									>
										<RootNavigator />
									</NavigationContainer>
								</SafeAreaView>
							</GestureHandlerRootView>
						</GeoWrapper>
					</AnimatedAppLoader>
				</PersistGate>
			</Provider>
		</AppearanceProvider>
	);
}

const styles = StyleSheet.create({
	container: {
    	flex: 1,
	},
	headerStyle: {
		flex: 0,
		width: '100%',
		height: Constants.statusBarHeight,
	}
});

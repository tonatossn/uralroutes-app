import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { StyleSheet } from 'react-native';

import { RouteListItem } from '../components/RouteListItem';
import { SortFilterBar } from '../components/elements/SortFilterBar';
import { fetchRoutes, setRouteOrdering, setRouteFilters } from '../redux/actions/route';
import { ReloadedPaginatedFlatListWrapper } from './ReloadedPaginatedFlatListWrapper';
import { MultiSelectModal } from '../components/elements/MultiSelectModal';
import { ButtonContainerBottom } from '../components/elements/ButtonContainerBottom';

const AvailableFilter = ({ pickerShow, setPickerShow, activeFilter }) => {
    const dispatch = useDispatch();
    const availables = [
        {
            value: 'self',
            label: 'Открытые',
            description: 'Можно осмотреть объекты свободно',
        },
        {
            value: 'escort',
            label: 'Доступ с сопровождением',
            description: 'Можно попасть только с сопровождающим лицом или в составе экскурсионной группы',
        },
        {
            value: 'only_in_route',
            label: 'Только в составе маршрута',
        },
    ]
    const [selectedFilterValue, setSelectedFilterValue] = useState(availables[0].value);

    return (
        <MultiSelectModal
            items={availables}
            title={'Доступность'}
            onValueChange={value => {
                if (selectedFilterValue === value) {
                    return;
                }
                dispatch(setRouteFilters({
                    ...activeFilter,
                    'availability__in': value
                }));
                setSelectedFilterValue(value);
            }}
            status={pickerShow}
            setStatus={setPickerShow}
            selectedValues={selectedFilterValue}
        />
    )
}

export const RouteListContainer = ({ navigation, route }) => {
    const dispatch = useDispatch();
    const [pickerShow, setPickerShow] = useState(false);
    const [activeTab, setActiveTab] = useState(0);
    const activeFilter = useSelector(state => state.route.filters);
    const activeSort = useSelector(state => state.route.ordering);
    
    useEffect(() => {
        if (route.params?.tabIndex) {
            setActiveTab(route.params.tabIndex);
        }
    }, [route])

    useEffect(() => {
        dispatch(setRouteFilters({
            ...activeFilter,
            'tours__isnull': activeTab === 0 ? null : true
        }));
    }, [activeTab])

	const renderItem = ({item}) => {
        return (
            <RouteListItem
                item={item}
                navigation={navigation}
            />
        );
    };
    
    return (
        <>
            <AvailableFilter
                pickerShow={pickerShow}
                setPickerShow={setPickerShow}
                activeFilter={activeFilter}
            />
            <SortFilterBar
                sortVariants={[
                    {
                        value: 'id',
                        label: 'По умолчанию'
                    },
                    {
                        value: 'duration',
                        label: 'По длительности'
                    },
                    {
                        value: 'points__count',
                        label: 'По кол-ву мест'
                    },
                    {
                        value: 'title',
                        label: 'По названию'
                    }
                ]}
                sortAction={(value) => {
                    dispatch(setRouteOrdering(value));
                }}
                sortActive={activeSort}
                filterTitle={'Доступность'}
                filterAction={() => {
                    setPickerShow(true);
                }}
                filterActive={
                    activeFilter['availability__in'] != null
                    && activeFilter['availability__in'].length > 0
                }
            />
            <ReloadedPaginatedFlatListWrapper
                stateKey='route'
                dispatchFunction={fetchRoutes}
                renderItem={renderItem}
            />
            <ButtonContainerBottom
                activeIndex={activeTab}
                items={[
                    {
                        onPress: () => {
                            setActiveTab(0);
                        },
                        title: 'Все маршруты'
                    },
                    {
                        onPress: () => {
                            setActiveTab(1);
                        },
                        title: 'Платные туры'
                    }
                ]}
            />
        </>
    )
};

const styles = StyleSheet.create({
    availableFilter: {
        opacity: 0,
        position: 'absolute',
        left: -1000,
    },
})
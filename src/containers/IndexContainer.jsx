import React, { useContext, useEffect } from 'react';
import { ScrollView, StatusBar, StyleSheet, ImageBackground } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { ContextPoints } from '../components/ContextPoints';
import { ContextRoutes } from '../components/ContextRoutes';
import { IndexMapTeaser } from '../components/IndexMapTeaser';
import { IndexSchedule } from '../components/IndexSchedule';
import { IndexOfferPoint } from '../components/IndexOfferPoint';
import { fetchPointsBiennaleChoice, fetchPointsByDistance } from '../redux/actions/point';
import { fetchRoutesOnIndex, fetchRoutesByDate } from '../redux/actions/route';
import { GeoContext } from './GeoWrapper';
import { DEFAULT_COLOR } from '../constance/styles';


export const IndexContainer = ({ navigation }) => {
    const dispatch = useDispatch();
    const location = useContext(GeoContext);
    const backgroundImage = require('../../assets/header--bg.png');

    useEffect(() => {
        dispatch(fetchRoutesOnIndex())
        dispatch(fetchPointsBiennaleChoice())
        dispatch(fetchPointsByDistance({lat: location.latitude, lng: location.longitude}))
        dispatch(fetchRoutesByDate());
    }, [dispatch, location])

    const {
        routesOnIndex,
        routesByDate,
        pointsBiennaleChoice,
        pointsByDistance,
    } = useSelector(state => ({
        routesOnIndex: state.route.routesOnIndex,
        routesByDate: state.route.routesByDate,
        pointsBiennaleChoice: state.point.pointsBiennaleChoice,
        pointsByDistance: state.point.pointsByDistance,
    }));
    
    return (
        <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
            <ImageBackground
                source={backgroundImage}
                style={styles.backgroundImage}
                imageStyle={{
                  resizeMode: 'cover',
                  height: 400,
                  alignItems: 'flex-start',
                  top: -100
                }}
            >
                <StatusBar translucent backgroundColor={DEFAULT_COLOR} barStyle='dark-content' />
                <ContextRoutes title='Маршруты: выбор биеннале' routes={routesOnIndex.results} navigation={navigation} />
                <ContextPoints title='Места: выбор биеннале' points={pointsBiennaleChoice.results} navigation={navigation} />
                <IndexSchedule title='Платные туры' days={routesByDate.results} navigation={navigation} />
                {pointsByDistance
                    ? <ContextPoints title='Интересные места рядом с вами' points={pointsByDistance.results} navigation={navigation} />
                    : <></>
                }
                <IndexMapTeaser navigation={navigation} />
                <IndexOfferPoint navigation={navigation} />
            </ImageBackground>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
	container: {
        flex: 1,
	},
    backgroundImage: {
        width: '100%',
        height: 'auto',
        paddingTop: 20
    }
});

import * as React from 'react';
import { ScrollView } from 'react-native';
import { useSelector } from 'react-redux';

import { About } from '../components/Aboute';


export const AbouteContainer = ({ navigation }) => {
    const data = useSelector(state => state.about.data);
    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <About data={data} />
        </ScrollView>
    );
}

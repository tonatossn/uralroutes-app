import React, { useEffect } from 'react';
import { ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { ContextPoints } from '../components/ContextPoints';
import { ContextCities } from '../components/ContextCities';
import { RouteItem } from '../components/RouteItem';
import { fetchRouteItem } from '../redux/actions/route';

export const RouteItemContainer = ({ navigation, route }) => {
    const dispatch = useDispatch();
    
    useEffect(() => {
        dispatch(fetchRouteItem(route.params.id));
    }, [dispatch, route.params.id]);
    
    const item = useSelector(state => state.route.routesById[route.params.id]);
    
    return (
        item ? (
            <ScrollView showsVerticalScrollIndicator={false}>
                <RouteItem navigation={navigation} item={item} />
                <ContextPoints title='Места в маршруте' points={item.points} navigation={navigation} />
                <ContextCities title='Города маршрута' cities={item.cities} navigation={navigation} />
            </ScrollView>
        ) : null
    );
}

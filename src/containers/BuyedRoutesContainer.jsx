import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import DeviceInfo from 'react-native-device-info';

import { BuyedRoutes } from '../components/BuyedRoutes';
import { fetchTickets } from '../redux/actions/ticket';


export const BuyedRoutesContainer = ({ navigation }) => {
    const dispatch = useDispatch();
    const buyedroutes = useSelector(state => state.ticket.list);
    
    useEffect(() => {
        const devideId = DeviceInfo.getUniqueId();
        dispatch(fetchTickets(devideId))
    }, [dispatch]);

    return (
        <BuyedRoutes
            navigation={navigation}
            buyedroutes={buyedroutes.results}
        />
    );
}

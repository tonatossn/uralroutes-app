import React, { useContext, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import config from '../constance/config';
import { PointListItem } from '../components/PointListItem';
import { fetchPoints, setPointsListLimit } from '../redux/actions/point';
import { ReloadedPaginatedFlatListWrapper } from './ReloadedPaginatedFlatListWrapper';
import { GeoContext } from '../containers/GeoWrapper';
import { PointBaseWrapper } from './PointBaseWrapper';

export const PointListContainer = ({ navigation, route }) => {
    const userLocation = useContext(GeoContext);
    const dispatch = useDispatch();
    
    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            dispatch(setPointsListLimit(config.LIMIT_BY_PAGE))
        });
        return unsubscribe;
    }, [navigation]);
    
    const renderItem = ({ item }) => {
        return (
            <PointListItem
                item={item}
                navigation={navigation}
                userLocation={userLocation}
            />
        );
    };
    
    return (
        <PointBaseWrapper hasSort={true} navigation={navigation} needShadowForTags={true}>
            <ReloadedPaginatedFlatListWrapper
                stateKey='point'
                route={route}
                dispatchFunction={fetchPoints}
                additionalDispatchParams={userLocation.longitude ? {
                    point: `${userLocation.longitude},${userLocation.latitude}`
                } : {}}
                renderItem={renderItem}
            />
        </PointBaseWrapper>
    );
};

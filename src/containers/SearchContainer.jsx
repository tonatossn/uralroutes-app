import React, { useEffect, useState, useContext } from 'react';
import { View, Text, StyleSheet, FlatList, ActivityIndicator, useWindowDimensions } from 'react-native';
import SearchBar from 'react-native-platform-searchbar';
import { TabView, TabBar } from 'react-native-tab-view';
import { useDispatch } from 'react-redux';
import { Ionicons } from '@expo/vector-icons';

import DefaultStyles, { DEFAULT_COLOR } from '../constance/styles';
import { PointListItem } from '../components/PointListItem';
import { RouteListItem } from '../components/RouteListItem';
import { CityListItem } from '../components/CityListItem';
import { fetchSearchData, clearSearchData } from '../redux/actions/search';
import { GeoContext } from '../containers/GeoWrapper';
import { useSelector } from 'react-redux';


const NotFound = () => {
    return (
        <View style={styles.notFound}>
            <Ionicons name="search-outline" color={'#efefef'} size={100} />
            <Text style={styles.notFoundText}>
                К сожалению, ничего не найдено. Попробуйте смягчить условия поиска
            </Text>
        </View>
    );
}

const BaseFrameRoute = ({ navigation, data, itemComponent }) => {
    const userLocation = useContext(GeoContext);
    const notFoundList = [{id: 'notfound-404', title: 'Not found'}];
    const renderItem = ({ item }) => {
        if (data.results.length > 0) {
            return React.createElement(itemComponent, {
                key: item.id,
                item: item,
                navigation: navigation,
                userLocation: userLocation,
            });
        } else {
            return <NotFound />
        }
    }

    return (
        <FlatList
            data={data.results.length > 0 ? data.results : notFoundList}
            renderItem={renderItem}
            keyExtractor={item => item.id}
            showsVerticalScrollIndicator={false}
            style={styles.flatList}
        />
    );
}

export const SearchContainer = ({ navigation }) => {
    const dispatch = useDispatch();
    
    const [ searchValue, setSearchValue ] = useState('');
    const [ typing, setTyping ] = useState(false)

    const data = useSelector(state => state.search);
    const layout = useWindowDimensions();
    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'points', title: 'Места' },
        { key: 'routes', title: 'Маршруты'},
        { key: 'cities', title: 'Города' },
    ]);

    useEffect(() => {
        setTyping(true);
        let typingTimeout = setTimeout(() => {
            if (searchValue == '') {
                dispatch(clearSearchData())
            } else {
                dispatch(fetchSearchData(1, {search: searchValue}))
            }
            setTyping(false)
        }, 1000);
        return () => {
            clearTimeout(typingTimeout);
        };
    }, [searchValue])

    return (
        <View style={styles.container}>
            <SearchBar
                value={searchValue}
                placeholder='Поиск'
                onChangeText={setSearchValue}
                onCancel={() => setSearchValue('')}
                style={styles.searchBar}
                cancelText={'Отмена'}
            >
                {typing ? (
                    <ActivityIndicator style={{ marginRight: 10 }} />
                ) : undefined}
            </SearchBar>
            <TabView
                navigationState={{ index, routes }}
                renderScene={({ route }) => {
                    switch (route.key) {
                        case 'points':
                            return (
                                <BaseFrameRoute
                                    navigation={navigation}
                                    data={data.points}
                                    itemComponent={PointListItem}
                                />
                            )
                        case 'routes':
                            return (
                                <BaseFrameRoute
                                    navigation={navigation}
                                    data={data.routes}
                                    itemComponent={RouteListItem}
                                />
                            )
                        case 'cities':
                            return (
                                <BaseFrameRoute
                                    navigation={navigation}
                                    data={data.cities}
                                    itemComponent={CityListItem}
                                />
                            )
                        default:
                            return null;
                    }
                }}
                onIndexChange={setIndex}
                initialLayout={{ width: layout.width }}
                renderTabBar={props => (
                    <TabBar
                        {...props}
                        indicatorStyle={styles.tabBarIndicator}
                        tabStyle={styles.tabBarTab}
                        labelStyle={styles.tabBarLabel}
                        renderBadge={({ route }) => {
                            const badge = data[route.key].results.length;
                            return <Text
                                style={{
                                    ...styles.badge,
                                    ...(badge === 0 ? styles.badgeZero : {})
                                }}>{badge}</Text>;
                        }}
                        style={styles.tabBar}
                    />
                )}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    searchBar: {
        marginHorizontal: 12
    },
    tabContainer: {},
    tabBar: {
        backgroundColor: '#fff',
    },
    tabBarTab: {},
    tabBarLabel: {
        color: '#000',
        ...DefaultStyles.font,
        textTransform: 'none'
    },
    tabBarIndicator: {
        backgroundColor: DEFAULT_COLOR,
        height: 3
    },
    flatList: {
        paddingTop: 12
    },
    notFound: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        height: '100%',
        marginVertical: '30%'
    },
    notFoundText: {
        ...DefaultStyles.font,
        fontSize: 20,
        color: '#aaa',
        textAlign: 'center',
        marginVertical: 32,
        marginHorizontal: 32
    },
    badge: {
        backgroundColor: DEFAULT_COLOR,
        color: 'white',
        borderRadius: 8,
        height: 16,
        width: 20,
        lineHeight: 16,
        textAlign: 'center',
        overflow: 'hidden',
        fontSize: 10,
        ...DefaultStyles.font,
        top: 6,
        right: 4,
    },
    badgeZero: {
        backgroundColor: '#eee'
    }
});
import React, { useEffect, useState, useContext } from 'react';
import { View, StyleSheet, Text, useWindowDimensions } from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';
import { useSelector } from 'react-redux';

import DefaultStyles, { DEFAULT_COLOR } from '../constance/styles';
import { ReloadedPaginatedFlatListWrapper } from './ReloadedPaginatedFlatListWrapper';
import { PointListItem } from '../components/PointListItem';
import { RouteListItem } from '../components/RouteListItem';
import { fetchPointsFavorite } from '../redux/actions/point';
import { fetchRoutesFavorite } from '../redux/actions/route';

import { GeoContext } from './GeoWrapper';
import { useDispatch } from 'react-redux';


const PointRoute = ({ navigation }) => {
    const dispatch = useDispatch()
    const userLocation = useContext(GeoContext);
    const idsList = useSelector(state => state.bookmark.points);
    const params = {
        limit: 100,
        id__in: idsList.length > 0 ? idsList.join(',') : '-1',
        point: `${userLocation.longitude},${userLocation.latitude}`
    }
    
    useEffect(() => {
        dispatch(fetchPointsFavorite(1, params))
    }, [idsList]);

    const renderItem = ({ item }) => {
        return (
            <PointListItem
                item={item}
                navigation={navigation}
                userLocation={userLocation}
            />
        );
    };

    return (
        idsList ? <ReloadedPaginatedFlatListWrapper
            stateKey='point'
            dispatchFunction={fetchPointsFavorite}
            additionalDispatchParams={params}
            renderItem={renderItem}
            stateContainer={'pointsFavorite'}
        /> : <></>
    )
}

const RoutesdRoute = ({ navigation }) => {
    const dispatch = useDispatch()
    const idsList = useSelector(state => state.bookmark.routes);
    const params = {
        limit: 100,
        id__in: idsList.length > 0 ? idsList.join(',') : '-1',
    }
    
    useEffect(() => {
        dispatch(fetchRoutesFavorite(1, params))
    }, [idsList]);
    
    const renderItem = ({ item }) => {
        return (
            <RouteListItem
                item={item}
                navigation={navigation}
            />
        );
    };

    return (
        idsList ? <ReloadedPaginatedFlatListWrapper
            stateKey='route'
            dispatchFunction={fetchRoutesFavorite}
            additionalDispatchParams={params}
            renderItem={renderItem}
            stateContainer={'routesFavorite'}
        /> : <></>
    );
};
export const BookmarkContainer = ({ navigation }) => {
    const layout = useWindowDimensions();

    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'points', title: 'Места' },
        { key: 'routes', title: 'Маршруты' },
    ]);

    return (
        <View style={styles.container}>
            <TabView
                navigationState={{ index, routes }}
                renderScene={({ route }) => {
                    switch (route.key) {
                        case 'points':
                            return <PointRoute navigation={navigation} />
                        case 'routes':
                            return <RoutesdRoute navigation={navigation} />
                        default:
                            return null;
                    }
                }}
                onIndexChange={setIndex}
                initialLayout={{ width: layout.width }}
                renderTabBar={props => (
                    <TabBar
                        {...props}
                        indicatorStyle={styles.tabBarIndicator}
                        tabStyle={styles.tabBarTab}
                        labelStyle={styles.tabBarLabel}
                        style={styles.tabBar}
                    />
                )}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    tabContainer: {

    },
    tabBar: {
        backgroundColor: '#fff',
    },
    tabBarTab: {},
    tabBarLabel: {
        color: '#000',
        ...DefaultStyles.font,
        textTransform: 'none'
    },
    tabBarIndicator: {
        backgroundColor: DEFAULT_COLOR,
        height: 3
    },
    tabFlatList: {

    },
    notFound: {

    },
    notFoundText: {

    }
})

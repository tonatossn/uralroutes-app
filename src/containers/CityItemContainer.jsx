import React, { useState, useEffect } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { ContextPoints } from '../components/ContextPoints';
import { ContextRoutes } from '../components/ContextRoutes';
import { CityItem } from '../components/CityItem';
import { fetchCityItem } from '../redux/actions/city';
import { fetchPointsByCity } from '../redux/actions/point';
import { fetchRoutesByCity } from '../redux/actions/route';

export const CityItemContainer = ({ navigation, route }) => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchCityItem(route.params.id));
        dispatch(fetchPointsByCity(route.params.id));
        dispatch(fetchRoutesByCity(route.params.id));
    }, [dispatch, route.params.id]);
    
    const item = useSelector(state => {
        return state.city.citiesById
                    ? state.city.citiesById[route.params.id]
                    : null;
    });
    const pointsByCity = useSelector(state => {
        try {
            return state.point.pointsByCity[route.params.id].results
        } catch (e) { return [] }
    });
    const routesByCity = useSelector(state => {
        try {
            return state.route.routesByCity[route.params.id].results
        } catch (e) { return [] }
    });
    
    return (
        item ? (
            <ScrollView showsVerticalScrollIndicator={false}>
                <CityItem navigation={navigation} item={item} />
                <ContextPoints title='Места в городе и окрестностях' points={pointsByCity} navigation={navigation} />
                <ContextRoutes title='Входит в маршруты' routes={routesByCity} navigation={navigation} />
            </ScrollView>
        ) : null
    );
}

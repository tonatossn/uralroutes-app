import React, { useState, useEffect, useRef } from 'react';
import { Ionicons } from '@expo/vector-icons';
import { useSelector, useDispatch } from 'react-redux';
import { FlatList, View, StyleSheet, Text } from 'react-native';
import DefaultStyles from '../constance/styles';
import config from '../constance/config';


const NotFound = () => {
    return (
        <View style={styles.notFound}>
            <Ionicons name="sad-outline" color={'#efefef'} size={100} />
            <Text style={styles.notFoundText}>
                К сожалению, данных не найдено. Попробуйте смягчить условия фильтров
            </Text>
        </View>
    );
}

export const ReloadedPaginatedFlatListWrapper = ({
    route,
    dispatchFunction,
    additionalDispatchParams,
    renderItem,
    stateKey,
    stateContainer,
}) => {
    stateContainer = stateContainer === undefined ? 'list' : stateContainer;
    
    const dispatch = useDispatch();
    const flatListRef = useRef(null);
    const dataList = useSelector(state => state[stateKey][stateContainer]);
    const ordering = useSelector(state => state[stateKey].ordering);
    const limit = useSelector(state => state[stateKey].limit);
    const dataFilters = useSelector(state => state[stateKey].filters);
    const [page, setPage] = useState(1);
    const [isRefreshing, setIsRefreshing] = useState(false);
    const [needRefresh, setNeedRefresh] = useState(false);
    
    const notFoundList = [{id: 'notfound-404', title: 'Not found'}];

    useEffect(() => {
        if (config.RELOAD_LIST_ON_OPEN) {
            setNeedRefresh(!needRefresh);
        }
    }, []);

    useEffect(() => {
        setPage(1);
    }, [ordering, dataFilters]);

    useEffect(() => {
        setIsRefreshing(true);
        let params = {}
        if (ordering != undefined) {
            params['ordering'] = ordering;
        }
        if (limit != undefined) {
            params['limit'] = limit;
        }
        
        if (dataFilters != undefined) {
            Object.entries(dataFilters).forEach(([key, value]) => {
                if (value != null) {
                    params[key] = typeof value === 'object' ? value.join() : value;
                }
            });
        }
        
        dispatch(
            dispatchFunction(page, {
                    ...additionalDispatchParams,
                    ...params
                }
            )
        );
        setIsRefreshing(false);
    }, [
        dispatch,
        ordering,
        dataFilters,
        page,
        needRefresh,
        limit,
        stateContainer
    ]);

    const onRefresh = () => {
        setPage(1);
        setNeedRefresh(!needRefresh);
    }

    const handleLoadMore = ({ distanceFromEnd }) => {
        if (dataList.next == null) {
            return;
        }
        setPage(page + 1);
    }

    const renderNotFound = ({ item }) => {
        return <NotFound />;
    }
    
    return (
        <View style={styles.container}>
            <FlatList
                data={dataList.results?.length > 0 ? dataList.results : notFoundList}
                renderItem={dataList.results?.length > 0 ? renderItem : renderNotFound}
                ref={flatListRef}
                keyExtractor={item => item.id}
                refreshing={isRefreshing}
                onRefresh={onRefresh}
                onEndReached={handleLoadMore}
                onEndReachedThreshold={0.2}
                showsVerticalScrollIndicator={false}
                style={styles.flatList}
                contentContainerStyle={{ paddingBottom: 36 }}
            />
		</View>
    );
};

const styles = StyleSheet.create({
	container: {
        flex: 1
	},
    flatList: {
        paddingTop: 12
    },
    notFound: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        height: '100%',
        marginVertical: '30%'
    },
    notFoundText: {
        ...DefaultStyles.font,
        fontSize: 20,
        color: '#aaa',
        textAlign: 'center',
        marginVertical: 32,
        marginHorizontal: 32
    }
});

import React, { useEffect, useState } from 'react';
import * as Location from 'expo-location';
import { Alert } from 'react-native';

export const GeoContext = React.createContext({});

export default function GeoWrapper({ children }) {
    const [location, setLocation] = useState({
        latitude: null,
        longitude: null
    });
    
    useEffect(() => {
        async function getLocation () {
            let { status } = await Location.requestForegroundPermissionsAsync();
            
            if (status !== 'granted') {
                Alert.alert(
                    title='Не удалось получить геолокацию',
                    message='Разрешите доступ к геолокациии в настройках для полноценной работы приложения'
                )
                return null;
            }
            
            let data = await Location.getCurrentPositionAsync({});
            setLocation(data.coords)
        }
        getLocation()
    }, []);

    return (
        <GeoContext.Provider value={location} style={{ flex: 1, height: '100%'}}>
            {children}
        </GeoContext.Provider>
    );
}
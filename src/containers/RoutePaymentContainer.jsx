import React, { useEffect } from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { RoutePayment } from '../components/RoutePayment';

export const RoutePaymentContainer = ({ navigation, route }) => {
    return (
        <RoutePayment
            navigation={navigation}
            day={route.params.day}
            routeItem={route.params.item}
        />
    );
}
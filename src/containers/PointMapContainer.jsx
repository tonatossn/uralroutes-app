import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { PointBaseWrapper } from './PointBaseWrapper';
import { PointMap } from '../components/PointMap';
import { setPointsListLimit } from '../redux/actions/point';

export const PointMapContainer = ({ navigation, route }) => {
    const points = useSelector(state => state.point.list);
    const dispatch = useDispatch();
    
    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            dispatch(setPointsListLimit(200))
        });
        return unsubscribe;
    }, [navigation]);

    return (
        <PointBaseWrapper hasSort={false} navigation={navigation}>
            <PointMap
                navigation={navigation}
                points={points.results}
            />
        </PointBaseWrapper>
    )
}
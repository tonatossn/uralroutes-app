import React from 'react';
import { ScrollView } from 'react-native';
import { useSelector } from 'react-redux';

import { MyRoutesItem } from '../components/MyRoutesItem';
import { ButtonContainerBottom } from '../components/elements/ButtonContainerBottom';

export const MyRoutesItemContainer = ({ navigation, route }) => {
    const myroutes = useSelector(state => state.myroutes.myroutes);
    const item = myroutes.filter(item => item.id == route.params.id)[0];
    
    return (
        <>
            <ScrollView showsVerticalScrollIndicator={false} nestedScrollEnabled={true}>
                <MyRoutesItem
                    navigation={navigation}
                    item={item}
                />
            </ScrollView>
            <ButtonContainerBottom items={[{
                onPress: () => {
                    return navigation.navigate('MyRoutes', {
                        screen: 'MyRoutesReorder',
                        initial: false,
                        params: {
                            id: item.id
                        }
                    });
                },
                title: 'Перестроить маршрут'
            }]} />
        </>
    );
}

import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import DraggableFlatList, {
    ScaleDecorator,
} from "react-native-draggable-flatlist";
import { Ionicons } from '@expo/vector-icons';

import DefaultStyles from '../constance/styles';
import { reorderPoints } from '../redux/actions/myroutes';


export const MyRoutesReorderContainer = ({ navigation, route }) => {
    const dispatch = useDispatch()
    const myroutes = useSelector(state => state.myroutes.myroutes);
    const item = myroutes.filter(item => item.id == route.params.id)[0];
    
    const reorderItems = (data) => {
        dispatch(reorderPoints(item.id, data))
        return data
    }

    const renderItem = ({ item, drag, isActive }) => {
        return (
            <ScaleDecorator>
                <TouchableOpacity
                    activeOpacity={1}
                    disabled={isActive}
                    onLongPress={drag}
                    pointerEvents='none'
                    style={{
                        ...styles.item,
                        ...{opacity: isActive ? 0.5 : 1}
                    }}
                >
                    <Text style={styles.itemText}>{item.title}</Text>
                    <Ionicons style={styles.itemIcon} name={'reorder-two-outline'} color={'#000'} size={24} />
                </TouchableOpacity>
            </ScaleDecorator>
        )
    }

    return (
        <View style={styles.container}>
            <DraggableFlatList
                data={item.points}
                onDragEnd={({ data }) => reorderItems(data)}
                keyExtractor={item => item.id}
                renderItem={renderItem}
                style={styles.list}
            />
            
            <View style={styles.help}>
                <Ionicons name={'shuffle-outline'} color={'#ccc'} size={64} style={styles.helpIcon} />
                <Text style={styles.helpText}>
                    Для изменения порядка, зажмите объект и перетащите в нужное место
                </Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 16
    },
    list: {
    },
    item: {
        paddingVertical: 24,
        borderBottomWidth: 1,
        borderBottomColor: '#efefef',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    itemText: {
        ...DefaultStyles.regularFont,
        fontSize: 16,
        paddingRight: 24,
        flexShrink: 10
    },
    itemIcon: {
    },

    help: {
        flexDirection: 'row',
        marginVertical: 32,
    },
    helpIcon: {
        flex: 1,
    },
    helpText: {
        ...DefaultStyles.font,
        flex: 4,
        fontSize: 16,
        color: '#ccc',
        textAlign: 'left',
    }
})
import * as React from 'react';
import { Button, View, Text } from 'react-native';


export const Page404 = ({ navigation }) => {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', height: '100%' }}>
            <Text style={{fontSize: 32}}>Страница не найдена</Text>
            <Button onPress={() => navigation.goBack()} title="Вернуться" />
        </View>
    );
}
import * as React from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { UserOfferPoint } from '../components/UserOfferPoint';


export const UserOfferPointContainer = ({ navigation }) => {
    return (
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
            <UserOfferPoint />
        </KeyboardAwareScrollView>
    );
}
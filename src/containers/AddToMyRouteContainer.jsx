import React from 'react';
import { useSelector } from 'react-redux';

import { AddToMyRouteModal } from '../components/AddToMyRouteModal';

export const AddToMyRouteContainer = ({ navigation, route }) => {
    const addedPoint = useSelector(state => state.point.pointsById[route.params.id]);
    const myroutes = useSelector(state => state.myroutes.myroutes);
    
    return (
        <AddToMyRouteModal
            addedPoint={addedPoint}
            myroutes={myroutes}
            navigation={navigation}
        />
    )
}

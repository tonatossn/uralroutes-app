import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { View, Text, FlatList, StyleSheet, Switch, TouchableOpacity } from 'react-native';

import DefaultStyles, { DEFAULT_COLOR } from '../constance/styles';
import { setPointFilters } from '../redux/actions/point';
import { fetchCities } from '../redux/actions/city';
import { UniversalPicker } from '../components/elements/UniversalPicker';
import { MultiSelectModal } from '../components/elements/MultiSelectModal';

const FilterSwitcher = ({ item }) => {
    const dispatch = useDispatch();
    const filterValue = useSelector(state => state.point.filters[item.name]);

    return (
        <Switch
            trackColor={{ false: "#767577", true: `${DEFAULT_COLOR}40` }}
            thumbColor={filterValue ? DEFAULT_COLOR : "#f4f3f4"}
            onValueChange={(state) => {
                dispatch(setPointFilters({
                    [item.name]: state ? state : null
                }))
            }}
            value={filterValue}
        />
    )
}

const Item = ({ item, active, getValues }) => {
    const dispatch = useDispatch();
    const [filterShow, setFilterShow] = useState(false);
    const onPress = () => {
        setFilterShow(!filterShow);
    }
    
    return (
        <TouchableOpacity style={styles.item} onPress={onPress}>
            <View>
                <Text style={styles.title}>{item.title}</Text>
                {(
                    (item.data.type === 'picker' || item.data.type === 'checkbox')
                    && getValues(item.data.name, active) != ''
                ) ? (
                    <Text style={styles.titleUndeline}>
                        Выбранные значения: {getValues(item.data.name, active)}
                    </Text>
                ): <></>}
            </View>

            {item.data.type === 'bool' ? (
                <FilterSwitcher item={item.data} />
            ) : <></>}
            
            {item.data.type === 'picker' ? (
                <UniversalPicker
                    items={item.data.values}
                    title={item.title}
                    onValueChange={(state) => {
                        dispatch(setPointFilters({
                            [item.data.name]: state ? state : null
                        }))
                    }}
                    status={filterShow}
                    setStatus={setFilterShow}
                    selectedValue={active}
                />
            ) : <></>}
            
            {item.data.type === 'checkbox' ? (
                <MultiSelectModal
                    items={item.data.values}
                    title={item.title}
                    onValueChange={state => {
                        dispatch(setPointFilters({
                            [item.data.name]: state ? state : []
                        }))
                    }}
                    status={filterShow}
                    setStatus={setFilterShow}
                    selectedValues={active}
                />
            ) : <></>}
        </TouchableOpacity>
    );
};
  

export const PointFilterContainer = ({ navigation }) => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchCities())
    }, [])
    const cities = useSelector(state => state.city.list)
    const activeFilters = useSelector(state => state.point.filters)

    const data = [
        {
            title: 'Доступность',
            data: {
                type: 'checkbox',
                name: 'availability__in',
                values: [
                    {
                        label: 'Открытые',
                        description: 'Можно осмотреть объект свободно',
                        value: 'open',
                    },
                    {
                        label: 'Доступ с сопровождением',
                        value: 'part_open',
                        description: 'Можно попасть только с сопровождающим лицом или в составе экскурсионной группы',
                    },
                    {
                        label: 'Закрытые',
                        value: 'closed',
                        description: 'Можно попасть только в рамках тура Уральской биеннале'
                    },
                    {
                        label: 'Только внешний осмотр',
                        value: 'fully_closed',
                        description: 'Закрыто для посещения и может быть небезопасно'
                    },
                ]
            }
        },
        {
            title: 'Входит в маршрут Биеналле',
            data: {
                type: 'bool',
                name: 'routes__schedules__isnull'
            }
        },
        {
            title: 'Выбор Биеннале',
            data: {
                type: 'bool',
                name: 'biennial_choice'
            }
        },
        {
            title: 'Удаленность',
            data: {
                type: 'picker',
                name: 'dist',
                values: [
                    {
                        label: 'Любая',
                        value: null,
                    },
                    {
                        label: 'до 1 км',
                        value: 1000,
                    },
                    {
                        label: 'до 5 км',
                        value: 5000,
                    },
                    {
                        label: 'до 20 км',
                        value: 20000,
                    },
                    {
                        label: 'до 100 км',
                        value: 100000,
                    },
                ]
            }
        },
        {
            title: 'Город',
            data: {
                type: 'checkbox',
                name: 'city__in',
                values: cities.results.length > 0 ? [
                    ...(cities.results.map(el => ({label: el.title, value: el.id})))
                ] : []
            }
        },
    ];

    const findSelectedValue = (key, active) => {
        let section = data.find(el => el.data.name == key);
        if (section === undefined) {
            return ''
        }
        
        return section.data.values.filter(el => {
            return (Array.isArray(active) ? active : [active]).includes(el.value)
        }).map((el) => el.label).join(', ');
    }
    
    return (
        <View style={styles.container}>
            <FlatList
                data={data}
                keyExtractor={(item, index) => item + index}
                renderItem={({ item }) => {
                    return (
                        <Item item={item} active={activeFilters[item.data.name]} getValues={findSelectedValue} />
                    )
                }}
                style={styles.list}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 16
    },
    list: {},
    item: {
        paddingVertical: 24,
        borderBottomWidth: 1,
        borderBottomColor: '#efefef',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    title: {
        ...DefaultStyles.regularFont,
        fontSize: 16
    },
    titleUndeline: {
        ...DefaultStyles.regularFont,
        fontSize: 12,
        color: '#999'
    }
})
import React, { useContext, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import * as Location from 'expo-location';

import { CityListItem } from '../components/CityListItem';
import { SortFilterBar } from '../components/elements/SortFilterBar';
import { fetchCities, setCityOrdering } from '../redux/actions/city';
import { collectCachedLocationByData } from '../redux/actions/location';
import { ReloadedPaginatedFlatListWrapper } from './ReloadedPaginatedFlatListWrapper';
import { GeoContext } from '../containers/GeoWrapper';


export const CityListContainer = ({ navigation }) => {
    const dispatch = useDispatch();
    const data = useSelector(state => state.city.list);
    const activeSort = useSelector(state => state.city.ordering);
    const locations = useSelector(state => state.location.addressGeodata);
    const userLocation = useContext(GeoContext);
    
    useEffect(() => {
        collectCachedLocationByData(dispatch, data.results, locations);
    }, [data]);

	const renderItem = ({item}) => {
        return (
            <CityListItem
                item={item}
                navigation={navigation}
                userLocation={userLocation}
            />
        );
    };

    return (
        <>
            <SortFilterBar
                sortVariants={[
                    {
                        value: 'title',
                        label: 'По названию'
                    },
                    {
                        value: 'points',
                        label: 'По кол-ву мест'
                    },
                ]}
                sortAction={(value) => {
                    dispatch(setCityOrdering(value));
                }}
                sortActive={activeSort}
            />
            <ReloadedPaginatedFlatListWrapper
                stateKey='city'
                dispatchFunction={fetchCities}
                renderItem={renderItem}
            />
        </>
    )
};

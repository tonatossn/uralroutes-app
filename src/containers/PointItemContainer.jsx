import React, { useState, useEffect } from 'react';
import { ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { ContextRoutes } from '../components/ContextRoutes';
import { PointItem } from '../components/PointItem';
import { fetchPointItem } from '../redux/actions/point';
import { fetchRoutesByPoint } from '../redux/actions/route';

export const PointItemContainer = ({ navigation, route }) => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchPointItem(route.params.id));
        dispatch(fetchRoutesByPoint(route.params.id));
    }, [dispatch, route.params.id]);
    
    const item = useSelector(state => state.point.pointsById[route.params.id]);
    const routes = useSelector(state => state.route.routesByPoint[route.params.id]);
    
    return (
        item ? (
            <ScrollView showsVerticalScrollIndicator={false}>
                <PointItem navigation={navigation} item={item} />
                {
                    routes
                        ? <ContextRoutes title='Входит в маршруты' routes={routes.results} navigation={navigation} />
                        : null
                }
            </ScrollView>
        ) : null
    )
}

import React, { useContext, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { SortFilterBar } from '../components/elements/SortFilterBar';
import { fetchCategories, setPointOrdering, setPointFilters } from '../redux/actions/point';
import { initialState as pointInitialState } from '../redux/reducers/point';
import { GeoContext } from '../containers/GeoWrapper';


export const PointBaseWrapper = ({ navigation, hasSort, needShadowForTags, children }) => {
    const dispatch = useDispatch();
	const userLocation = useContext(GeoContext);
    const activeSort = useSelector(state => state.point.ordering);
    const activeFilter = useSelector(state => state.point.filters);
    
    useEffect(() => {
        dispatch(fetchCategories());
    }, [dispatch]);

    const tags = useSelector(state => state.point.categoriesList);
    
    const onPressTag = (value) => {
        let valuesList = activeFilter['category__id__in'] != null
            ? activeFilter['category__id__in']
            : [];
        
        if (activeFilter['category__id__in'] != null) {
            if (activeFilter['category__id__in'].find(el => el == value.id)) {
                valuesList = valuesList.filter(el => value.id != el)
            } else {
                valuesList.push(value.id)
            }
        } else {
            valuesList.push(value.id);
        }
        dispatch(setPointFilters({
            'category__id__in': valuesList
        }))
    }

    const checkFilterIsActive = () => {
        let isActive = false;
        Object.keys(pointInitialState.filters).forEach((key) => {
            if (!Array.isArray(activeFilter[key]) && activeFilter[key] != null || (
                Array.isArray(activeFilter[key]) && activeFilter[key].length > 0
            )) {
                isActive = true
            }
        })
        return isActive;
    }
    
    let sortParams = {}
    if (hasSort) {
        sortParams = {
            sortVariants: [
                {
                    value: 'title',
                    label: 'По названию'
                },
                {
                    value: 'geodata',
                    label: 'По удаленности'
                },
            ],
            sortAction: (value) => {
                dispatch(setPointOrdering(value));
            },
            sortActive: activeSort
        }
    }
    
    return (
        <>
            <SortFilterBar
                {...sortParams}
                filterTitle={'Фильтры'}
                filterAction={() => {
                    navigation.navigate('PointFilters', {title: 'Фильтры'})
                }}
                tagsData={{
                    items: tags,
                    onPressAction: onPressTag,
                    selectedValues: activeFilter['category__id__in'],
                    needShadow: needShadowForTags
                }}
                filterActive={checkFilterIsActive()}
            />
            {userLocation != {} ? children : <></>}
        </>
    );
};

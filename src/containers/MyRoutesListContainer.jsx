import React from 'react';
import { useSelector } from 'react-redux';

import { MyRoutesList } from '../components/MyRoutesList';


export const MyRoutesListContainer = ({ navigation }) => {
    const myroutes = useSelector(state => state.myroutes.myroutes);

    return (
        <MyRoutesList
            navigation={navigation}
            myroutes={myroutes}
        />
    );
}

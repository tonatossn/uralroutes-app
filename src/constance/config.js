const isDevEnv = process.env.NODE_ENV === 'development';

// const LOCAL_HOST = process.env.LOCAL_HOST ? process.env.LOCAL_HOST : 'http://192.168.31.121:8000';
const LOCAL_HOST = process.env.LOCAL_HOST ? process.env.LOCAL_HOST : 'http://routes.zaart.net';
const PRODUCTION_HOST = 'http://routes.zaart.net';

const API_ENDPOINT = {
    CITY: 'cities',
    POINT: 'points',
    CATEGORIES: 'points/categories',
    ROUTE: 'routes',
    SCHEDULE_ROUTE: 'schedule_routes',
    FEEDBACK: 'feedback',
    SCHEDULE: 'schedule_routes',
    ABOUTE: 'pages',
    TICKET: 'tickets',
    MAPBOX_DIRECTION: `https://api.mapbox.com/directions/v5/mapbox/{0}/{1}?steps=false&geometries=geojson&access_token={2}`
}

export default config = {
    APP_NAME: 'Индустриальные маршруты «Биеннале»',
    API_BASE_URL: `${isDevEnv ? LOCAL_HOST : PRODUCTION_HOST}/api`,
    HOST_NAME: isDevEnv ? LOCAL_HOST : PRODUCTION_HOST,

    API_ENDPOINT: API_ENDPOINT,
    LIMIT_BY_PAGE: 10,
    RELOAD_LIST_ON_OPEN: false,
    MAPBOX_ACCESS_TOKEN: 'pk.eyJ1IjoidG9uYXRvcyIsImEiOiJja3czYmI4ajIwNTVkMm9udjJ1a2kyMDI2In0.OCu_Y_G_ggInIylOZHrEdQ',
}

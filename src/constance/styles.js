import { StyleSheet, Platform } from "react-native";

export const DEFAULT_COLOR = '#FF8A1F';
export const BACKGROUND_COLOR = '#ffffff';

let DefaultStyles = StyleSheet.create({
    regularFont: {
        fontFamily: 'Rubik_400Regular',
    },
    font: {
        fontFamily: 'Rubik_500Medium',
    },
    fontBold: {
        fontFamily: 'Rubik_700Bold',
    },
    fontItalic: {
        fontFamily: 'Rubik_400Regular_Italic',
    },
    header: {
        color: '#05030D',
        fontFamily: 'Rubik_500Medium',
        fontSize: 30,
        lineHeight: 32
    },
    headerMiddle: {
        color: '#05030D',
        fontFamily: 'Rubik_500Medium',
        fontSize: 24,
        lineHeight: 26
    },
    headerSmall: {
        color: '#05030D',
        fontFamily: 'Rubik_700Bold',
        fontSize: 20,
        lineHeight: 22
    },
    contextWrapper: {
        width: '100%',
        position: 'absolute',
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 14},
        shadowOpacity: 0.29,
        elevation: 12,
        borderRadius: 24,
    },
    contextShadowedImage: {
        flex: 1,
        justifyContent: 'flex-end',
		backgroundColor: '#dedede',
        overflow: 'hidden',
        borderRadius: 24
    },
    contextDistanceBaloon: {
        flex: 1,
        bottom: -16,
        borderRadius: 30,
        backgroundColor: '#fff',
        padding: 12,
        position: 'absolute',
        flexDirection: 'row',
        height: 42
    },
    contextShadowedTitle: {
        fontFamily: 'Rubik_700Bold',
        color: '#fff',
        fontSize: 18,
        textShadowOffset: {width: 0, height: 2},
        textShadowRadius: 25,
        textShadowColor: '#000',
    },
	buttonContainer: {
        shadowColor: '#999',
        shadowOffset: { width: 0, height: 14},
        shadowOpacity: 0.29,
        elevation: 12,
        borderRadius: 16,
        backgroundColor: '#fff',
        height: 56,
        alignItems: 'center',
        justifyContent: 'center',
	},
    buttonTitle: {
        fontFamily: 'Rubik_700Bold',
        color: DEFAULT_COLOR,
        fontSize: 16
    },
    buttonIcon: {
        position: 'absolute',
        right: 16
    },
    inputStyle: {
        borderColor: '#eee',
        borderRadius: 12,
        borderWidth: 1,
        paddingHorizontal: 16,
        paddingTop: 8,
        paddingBottom: 8,
        backgroundColor: '#fff',
        shadowColor: '#eee',
        shadowOffset: { width: 0, height: 4},
        shadowOpacity: 0.5,
        fontSize: 16,
    }
});

if (Platform.OS === 'ios') {
    DefaultStyles = {
        ...DefaultStyles,
        contextWrapper: {
            ...DefaultStyles.contextWrapper,
            shadowOffset: { width: 0, height: 5},
            shadowRadius: 6,
        },
        buttonContainer: {
            ...DefaultStyles.buttonContainer,
            shadowOffset: { width: 0, height: 6},
            shadowRadius: 8,
            shadowOpacity: 0.39,
        }
    }
}

export const FormStyles = StyleSheet.create({
    field: {
        marginBottom: 24
    },
    label: {
        ...DefaultStyles.fontBold,
        fontSize: 18,
        marginBottom: 8
    },
    input: {
        ...DefaultStyles.inputStyle,
        height: 50,
    },
    textarea: {
        ...DefaultStyles.inputStyle,
        height: 150,
    },
    error: {
        color: DEFAULT_COLOR,
        ...DefaultStyles.fontBold,
    }
})

export default DefaultStyles;
import React from 'react';

import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { PointMapContainer } from '../containers/PointMapContainer';
import { PointListContainer } from '../containers/PointListContainer';
import { PointItemContainer } from '../containers/PointItemContainer';
import { PointFilterContainer } from '../containers/PointFilterContainer';
import { AddToMyRouteContainer } from '../containers/AddToMyRouteContainer';
import { PointViewSwitcherTabBar } from '../components/elements/PointViewSwitcherTabBar';
import {
    mainTabNavigatorScreenOptions,
    pointTabNavigatorScreenOptions,
    innerNavigatorProps,
    modalProps
} from './navigationProps';
import {
    RouteStackNavigator,
    CityStackNavigator,
    IndexStackNavigator,
} from './StackNavigator';


const PointTab = createBottomTabNavigator();
const PointTabNavigator = () => {
    return (
        <PointTab.Navigator
            tabBar={props => <PointViewSwitcherTabBar {...props} />}
            screenOptions={{
                ...pointTabNavigatorScreenOptions,
            }}
        >

            <PointStack.Screen
                name="PointList"
                component={PointListContainer}
                options={{
                    headerTitle: 'Места',
                    title: 'Списком'
                }}
            />
            <PointTab.Screen
                name="PointMap"
                component={PointMapContainer}
                options={{
                    headerTitle: 'Места на карте',
                    title: 'На карте'
                }}
            />
        </PointTab.Navigator>
    )
}

const PointStack = createNativeStackNavigator();
const PointStackNavigator = () => {
    return (
        <PointStack.Navigator {...innerNavigatorProps}>
            <PointStack.Screen
                name={'PointIndex'}
                component={PointTabNavigator}
                options={{
                    headerShown: false
                }}
            />
            <PointStack.Screen
                name="PointItem"
                component={PointItemContainer}
                options={{
                    headerTitle: 'Место'
                }}
            />
            <PointStack.Group screenOptions={{ presentation: 'modal' }}>
                <PointStack.Screen
                    name="PointFilters"
                    label='Фильтры'
                    component={PointFilterContainer}
                    options={modalProps}
                />
                <PointStack.Screen
                    name="AddToMyRoute"
                    label='Добавить к маршруту'
                    component={AddToMyRouteContainer}
                    options={modalProps}
                />
            </PointStack.Group>
        </PointStack.Navigator>
    )
}

const Tab = createBottomTabNavigator();
export const TabNavigator = () => {
    return (
        <Tab.Navigator
            screenOptions={{
                ...mainTabNavigatorScreenOptions,
            }}
        >
            <Tab.Screen
                name="Index"
                component={IndexStackNavigator}
                options={{
                    tabBarLabel: 'Главная',
                    tabBarIcon: ({ color, size }) => (
                        <Ionicons name="home-outline" color={color} size={size} />
                    ),
                    headerShown: false
                }}
            />
            <Tab.Screen
                name="Point"
                component={PointStackNavigator}
                options={{
                    tabBarLabel: 'Места',
                    tabBarIcon: ({ color, size }) => (
                        <Ionicons name="location-outline" color={color} size={size} />
                    ),
                    headerShown: false
                }}
            />
            <Tab.Screen
                name="Route"
                component={RouteStackNavigator}
                options={{
                    tabBarLabel: 'Маршруты',
                    tabBarIcon: ({ color, size }) => (
                        <Ionicons name="git-branch-outline" color={color} size={size} />
                    ),
                    headerShown: false
                }}
            />
            <Tab.Screen
                name="City"
                component={CityStackNavigator}
                options={{
                    tabBarLabel: 'Города',
                    tabBarIcon: ({ color, size }) => (
                        <Ionicons name="business-outline" color={color} size={size} />
                    ),
                    headerShown: false
                }}
            />
        </Tab.Navigator>
    )
};

import * as React from 'react';

import { TabNavigator } from './TabNavigator';

export const RootNavigator = () => {
    return (
        <TabNavigator />
    )
};
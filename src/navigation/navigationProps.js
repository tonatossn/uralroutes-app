import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import { HeaderInner } from '../components/elements/HeaderInner';
import { HeaderSection } from '../components/elements/HeaderSection';
import DefaultStyles, { DEFAULT_COLOR } from '../constance/styles';


export const innerNavigatorProps = {
    screenOptions: {
        header: props => <HeaderInner
            navigation={props.navigation}
            route={props.route}
            options={props.options}
        />,
    }
}

export const sectionNavigatorProps = {
    screenOptions: {
        header: props => <HeaderSection
            navigation={props.navigation}
            route={props.route}
            options={props.options}
        />
    },
    headerBlurEffect: 'extraLight'
}

export const baseTabNavigatorScreenOptions = {
    ...sectionNavigatorProps.screenOptions,
    tabBarActiveTintColor: DEFAULT_COLOR,
    tabBarStyle: {
        height: 64,
        paddingTop: 6,
        paddingBottom: 6,
    },
}

export const pointTabNavigatorScreenOptions = {
    ...baseTabNavigatorScreenOptions,
    tabBarStyle: {
        height: '100%',
        borderRadius: 21,
        overflow: 'hidden',
    },
    tabBarItemStyle: {
        borderRightWidth: 1,
        borderColor: '#ddd',
        right: -2,
        borderWidth: 0
    },
    tabBarLabelStyle: {
        ...DefaultStyles.font,
        lineHeight: 42,
        fontSize: 14
    },
    tabBarIconStyle: {
        display: 'none'
    }
}

export const mainTabNavigatorScreenOptions = {
    ...baseTabNavigatorScreenOptions,
    tabBarButton: props => {
        const color = props.accessibilityState.selected ? `${DEFAULT_COLOR}17` : null;
        return (
            <TouchableOpacity {...props} activeOpacity={0.7}>
                <View style={{
                    backgroundColor: color,
                    borderRadius: 6,
                    paddingHorizontal: 16,
                    paddingVertical: 6,
                }}>
                    {props.children}
                </View>
            </TouchableOpacity>
        )
    },
}

export const modalProps = ({ route, navigation }) => {
    return {
        headerBackVisible: false,
        headerShadowVisible: false,
        headerLeft: () => {},
        headerTitle: () => (
            <Text style={DefaultStyles.headerSmall}>{route.params.title}</Text>
        ),
        headerRight: () => (
            <TouchableOpacity onPress={() => {
                navigation.goBack();
            }}>
                <Ionicons name="close-circle-outline" color={'#000'} size={32} />
            </TouchableOpacity>
        ),
        header: (props) => {
            return (
                <View style={styles.modalHeader}>
                    <View style={styles.headerLeft}>{props.options.headerLeft()}</View>
                    <View style={styles.headerTitle}>{props.options.headerTitle()}</View>
                    <View style={styles.headerRight}>{props.options.headerRight()}</View>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    modalHeader: {
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16
    },
    headerLeft: {},
    headerRight: {},
    headerTitle: {},
})
import * as React from 'react';

import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { IndexContainer } from '../containers/IndexContainer';
import { CityListContainer } from '../containers/CityListContainer';
import { CityItemContainer } from '../containers/CityItemContainer';
import { RouteListContainer } from '../containers/RouteListContainer';
import { RouteItemContainer } from '../containers/RouteItemContainer';
import { SearchContainer } from '../containers/SearchContainer';
import { AbouteContainer } from '../containers/AbouteContainer';
import { MyRoutesListContainer } from '../containers/MyRoutesListContainer';
import { MyRoutesItemContainer } from '../containers/MyRoutesItemContainer';
import { MyRoutesReorderContainer } from '../containers/MyRoutesReorderContainer'
import { BuyedRoutesContainer } from '../containers/BuyedRoutesContainer';
import { UserOfferPointContainer } from '../containers/UserOfferPointContainer';
import { BookmarkContainer } from '../containers/BookmarkContainer';
import { RoutePaymentContainer } from '../containers/RoutePaymentContainer';
import { HeaderIndex } from '../components/elements/HeaderIndex';
import { innerNavigatorProps, sectionNavigatorProps, modalProps } from './navigationProps';


const MyRoutesStack = createNativeStackNavigator();
const MyRoutesStackNavigator = () => {
    return (
        <MyRoutesStack.Navigator {...sectionNavigatorProps}>
            <MyRoutesStack.Screen
                name="MyRoutesList"
                component={MyRoutesListContainer}
                options={{
                }}
            />
            <MyRoutesStack.Screen
                name="MyRoutesItem"
                component={MyRoutesItemContainer}
                options={{
                    headerTitle: 'Маршруты',
                    ...innerNavigatorProps.screenOptions
                }}
            />
            <MyRoutesStack.Screen
                name="MyRoutesReorder"
                component={MyRoutesReorderContainer}
                options={{
                    headerTitle: 'Вернуться к маршруту',
                    ...innerNavigatorProps.screenOptions
                }}
            />
        </MyRoutesStack.Navigator>
    )
}

const IndexStack = createNativeStackNavigator();
const IndexStackNavigator = () => {
    return (
        <IndexStack.Navigator>
            <IndexStack.Screen
                name="IndexPage"
                component={IndexContainer}
                options={{
                    tabBarLabel: 'Главная',
                    header: props => <HeaderIndex
                        navigation={props.navigation}
                        route={props.route}
                        options={props.options}
                    />
                }}
            />
            <IndexStack.Group screenOptions={{ presentation: 'modal' }}>
                <IndexStack.Screen
                    name="Search"
                    component={SearchContainer}
                    label='Поиск'
                    options={modalProps}
                />
            </IndexStack.Group>
            <IndexStack.Group {...sectionNavigatorProps}>
                <IndexStack.Screen
                    name="Bookmark"
                    component={BookmarkContainer}
                />
                <IndexStack.Screen
                    name="MyRoutes"
                    component={MyRoutesStackNavigator}
                    options={{
                        headerShown: false
                    }}
                />
                <IndexStack.Screen
                    name="BuyedRoutes"
                    component={BuyedRoutesContainer}
                />
                <IndexStack.Screen
                    name="UserOfferPoint"
                    component={UserOfferPointContainer}
                />
                <IndexStack.Screen
                    name="Aboute"
                    component={AbouteContainer}
                />
            </IndexStack.Group>
        </IndexStack.Navigator>
    )
}

const RouteStack = createNativeStackNavigator();
const RouteStackNavigator = () => {
    return (
        <RouteStack.Navigator {...sectionNavigatorProps}>
            <RouteStack.Screen
                name="RouteList"
                component={RouteListContainer}
            />
            <RouteStack.Screen
                name="RouteItem"
                component={RouteItemContainer}
                options={{
                    ...innerNavigatorProps.screenOptions,
                    headerTitle: 'Маршрут'
                }}
            />
            <RouteStack.Group screenOptions={{ presentation: 'modal' }}>
                <RouteStack.Screen
                    name="RoutePayment"
                    component={RoutePaymentContainer}
                    label='Купить тур'
                    options={modalProps}
                />
            </RouteStack.Group>
        </RouteStack.Navigator>
    )
}

const CityStack = createNativeStackNavigator();
const CityStackNavigator = () => {
    return (
        <CityStack.Navigator {...sectionNavigatorProps}>
            <CityStack.Screen
                name="CityList"
                component={CityListContainer}
            />
            <CityStack.Screen
                name="CityItem"
                component={CityItemContainer}
                options={{
                    headerTitle: 'Город',
                    ...innerNavigatorProps.screenOptions,
                }}
            />
        </CityStack.Navigator>
    )
}


export {
    RouteStackNavigator,
    CityStackNavigator,
    IndexStackNavigator
}
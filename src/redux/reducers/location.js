import { GET_CITY_LOCATION } from '../actions/location';

const initialState = {
    addressGeodata: {}
}

const location = (state = initialState, action) => {
    switch (action.type) {
        case GET_CITY_LOCATION:
            return {
                ...state,
                addressGeodata: {
                    ...state.addressGeodata,
                    [action.payload.id]: action.payload.data
                }
            };

        default:
            return state;
    }
}

export default location;
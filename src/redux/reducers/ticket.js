import config from '../../constance/config';

import {
    CREATE_TICKET,
    GET_TICKET_LIST,
    CLEAR_PAYMENT,
} from '../actions/ticket';

export const initialState = {
    list: {
        results: []
    },
    currentPayment: {},
};

const ticket = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_TICKET:
            return {
                ...state,
                currentPayment: action.payload
            };

        case CLEAR_PAYMENT:
            return {
                ...state,
                currentPayment: {}
            };

        case GET_TICKET_LIST:
            return {
                ...state,
                list: action.payload
            };
    
        default:
            return state;
    }
}

export default ticket;
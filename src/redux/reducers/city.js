import { mergeListById } from '../../utils/mergeListById';
import { GET_CITY, GET_CITY_LIST, SET_CITY_ORDERING } from '../actions/city';

const initialState = {
    list: {
        results: []
    },
    citiesById: {},
    ordering: 'title',
    filters: {},
};

const city = (state = initialState, action) => {
    switch (action.type) {
        case GET_CITY:
            return {
                ...state,
                citiesById: {
                    ...state.citiesById,
                    [action.payload.id]: action.payload
                }
            };

        case GET_CITY_LIST:
            return {
                ...state,
                list: {
                    ...(
                        action.payload['previous'] != null
                            ? mergeListById(state.list, action.payload, 'results')
                            : action.payload
                    )
                }
            };

        case SET_CITY_ORDERING:
            return {
                ...state,
                // list: initialState.list,
                ordering: action.payload
            };
            
        default:
            return state;
    }
}

export default city;
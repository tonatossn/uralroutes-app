import config from '../../constance/config';
import { mergeListById } from '../../utils/mergeListById';
import {
    GET_ROUTE,
    GET_ROUTE_LIST,
    GET_ROUTE_LIST_BY_CITY,
    GET_ROUTE_LIST_BY_DATE,
    GET_ROUTE_LIST_BY_POINT,
    SET_ROUTE_ORDERING,
    SET_ROUTE_FILTERS,
    GET_ROUTE_LIST_ON_INDEX,
    GET_ROUTE_LIST_FAVORITE,
} from '../actions/route';

export const initialState = {
    list: {
        results: []
    },
    routesById: {},
    routesByCity: {},
    routesByDate: [],
    routesByPoint: {},
    routesOnIndex: [],
    routesFavorite: {
        results: []
    },
    ordering: 'id',
    filters: {
        'availability__in': null,
        'schedules__isnull': null,
        'tours__isnull': null,
    },
    limit: config.LIMIT_BY_PAGE
};

const route = (state = initialState, action) => {
    switch (action.type) {
        case GET_ROUTE:
            return {
                ...state,
                routesById: {
                    ...state.routesById,
                    [action.payload.id]: action.payload
                }
            };
            
        case GET_ROUTE_LIST:
            return {
                ...state,
                list: {
                    ...(
                        action.payload['previous'] != null
                            ? mergeListById(state.list, action.payload, 'results')
                            : action.payload
                    )
                }
            };

        case GET_ROUTE_LIST_BY_DATE:
            return {
                ...state,
                routesByDate: action.payload
            };

        case GET_ROUTE_LIST_BY_CITY:
            return {
                ...state,
                routesByCity: {
                    ...state.routesByCity,
                    [action.payload.cityId]: action.payload.data
                }
            };

        case GET_ROUTE_LIST_BY_POINT:
            return {
                ...state,
                routesByPoint: {
                    ...state.routesByPoint,
                    [action.payload.pointId]: action.payload.data
                }
            };

        case GET_ROUTE_LIST_ON_INDEX:
            return {
                ...state,
                routesOnIndex: action.payload
            };
            
        case SET_ROUTE_ORDERING:
            return {
                ...state,
                // list: initialState.list,
                ordering: action.payload
            };
        
        case SET_ROUTE_FILTERS:
            return {
                ...state,
                // list: initialState.list,
                filters: action.payload
            };

        case GET_ROUTE_LIST_FAVORITE:
            return {
                ...state,
                routesFavorite: action.payload
            };

        default:
            return state;
    }
}

export default route;
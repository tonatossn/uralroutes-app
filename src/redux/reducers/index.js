import { combineReducers } from 'redux';
import point from './point';
import city from './city';
import route from './route';
import location from './location';
import bookmark from './bookmark';
import search from './search';
import about from './about';
import myroutes from './myroutes';
import ticket from './ticket';

export default combineReducers({
    point,
    city,
    route,
    location,
    bookmark,
    search,
    about,
    myroutes,
    ticket,
});
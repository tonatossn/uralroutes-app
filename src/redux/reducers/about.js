import { GET_ABOUT_DATA } from '../actions/about';

const initialState = {
    data: {}
};

const about = (state = initialState, action) => {
    switch (action.type) {
        case GET_ABOUT_DATA:
            return {
                ...state,
                data: action.payload
            };
            
        default:
            return state;
    }
}

export default about;
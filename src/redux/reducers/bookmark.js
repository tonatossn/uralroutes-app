import {
    ADD_TO_BOOKMARK_LIST,
    REMOVE_FROM_BOOKMARK_LIST
} from '../actions/bookmark';

const initialState = {
    points: [],
    routes: [],
}

const bookmark = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TO_BOOKMARK_LIST:
            return {
                ...state,
                [action.payload.type]: [
                    ...state[action.payload.type],
                    action.payload.id
                ]
            };

        case REMOVE_FROM_BOOKMARK_LIST:
            return {
                ...state,
                [action.payload.type]: state[action.payload.type].filter(
                    obj => obj !== action.payload.id
                )
            };

        default:
            return state;
    }
}

export default bookmark;
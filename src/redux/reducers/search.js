import {
    GET_SEARCH_DATA,
    CLEAR_SEARCH_DATA
} from '../actions/search';

const initialState = {
    points: {results: []},
    routes: {results: []},
    cities: {results: []},
}

const bookmark = (state = initialState, action) => {
    switch (action.type) {
        case GET_SEARCH_DATA:
            return action.payload;

        case CLEAR_SEARCH_DATA:
            return initialState;

        default:
            return state;
    }
}

export default bookmark;
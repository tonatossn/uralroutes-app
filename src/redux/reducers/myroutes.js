import {
    CREATE_ROUTE,
    REMOVE_ROUTE,
    RENAME_ROUTE,
    ADD_POINT_TO_ROUTE,
    REMOVE_POINT_FROM_ROUTE,
    REORDER_POINTS,
} from '../actions/myroutes';

const initialState = {
    myroutes: [],
}

const myroutes = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_ROUTE:
            return {
                ...state,
                myroutes: [
                    ...state.myroutes,
                    action.payload
                ]
            };

        case REMOVE_ROUTE:
            return {
                ...state,
                myroutes: [
                    ...state.myroutes.filter(
                        route => route.id != action.payload.id
                    ),
                ]
            };

        case RENAME_ROUTE:
            return state;

        case ADD_POINT_TO_ROUTE:
            return {
                ...state,
                myroutes: state.myroutes.map(route => {
                    if (route.id == action.payload.routeId) {
                        route.points = [
                            ...route.points,
                            action.payload.point
                        ]
                    }
                    return route;
                })
            };

        case REMOVE_POINT_FROM_ROUTE:
            return state;
    
        case REORDER_POINTS:
            return {
                ...state,
                myroutes: state.myroutes.map(route => {
                    if (route.id == action.payload.routeId) {
                        route.points = action.payload.points
                    }
                    return route;
                })
            }
    
        default:
            return state;
    }
}

export default myroutes;
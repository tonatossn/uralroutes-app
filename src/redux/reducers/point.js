import config from '../../constance/config';
import { mergeListById } from '../../utils/mergeListById';
import {
    GET_POINT,
    GET_POINT_LIST,
    GET_CATEGORIES_LIST,
    GET_POINT_LIST_BY_CITY,
    GET_POINT_LIST_BY_DISTANCE,
    GET_POINT_LIST_BIENNALE_CHOICE,
    SET_POINT_ORDERING,
    SET_POINT_FILTERS,
    GET_POINT_LIST_FAVORITE,
    SET_POINT_LIST_LIMIT
} from '../actions/point';

export const initialState = {
    list: {
        results: []
    },
    pointsById: {},
    pointsByCity: {},
    pointsBiennaleChoice: [],
    pointsByDistance: [],
    pointsFavorite: {
        results: []
    },
    categoriesList: [],
    ordering: 'title',
    filters: {
        'category__id__in': [],
        'routes__schedules__isnull': null,
        'availability__in': [],
        'biennial_choice': null,
        'dist': null,
        'city__in': [],
    },
    limit: config.LIMIT_BY_PAGE
};

const point = (state = initialState, action) => {
    switch (action.type) {
        case GET_CATEGORIES_LIST:
            return {
                ...state,
                categoriesList: action.payload.results
            }

        case GET_POINT:
            return {
                ...state,
                pointsById: {
                    ...state.pointsById,
                    [action.payload.id]: action.payload
                }
            };

        case GET_POINT_LIST:
            return {
                ...state,
                list: {
                    ...(
                        action.payload['previous'] != null
                            ? mergeListById(state.list, action.payload, 'results')
                            : action.payload
                    )
                }
            };

        case GET_POINT_LIST_BY_DISTANCE:
            return {
                ...state,
                pointsByDistance: action.payload
            };

        case GET_POINT_LIST_BIENNALE_CHOICE:
            return {
                ...state,
                pointsBiennaleChoice: action.payload
            };

        case GET_POINT_LIST_BY_CITY:
            return {
                ...state,
                pointsByCity: {
                    ...state.pointsByCity,
                    [action.payload.cityIds]: action.payload.data
                }
            };
            
        case SET_POINT_ORDERING:
            return {
                ...state,
                // list: initialState.list,
                ordering: action.payload
            };

    
        case SET_POINT_FILTERS:
            return {
                ...state,
                // list: initialState.list,
                filters: {
                    ...state.filters,
                    ...action.payload
                }
            };
            
        case GET_POINT_LIST_FAVORITE:
            return {
                ...state,
                pointsFavorite: action.payload
            };

        case SET_POINT_LIST_LIMIT:
            return {
                ...state,
                limit: action.payload ? action.payload : state.limit
            };

        default:
            return state;
    }
}

export default point;
import { getAbout } from '../../api/about';

export const GET_ABOUT_DATA = 'GET_ABOUT_DATA';

export const fetchAboutData = () => async (dispatch) => {
    const data = await getAbout();
    dispatch({type: GET_ABOUT_DATA, payload: data});
}

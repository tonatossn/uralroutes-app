import { postTicket, getTicketList } from '../../api/ticket';

export const CREATE_TICKET = 'CREATE_TICKET';
export const GET_TICKET_LIST = 'GET_TICKET_LIST';
export const CLEAR_PAYMENT = 'CLEAR_PAYMENT';

export const createTicket = (postData = null) => async (dispatch) => {
    const data = await postTicket(postData);
    dispatch({type: CREATE_TICKET, payload: data});
};

export const fetchTickets = (devideId = null) => async (dispatch) => {
    const data = await getTicketList({
        limit: 100,
        status__in: 'success,failed',
        ordering: '-created_at,day__start',
        user_device_id: devideId
    });
    dispatch({type: GET_TICKET_LIST, payload: data});
};

export const clearPayment = () => async (dispatch) => {
    dispatch({type: CLEAR_PAYMENT});   
};

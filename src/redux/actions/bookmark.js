export const ADD_TO_BOOKMARK_LIST = 'ADD_TO_BOOKMARK_LIST';
export const REMOVE_FROM_BOOKMARK_LIST = 'REMOVE_FROM_BOOKMARK_LIST';

const AVAILABLE_CONTENT_TYPES = [
    'points', 'routes',
]

export const addBookmark = (contentType, id = null) => dispatch => {
    if (!AVAILABLE_CONTENT_TYPES.includes(contentType) || id === null) {
        return false;
    }
        
    dispatch({
        type: ADD_TO_BOOKMARK_LIST,
        payload: {
            type: contentType,
            id: id
        }
    });
};
  
export const removeBookmark = (contentType, id = null) => dispatch => {
    if (!AVAILABLE_CONTENT_TYPES.includes(contentType) || id === null) {
        return false;
    }

    dispatch({
        type: REMOVE_FROM_BOOKMARK_LIST,
        payload: {
            type: contentType,
            id: id
        }
    });
};

export const CREATE_ROUTE = 'CREATE_ROUTE';
export const REMOVE_ROUTE = 'REMOVE_ROUTE';
export const RENAME_ROUTE = 'RENAME_ROUTE';
export const ADD_POINT_TO_ROUTE = 'ADD_POINT_TO_ROUTE';
export const REMOVE_POINT_FROM_ROUTE = 'REMOVE_POINT_FROM_ROUTE';
export const REORDER_POINTS = 'REORDER_POINTS';


export const createRoute = (title = null) => async (dispatch) => {
    if (!title) {
        return;
    }
    dispatch({type: CREATE_ROUTE, payload: {
        id: Date.now(),
        title: title,
        points: []
    }});
};

export const removeRoute = (routeId = null) => async (dispatch) => {
    if (!routeId) {
        return;
    }
    dispatch({type: REMOVE_ROUTE, payload: {
        id: routeId
    }});
};

export const renameRoute = (routeId = null, title = null) => async (dispatch) => {
    if (!routeId || !title) {
        return;
    }
    dispatch({type: RENAME_ROUTE, payload: {
        id: routeId,
        title: title,
    }});
};

export const addPointToRoute = (point = null, routeId = null, order = null) => async (dispatch) => {
    if (!point || !routeId || !Number.isInteger(order)) {
        return;
    }
    dispatch({type: ADD_POINT_TO_ROUTE, payload: {
        point: {...point, order: order},
        routeId: routeId,
    }});
};

export const removePointFromRoute = (pointId = null, routeId = null) => async (dispatch) => {
    if (!pointId || !routeId) {
        return;
    }
    dispatch({type: REMOVE_POINT_FROM_ROUTE, payload: {
        pointId: pointId,
        routeId: routeId,
    }});
};

export const reorderPoints = (routeId = null, points = null) => async (dispatch) => {
    if (!routeId || !points) {
        return;
    }
    dispatch({type: REORDER_POINTS, payload: {
        routeId: routeId,
        points: points
    }});
};

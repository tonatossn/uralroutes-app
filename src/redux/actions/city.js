import { getCityList, getCityItem } from '../../api/city';

export const GET_CITY_LIST = 'GET_CITY_LIST';
export const GET_CITY = 'GET_CITY';
export const SET_CITY_ORDERING = 'SET_CITY_ORDERING';

export const fetchCities = (page = null, params = {}) => async (dispatch) => {
    const data = await getCityList({page: page, ...params});
    dispatch({type: GET_CITY_LIST, payload: data});
}

export const fetchCityItem = (pk = null) => async (dispatch) => {
    const data = await getCityItem(pk);
    dispatch({type: GET_CITY, payload: data});
}

export const setCityOrdering = (field) => async (dispatch) => {
    dispatch({type: SET_CITY_ORDERING, payload: field});
}

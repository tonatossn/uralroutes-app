import { getPointList, getPointItem, getCategoriesList } from '../../api/point';

export const GET_POINT = 'GET_POINT';
export const GET_POINT_LIST = 'GET_POINT_LIST';
export const GET_CATEGORIES_LIST = 'GET_CATEGORIES_LIST';
export const GET_POINT_LIST_BY_CITY = 'GET_POINT_LIST_BY_CITY';
export const GET_POINT_LIST_BY_DISTANCE = 'GET_POINT_LIST_BY_DISTANCE';
export const GET_POINT_LIST_BIENNALE_CHOICE = 'GET_POINT_LIST_BIENNALE_CHOICE';
export const GET_POINT_LIST_FAVORITE = 'GET_POINT_LIST_FAVORITE';
export const SET_POINT_ORDERING = 'SET_POINT_ORDERING';
export const SET_POINT_FILTERS = 'SET_POINT_FILTERS';
export const SET_POINT_LIST_LIMIT = 'SET_POINT_LIST_LIMIT';

export const fetchPointItem = (id = null) => async (dispatch) => {
    const data = await getPointItem(id);
    dispatch({type: GET_POINT, payload: data});
}

export const fetchPoints = (page = null, params = {}) => async (dispatch) => {
    const data = await getPointList({page: page, ...params});   
    dispatch({type: GET_POINT_LIST, payload: data});
}

export const fetchPointsByCity = (cityIds = null) => async (dispatch) => {
    const data = await getPointList({ city__in: cityIds, limit: 10 });
    dispatch({type: GET_POINT_LIST_BY_CITY, payload: {cityIds: cityIds, data: data}});
}

export const fetchPointsByDistance = ({lat, lng} = {}) => async (dispatch) => {
    // /api/points/?ordering=geodata&point={lng},{lat}
    const data = await getPointList({ point: `${lng},${lat}`, ordering: 'geodata', limit: 10 });
    dispatch({type: GET_POINT_LIST_BY_DISTANCE, payload: data});
}
export const fetchPointsBiennaleChoice = () => async (dispatch) => {
    const data = await getPointList({ biennial_choice: true, limit: 10 });
    dispatch({type: GET_POINT_LIST_BIENNALE_CHOICE, payload: data});
}

export const fetchCategories = () => async (dispatch) => {
    const data = await getCategoriesList({ limit: 50 });
    dispatch({type: GET_CATEGORIES_LIST, payload: data})
}

export const fetchPointsFavorite = (page = null, params = {}) => async (dispatch) => {
    const data = await getPointList({page: page, ...params});   
    dispatch({type: GET_POINT_LIST_FAVORITE, payload: data});
}

export const setPointOrdering = (field) => async (dispatch) => {
    dispatch({type: SET_POINT_ORDERING, payload: field});
}

export const setPointFilters = (filters) => async (dispatch) => {
    dispatch({type: SET_POINT_FILTERS, payload: filters});
}

export const setPointsListLimit = (limit) => async (dispatch) => {
    dispatch({type: SET_POINT_LIST_LIMIT, payload: limit});
}
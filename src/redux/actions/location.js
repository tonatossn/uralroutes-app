import * as Location from 'expo-location';

export const GET_CITY_LOCATION = 'GET_CITY_LOCATION';

export const getAddressLocation = (id, address = null) => async (dispatch) => {
    if (!address) {
        return;
    }
    
    try {
        const geodata = await Location.geocodeAsync(address);
        if (geodata) {
            dispatch({type: GET_CITY_LOCATION, payload: {
                    id: id,
                    data: geodata[0]
                }
            });
        }
    } catch(e) {
        console.error(`Error geocode address: ${e}`)
    }
}

export async function collectCachedLocationByData(dispatch, data, locations) {
    if (!data) {
        return;
    }
    
    for (let k in data) {
        if (locations[data[k].id] != undefined) {
            continue;
        }
        const geodata = await Location.geocodeAsync(data[k].title);
        dispatch({ type: GET_CITY_LOCATION, payload: {
            id: data[k].id,
            data: geodata[0]
        } })
    }
}

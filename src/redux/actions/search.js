import { getPointList } from '../../api/point';
import { getRouteList } from '../../api/route';
import { getCityList } from '../../api/city';

export const GET_SEARCH_DATA = 'GET_SEARCH_DATA';
export const CLEAR_SEARCH_DATA = 'CLEAR_SEARCH_DATA';

export const fetchSearchData = (page = 1, params = {}) => async (dispatch) => {
    const points = await getPointList({page: page, ...params, limit: 20});
    const routes = await getRouteList({page: page, ...params, limit: 20});
    const cities = await getCityList({page: page, ...params, limit: 20});
    dispatch({type: GET_SEARCH_DATA, payload: {
        points: points,
        routes: routes,
        cities: cities,
    }});
}

export const clearSearchData = () => async (dispatch) => {
    dispatch({type: CLEAR_SEARCH_DATA});
}
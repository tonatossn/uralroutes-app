import { getRouteItem, getRouteList } from '../../api/route';
import { getScheduleList } from '../../api/schedule';

export const GET_ROUTE = 'GET_ROUTE';
export const GET_ROUTE_LIST = 'GET_ROUTE_LIST';
export const GET_ROUTE_LIST_BY_CITY = 'GET_ROUTE_LIST_BY_CITY';
export const GET_ROUTE_LIST_BY_DATE = 'GET_ROUTE_LIST_BY_DATE';
export const GET_ROUTE_LIST_BY_POINT = 'GET_ROUTE_LIST_BY_POINT';
export const GET_ROUTE_LIST_ON_INDEX = 'GET_ROUTE_LIST_ON_INDEX';
export const GET_ROUTE_LIST_FAVORITE = 'GET_ROUTE_LIST_FAVORITE';
export const SET_ROUTE_ORDERING = 'SET_ROUTE_ORDERING';
export const SET_ROUTE_FILTERS = 'SET_ROUTE_FILTERS';

export const fetchRouteItem = (id = null) => async (dispatch) => {
    const data = await getRouteItem(id);
    dispatch({type: GET_ROUTE, payload: data});
}

export const fetchRoutes = (page = null, params = {}) => async (dispatch) => {
    const data = await getRouteList({page: page, ...params});
    dispatch({type: GET_ROUTE_LIST, payload: data});
}

export const fetchRoutesByCity = (cityId = null) => async (dispatch) => {
    const data = await getRouteList({ points__city: cityId, limit: 10 });
    dispatch({type: GET_ROUTE_LIST_BY_CITY, payload: {cityId: cityId, data: data}});
}

export const fetchRoutesByDate = () => async (dispatch) => {
    const data = await getScheduleList({
        ordering: 'start',
        limit: 10,
        start__gte: new Date(Date.now()).toISOString()
    });
    dispatch({type: GET_ROUTE_LIST_BY_DATE, payload: data});
}

export const fetchRoutesByPoint = (pointId = null) => async (dispatch) => {
    const data = await getRouteList({ points: pointId, limit: 10 });
    dispatch({type: GET_ROUTE_LIST_BY_POINT, payload: {pointId: pointId, data: data}});
}

export const fetchRoutesOnIndex = () => async (dispatch) => {
    const data = await getRouteList({ ordering: '-on_index', limit: 10 });
    dispatch({type: GET_ROUTE_LIST_ON_INDEX, payload: data});
}

export const fetchRoutesFavorite = (page = null, params = {}) => async (dispatch) => {
    const data = await getRouteList({page: page, ...params});
    dispatch({type: GET_ROUTE_LIST_FAVORITE, payload: data});
}

export const setRouteOrdering = (field) => async (dispatch) => {
    dispatch({type: SET_ROUTE_ORDERING, payload: field});
}

export const setRouteFilters = (filters) => async (dispatch) => {
    dispatch({type: SET_ROUTE_FILTERS, payload: filters});
}


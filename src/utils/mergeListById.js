export const mergeListById = (objectA = [], objectB = [], key = 'results') => {
    const mergeList = (subListA, subListB) => {
        return [
            ...subListA,
            ...subListB.filter(
                (item) => !subListA.some(( even ) => even.id === item.id)
            ),
        ]
    }
    
    if (objectA.constructor.name == 'Object' && objectB.constructor.name == 'Object') {
        return {
            ...objectA,
            ...objectB,
            [key]: [
                ...mergeList(objectA[key], objectB[key])
            ]
        }
    } else {
        return mergeList(objectA, objectB)
    }
}

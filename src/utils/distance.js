import Intl from 'intl';
import 'intl/locale-data/jsonp/ru-RU';

const LOCALE = 'ru-RU';
const HALF_CIRCLE_DAGREE = 180;
const STATUTE_MILE_KOEFFICIENT = 1.1515;
const KM_MODIFIER = 1.609344;
const NAUTICAL_MILE_MODIFIER = 0.8684;

const geoDistance = ({from = {lat, lon}, to = {lat, lon}} = {}) => {
    if (
        from.lat === null || from.lon === null ||
        to.lat === null || to.lon === null
    ) {
        return null
    }

	if ((from.lat == to.lat) && (from.lon == to.lon)) {
		return 0;
	}

    const radlat1 = Math.PI * from.lat / HALF_CIRCLE_DAGREE;
    const radlat2 = Math.PI * to.lat / HALF_CIRCLE_DAGREE;
    const theta = from.lon - to.lon;
    const radtheta = Math.PI * theta / HALF_CIRCLE_DAGREE;
    
    let dist = (
        Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1)
        * Math.cos(radlat2) * Math.cos(radtheta)
    );

    dist = dist > 1 ? 1 : dist;
    dist = Math.acos(dist) * HALF_CIRCLE_DAGREE / Math.PI * 60  * STATUTE_MILE_KOEFFICIENT;
    
    return dist;
}

const humanizeDistance = (dist, unit = 'K', needModifier = true) => {
    if (isNaN(dist) || dist === null || !dist) {
        return 0
    }

    let result = {
        distance: dist,
        unit: 'мл',
        smallUnit: 'ярд',
        factor: 1760,
        smallBorder: 0.5
    }

    switch (unit) {
        // Километры
        case 'K':
            result = {
                distance: needModifier ? dist * KM_MODIFIER : dist,
                unit: 'км',
                smallUnit: 'м',
                factor: 1000,
                smallBorder: 0.9
            }
            break;
        
        // Морские мили
        case 'NM':
            result = {
                ...result,
                distance: needModifier ? dist * NAUTICAL_MILE_MODIFIER : dist,
            }
            break;
        
        // Обычные мили
        case 'SM':
        default:
            break;
    }
    
    const formatter = new Intl.NumberFormat(LOCALE, { maximumSignificantDigits: 2 })
    
    if (result.distance < result.smallBorder) {
        let distance = result.distance * result.factor
        if (distance < 40) return `рядом с вами`
  
        distance = Math.round(distance / 50) * 50
        return `≈ ${formatter.format(distance)} ${result.smallUnit}`
    } else {
        return `≈ ${formatter.format(result.distance)} ${result.unit}`;
    }
}

const listItemDistanceString = (distance, fromMe = false) => {
    const distanceText = humanizeDistance(distance, 'K', false);
    return distanceText
        ? `В пути: ${distanceText}${fromMe ? ' от вас' : ''}`
        : '...';
}

export {
    geoDistance,
    humanizeDistance,
    listItemDistanceString,
};
export const resetPropertyOfFeatures = (collection, property = {}) => {
    return {
        ...collection,
        features: collection.features.map(f => {
            return {
                ...f,
                properties: {
                    ...f.properties,
                    ...property
                }
            }
        })
    }
}

export const updatePropertyOfFeature = (collection, featureId, property = {}) => {
    const feature = collection.features.filter(f => f.properties.id === featureId)[0];
    return {
        ...collection,
        features: [
            ...collection.features.filter(f => f.properties.id != featureId),
            {
                ...feature,
                properties: {
                    ...feature.properties,
                    ...property
                }
            }
        ]
    }
}

import * as React from 'react';
import { StyleSheet, View, Text, Dimensions } from 'react-native';

import DefaultStyles from '../constance/styles';
import { TextContent } from '../components/content/TextContent';
import { SocialNetworks } from '../components/elements/SocialNetworks';

export const About = ({ data }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.header}>{data.title}</Text>
            <View style={styles.content}>
                <TextContent content={data.content} collapsable={true} />
            </View>

            <Text style={styles.headerMiddle}>Контакты</Text>
            <View style={styles.contact}>
                {data.contacts.map(ct => (
                    <View style={styles.contactRow} key={`${ct.id}`}>
                        <Text style={styles.contactKey}>{ct.title}</Text>
                        <Text style={styles.contactValue}>{ct.value}</Text>
                    </View>
                ))}
            </View>

            <View styles={styles.social}>
                <SocialNetworks data={data.socials} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 16,
    },
    header: {
        ...DefaultStyles.header,
        marginBottom: 24
    },
    content: {
        marginBottom: 32
    },
    headerMiddle: {
        ...DefaultStyles.headerMiddle,
        marginBottom: 24
    },
    contact: {
        marginBottom: 24
    },
    contactRow: {
        flexDirection: 'row',
        marginBottom: 12,
    },
    contactKey: {
        width: 140,
        ...DefaultStyles.fontBold,
        fontSize: 16
    },
    contactValue: {
        flexWrap: 'wrap',
        width: Dimensions.get('window').width - 140,
        ...DefaultStyles.regularFont,
        fontSize: 16
    },
    social: {
        flex: 1,
    }
})
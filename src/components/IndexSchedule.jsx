import React from 'react';
import { StyleSheet, Text, View, Dimensions} from 'react-native';
import {
    TabView,
    TabBar,
  } from 'react-native-tab-view';
import moment from 'moment';
import 'moment/locale/ru';

import { RouteListItem } from './RouteListItem';
import DefaultStyles, { BACKGROUND_COLOR, DEFAULT_COLOR } from '../constance/styles';
import { AppButton } from './elements/AppButton';

export const IndexSchedule = ({ navigation, title, days = [] }) => {
    if (!days || days.length === 0) {
        return <></>;
    }
    
    const [ index, setIndex ] = React.useState(0);
    const [ routes ] = React.useState(
        days.map((d, i) => {
            return {
                key: i,
                title: moment(d.start).format('D MMMM, в HH:MM')
            }
        })
    );
    
    return (
        <View style={styles.container}>
            <View style={styles.backdrop}></View>
            {title ? <Text style={styles.title}>{title}</Text> : ''}
            <Text style={styles.subtitle}>Путешествуйте вместе с Уральской биеннале.</Text>

            <TabView
                lazy
                navigationState={{index, routes}}
                renderScene={({ route }) => {
                    const day = days[route.key];
                    return (
                        <View
                            style={{
                                ...styles.item
                            }}>
                            <RouteListItem
                                item={day.route}
                                navigation={navigation}
                                removeItemMargin={true}
                                extraScheduleDay={{
                                    id: day.id,
                                    start: day.start,
                                    available_places: day.available_places,
                                }}
                            />
                        </View>
                    )
                }}
                renderTabBar={props => <TabBar
                    {...props}
                    scrollEnabled
                    indicatorStyle={styles.indicator}
                    style={styles.tabbar}
                    tabStyle={styles.tab}
                    labelStyle={styles.label}
                    activeColor={BACKGROUND_COLOR}
                />}
                onIndexChange={setIndex}
                style={styles.scrollContainer}
            />
            <View style={styles.button}>
                <AppButton title='Все туры' onPress={() => {
                    return navigation.navigate(
                        'Route',
                        {
                            screen: 'RouteList',
                            params: {
                                tabIndex: 1
                            }
                        }
                    )
                }}/>
            </View>
        </View>
    )
};

const ITEM_HEIGHT = 500;

const styles = StyleSheet.create({
	container: {
        paddingTop: 40,
        marginBottom: 16
	},
    title: {
        ...DefaultStyles.font,
        fontSize: 24,
        marginBottom: 8,
        marginLeft: 16
    },
    subtitle: {
        marginLeft: 16,
        marginBottom: 16,
        color: `${BACKGROUND_COLOR}`
    },
    scrollContainer: {
        height: ITEM_HEIGHT + 80
    },
    tabbar: {
        backgroundColor: null,
        marginBottom: 16,
    },
    tab: {
        textTransform: null,
        width: 'auto',
        padding: 0,
        paddingHorizontal: 12,
    },
    tabActive: {
        backgroundColor: BACKGROUND_COLOR
    },
    label: {
        ...DefaultStyles.font,
        color: `${BACKGROUND_COLOR}80`,
        textTransform: 'lowercase',
        fontSize: 16,
    },
    indicator: {
        backgroundColor: BACKGROUND_COLOR,
    },
    item: {
        flex: 1,
        margin: 16,
        top: -16,
        transform: [
            { perspective: 600 },
        ],
    },
    button: {
        marginHorizontal: 16,
        marginBottom: 24
    },
    backdrop: {
        flex: 1,
        backgroundColor: DEFAULT_COLOR,
        width: Dimensions.get('window').width,
        height: 300,
        position: 'absolute',
        top: 0,
        right: 0,
        transform: [
            { perspective: 500 },
            // { translateX: Dimensions.get('window').width * 0.24 },
            { translateY: 0 },
            { rotateY: '-45deg' },
            { rotateZ: '8deg' },
            { scaleX: 20 },
        ],
        zIndex: -1000
    },
});

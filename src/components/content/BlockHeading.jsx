import React from 'react';
import { StyleSheet, Text, View} from 'react-native';

import DefaultStyles from '../../constance/styles';


export const BlockHeading = ({ content }) => {
    return (
        <View style={styles.container}>
            <Text
                style={{
                    ...styles.heading,
                    fontSize: SizeByLevel[content.data.level]
                }}>
                {content.data.text}
            </Text>
        </View>
    );
};

const SizeByLevel = {
    2: 30,
    3: 24,
}
const styles = StyleSheet.create({
    container: {},
    heading: {
        ...DefaultStyles.header,
        marginTop: 12,
        marginBottom: 12
    },
});

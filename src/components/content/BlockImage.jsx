import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, Image, View} from 'react-native';

import config from '../../constance/config';
import DefaultStyles from '../../constance/styles';


export const BlockImage = ({ content }) => {
    const imageUri = `${config.HOST_NAME}${content.data.file.url}`;
    const [{width, height}, setSize] = useState(1);
    
    useEffect(() => {
        if (content.data.file.url === undefined) {
            return;
        }
        Image.getSize(imageUri, (width, height) => {
            setSize({width: width, height: height})
        });
    }, [])

    return (
        height && width ? <View style={styles.container}>
            <Image
                resizeMode='contain'
                style={{
                    ...styles.image,
                    ...{aspectRatio: width / height}
                }}
                source={{ uri: imageUri }}
            />
            <Text style={styles.subscribe}>{content.data.caption}</Text>
        </View>
        : <></>
    );
};

const styles = StyleSheet.create({
    container: {
        marginTop: 12,
        marginBottom: 24,
    },
    image: {
        marginBottom: 12,
        width: '100%',
        backgroundColor: '#eaeaea',
    },
    subscribe: {
        ...DefaultStyles.fontItalic,
        color: '#999',
        fontSize: 16,
    }
});

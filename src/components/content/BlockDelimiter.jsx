import React from 'react';
import { StyleSheet, Text, View} from 'react-native';

import DefaultStyles, { DEFAULT_COLOR } from '../../constance/styles';


export const BlockDelimiter = ({ content }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.delimiter}>* * *</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginTop: 12
    },
    delimiter: {
        ...DefaultStyles.font,
        fontSize: 32,
        textAlign: 'center',
        color: DEFAULT_COLOR
    }
});

import React, { useState, useRef, useEffect } from 'react';
import { Animated, TouchableOpacity, StyleSheet, Text, View} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';

import DefaultStyles, { DEFAULT_COLOR, BACKGROUND_COLOR } from '../../constance/styles';
import { BlockDelimiter } from './BlockDelimiter';
import { BlockEmbed } from './BlockEmbed';
import { BlockHeading } from './BlockHeading';
import { BlockImage } from './BlockImage';
import { BlockList } from './BlockList';
import { BlockParagraph } from './BlockParagraph';
import { BlockQuote } from './BlockQuote';
import { BlockLink } from './BlockLink';


const BlockTypeMapping = {
    'paragraph': BlockParagraph,
    'Image': BlockImage,
    'Header': BlockHeading,
    'List': BlockList,
    'Quote': BlockQuote,
    'Delimiter': BlockDelimiter,
    'Embed': BlockEmbed,
    'LinkTool': BlockLink,
}

const stripContent = (str) => {
    if (str === null || str === '') {
        return str;
    }
    return str
            .toString()
            .replace(/(<br.*>)/ig, '')
            .replace(/(\&nbsp\;)/ig, ' ')
            .replace(/\r\n/ig, ' ')
            .replace(/(<\/?a[^>]*>)/ig, '');
}

const BaseContainerHeight = 250;

export const TextContent = ({ content, collapsable = true} = {}) => {
    const [collapsed, setCollapsed] = useState(true);
    const _onPressCollapse = () => {
        setCollapsed(!collapsed);
    }

    if (content == null || content == 'undefined') {
        return <></>;
    }
    
    return (
        <View style={styles.container}>
            <Animated.View
                style={{
                    ...styles.content,
                    ...(collapsable === true && collapsed ? styles.contentHidden : styles.contentShowed),
            }}>
                {content.blocks.map(block => {
                    if (typeof BlockTypeMapping[block.type] === "undefined") {
                        return <Text key={block.id}></Text>;
                    }
                    return React.createElement(BlockTypeMapping[block.type], {
                        key: block.id,
                        content: block,
                        collapsed: collapsed,
                        stripFunction: stripContent,
                    });
                })}
            </Animated.View>
            {collapsable ? (
                <TouchableOpacity
                    onPress={_onPressCollapse}
                    activeOpacity={1}
                    style={{...styles.collapse, ...(collapsed ? styles.collapseHidden : styles.collapseShowed)}}>
                    <LinearGradient
                        colors={[`${BACKGROUND_COLOR}00`, BACKGROUND_COLOR]}
                        locations={[0, 0.8]}
                        style={styles.collapseGradient}
                    >
                        <Ionicons name={collapsed ? 'chevron-down-outline' : 'chevron-up-outline'} style={styles.collapseIcon} size={24} />
                        <Text style={styles.collapseText}>{collapsed ? 'Развернуть' : 'Свернуть'}</Text>
                    </LinearGradient>
                </TouchableOpacity>
            ) : <></>}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginBottom: 24
    },
    content: {
    },
    contentHidden: {
        maxHeight: BaseContainerHeight,
        overflow: 'hidden',
    },
    contentShowed: {
        height: 'auto',
        overflow: 'visible',
        marginBottom: -24
    },
    collapse: {
        height: 100,
        marginTop: -100
    },
    collapseHidden: {
    },
    collapseShowed: {
        height: 40,
        marginTop: 0
    },
    collapseGradient: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    collapseText: {
        color: DEFAULT_COLOR,
        ...DefaultStyles.font,
        fontSize: 16
    },
    collapseIcon: {
        color: DEFAULT_COLOR,
        top: 4
    },
});

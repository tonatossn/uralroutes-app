import React from 'react';
import { StyleSheet, View} from 'react-native';
import StyledText from 'react-native-styled-text';

import DefaultStyles from '../../constance/styles';


export const BlockParagraph = ({ content, stripFunction }) => {
    return (
        <View style={styles.container}>
            <StyledText style={styles.paragraph}>{stripFunction(content.data.text)}</StyledText>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginBottom: 12
    },
    paragraph: {
        ...DefaultStyles.regularFont,
        fontSize: 16
    }
});

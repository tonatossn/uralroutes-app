import React from 'react';
import { StyleSheet, Text, View} from 'react-native';

import DefaultStyles from '../../constance/styles';


export const BlockList = ({ content, stripFunction }) => {
    return (
        <View style={styles.container}>
            {content.data.items.map((item, i) => {
                return (
                    <View style={styles.listItem} key={`${content.id}-${i}`}>
                        <Text style={styles.listBullet}>{'\u2022'}</Text>
                        <Text style={styles.listText}>{stripFunction(item)}</Text>
                    </View>
                )
            })}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginBottom: 12
    },
    listItem: {
        marginBottom: 12,
        paddingLeft: 32
    },
    listBullet: {
        position: 'absolute',
        marginLeft: 12,
        fontSize: 24,
        marginTop: -5,
        ...DefaultStyles.fontBold,
    },
    listText: {
        ...DefaultStyles.regularFont,
        fontSize: 16,
    }
});

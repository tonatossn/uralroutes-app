import React from 'react';
import { Dimensions, StyleSheet, Text, View} from 'react-native';
import { WebView } from 'react-native-webview';

import DefaultStyles from '../../constance/styles';


export const BlockEmbed = ({ content, collapsed }) => {
    const calcHeight = (width, height) => {
        const windowWidth =  Dimensions.get('window').width / Dimensions.get('window').scale;
        return {
            width: '100%',
            height: width / height * windowWidth
        }
    }
    return (
        !collapsed ? (
            <View style={styles.container}>
                <WebView 
                    style={{...styles.webview, ...calcHeight(content.data.width, content.data.height)}}
                    allowsFullscreenVideo={true}
                    source={{ uri: content.data.embed }}
                    // androidHardwareAccelerationDisabled={true}
                />
                <Text style={styles.subscribe}>{content.data.caption}</Text>
            </View>
        ) : <></>
    );
};

const styles = StyleSheet.create({
    container: {
        marginBottom: 12,
        marginLeft: -16,
        marginRight: -16,
    },
    webview: {
        width: '100%',
        height: 320,
        marginBottom: 12,
        opacity: 0.99,
        overflow: 'hidden'
    },
    subscribe: {
        ...DefaultStyles.fontItalic,
        color: '#999',
        fontSize: 16,
        marginLeft: 16
    }
});

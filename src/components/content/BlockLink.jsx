import React from 'react';
import { StyleSheet, Text, Linking, TouchableOpacity, Image } from 'react-native';

import DefaultStyles from '../../constance/styles';


const getHostFromUrl = (url) => {
    try {
        return url.match(/(\w+\.\w+)/)[1]
    } catch (e) {
        return url;
    }
}

export const BlockLink = ({ content }) => {
    const handleClick = () => {
        const url = content.data.link;
        Linking.canOpenURL(url).then(supported => {
            Linking.openURL(url);
        });
    };

    return (
        <TouchableOpacity style={styles.container} onPress={handleClick} activeOpacity={0.8}>
            <Text style={styles.title}>{content.data.meta.title}</Text>
            {content.data.meta.image ? (
                <Image
                    style={{
                        ...styles.image,
                        aspectRatio: content.data.meta.image.width / content.data.meta.image.height
                    }}
                    resizeMode='contain'
                    source={{ uri: content.data.meta.image.url }}
                />
            ) : <></>}
            <Text style={styles.description}>{content.data.meta.description}</Text>
            <Text style={styles.link}>{getHostFromUrl(content.data.link)}</Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        marginBottom: 24,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 8},
        shadowOpacity: 0.29,
        elevation: 12,
        shadowRadius: 6,
        backgroundColor: '#fff',
        borderRadius: 12,
        padding: 12,
        paddingVertical: 18
    },
    title: {
        ...DefaultStyles.font,
        fontSize: 16,
        marginBottom: 8
    },
    description: {
        ...DefaultStyles.regularFont,
        fontSize: 14,
        color: '#000',
        marginBottom: 8
    },
    link: {
        ...DefaultStyles.font,
        color: '#999',
        fontSize: 14
    },
    image: {
        width: '100%',
        marginBottom: 8
    },
});

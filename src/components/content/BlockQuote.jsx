import React from 'react';
import { StyleSheet, Text, View, Platform} from 'react-native';

import DefaultStyles, { DEFAULT_COLOR } from '../../constance/styles';


export const BlockQuote = ({ content, stripFunction }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.decoration}>{'“'}</Text>
            <Text style={styles.quote}>{stripFunction(content.data.text)}</Text>
            <View style={styles.splitter}></View>
            <Text style={styles.subscribe}>{stripFunction(content.data.caption)}</Text>
        </View>
    );
};

let styles = StyleSheet.create({
    container: {
        marginBottom: 12,
        flex: 1,
        alignItems: 'center',
    },
    quote: {
        fontFamily: 'Rubik_400Regular_Italic',
        fontSize: 20,
        textAlign: 'center',
        fontStyle: 'italic',
        color: DEFAULT_COLOR,
        marginBottom: 12
    },
    subscribe: {
        ...DefaultStyles.font,
        fontSize: 14,
        textAlign: 'center',
        color: '#999',
        fontSize: 16,
        textTransform: 'uppercase',
    },
    decoration: {
        width: 50,
        height: 50,
        marginBottom: 12,
        lineHeight: 66,
        textAlign: 'center',
        overflow: 'hidden',
        borderRadius: 25,
        fontSize: 50,
        backgroundColor: DEFAULT_COLOR,
        color: '#fff',
        fontFamily: 'serif',
    },
    splitter: {
        borderTopColor: '#999',
        borderTopWidth: 1,
        flex: 1,
        width: 100,
        marginTop: 8,
        marginBottom: 20,
    }
});

if (Platform.OS === 'ios') {
    styles = {
        ...styles,
        decoration: {
            ...styles.decoration,
            lineHeight: 70,
            fontFamily: 'Arial Rounded MT Bold',
        }
    }
}
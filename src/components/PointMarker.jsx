import React from 'react';
import { StyleSheet, TouchableOpacity, Text, View, Image } from 'react-native';
import MapboxGL from '@react-native-mapbox-gl/maps';



export const PointMarker = ({ coord, icon = null, size=50, id = null, onPress=null }) => {
    const {width, height} = {width: size * 0.8, height: size}
    const marker = require('../../assets/point_marker.png');
    return (
        <MapboxGL.MarkerView coordinate={[coord.lng, coord.lat]} id={id}>
            <View
                style={{
                    ...styles.container,
                    ...{width, height},
                    top: -height
                }}>
                <TouchableOpacity
                    style={styles.touchable}
                    activeOpacity={0.7}
                    onPress={onPress != null ? onPress : () => {}}
                >
                    <Image
                        resizeMode='contain'
                        source={marker}
                        style={{
                            ...styles.marker,
                            ...{width, height}
                        }}
                    />
                </TouchableOpacity>
            </View>
        </MapboxGL.MarkerView>
    )
}

const styles = StyleSheet.create({
    container: {
        width: 40,
        position: 'absolute',
        top: -50
    },
    touchable: {

    },
    marker: {
        height: 50,
        width: 40,
    },
    icon: {
        // position: 'absolute',
    }
});


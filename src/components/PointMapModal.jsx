import React from 'react';
import { StyleSheet, Text, TouchableOpacity, Image } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Modal from "react-native-modal";
import DefaultStyles from '../constance/styles';

import { geoDistance, listItemDistanceString } from '../utils/distance';


export const PointMapModal = ({
    navigation,
    isVisible,
    setIsVisible,
    item,
    userLocation,
    hasBackdrop = false,
    coverScreen = false
} = {}) => {
    if (Object.keys(item).length === 0) {
        return <></>;
    }
    return (
        <Modal
            swipeDirection={["down", "up"]}
            hardwareAccelerated={true}
            coverScreen={coverScreen}
            hasBackdrop={hasBackdrop}
            isVisible={isVisible}
            onModalHide={() => navigation.setParams({visible: true})}
            onSwipeComplete={({swipingDirection} ) => {
                if (swipingDirection == 'down') {
                    setIsVisible(false)
                } else if (swipingDirection == 'up') {
                    return navigation.navigate('Point', {
                        screen: 'PointItem',
                        initial: false,
                        params: {
                            id: item.id
                        }
                    });
                }
            }}
            style={styles.modal}
        >
            {/* <View
                // pointerEvents="none"
                style={styles.modalWrapper}
            > */}
                <TouchableOpacity
                    // pointerEvents="none"
                    style={styles.modalContainer}
                    activeOpacity={0.9}
                    onPress={() => {
                        return navigation.navigate('Point', {
                            screen: 'PointItem',
                            initial: false,
                            params: {
                                id: item.id
                            }
                        });
                    }}
                >
                    <Ionicons
                        name="close-circle-outline"
                        color={'#000'}
                        size={32}
                        onPress={() => setIsVisible(false)}
                        style={styles.close}
                    />

                    { item.cover ? <Image
                        style={{...styles.image}}
                        resizeMode={'cover'}
                        source={{
                            uri: item.cover
                        }}
                    /> : null }
                    <Text style={styles.title}>{item.title}</Text>

                    <Text style={styles.underline}>
                        {item.category.map(category => (
                            <Text style={styles.category} key={`category-${category.id}`}>{category.title}, </Text>
                        ))}
                        
                        <Text style={styles.distance}>{listItemDistanceString(
                                geoDistance(
                                    {
                                        from: { lat: userLocation.latitude, lon: userLocation.longitude},
                                        to: { lat: item.geodata.coordinates[1], lon: item.geodata.coordinates[0] }
                                    },
                                ), true
                            )}</Text>
                    </Text>
                </TouchableOpacity>
            {/* </View> */}
        </Modal>
    )
}

const styles = StyleSheet.create({
    modal: {
        margin: 0,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    modalWrapper: {
        flex: 1,
        padding: 0,
        margin: 0,
    },
    modalContainer: {
        backgroundColor: '#fff',
        height: 200,
        width: '100%',
        padding: 16,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        overflow: 'hidden',
    },
    close: {
        position: 'absolute',
        right: 12,
        top: 12,
        elevation: 10,
        zIndex: 10
    },
    image: {
        height: 100,
        marginHorizontal: -16,
        marginTop: -16,
        marginBottom: 16
    },
    title: {
        ...DefaultStyles.fontBold,
        fontSize: 16,
        marginBottom: 8,
    },
    category: {
        ...DefaultStyles.fontBold,
        color: '#999'
    },
    distance: {
        ...DefaultStyles.regularFont,
        color: '#999'
    }
})
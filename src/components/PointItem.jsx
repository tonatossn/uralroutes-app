import React, { useState, useEffect, useContext, useRef } from 'react';
import {
    Animated,
    StyleSheet,
    TouchableOpacity,
    Text,
    View,
    Dimensions,
    Linking
} from 'react-native';
import { SliderBox } from "react-native-image-slider-box";
import * as Clipboard from 'expo-clipboard';
import MapboxGL from '@react-native-mapbox-gl/maps';
import { Ionicons } from '@expo/vector-icons';

import config from '../constance/config';
import DefaultStyles from '../constance/styles';
import { geoDistance, listItemDistanceString } from '../utils/distance';
import { GeoContext } from '../containers/GeoWrapper';
import { TextContent } from './content/TextContent';
import { PointMarker } from './PointMarker';
import { DEFAULT_COLOR } from '../constance/styles';
import { ImageModal } from './ImageModal';
import { ButtonShareObject } from './elements/ButtonShareObject';
import { ButtonAddToMyRoute } from './elements/ButtonAddToMyRoute';


MapboxGL.setAccessToken(config.MAPBOX_ACCESS_TOKEN);

const MetadataItem = ({ item, title, wrapFun = (a) => a }) => {
    if (!item) {
        return <></>
    }
    return (
        <Text style={styles.metadataItem}>
            <Text style={styles.metadataItemKey}>{title} </Text>
            {wrapFun(item)}
        </Text>
    )
}

export const PointItem = ({ navigation, item }) => {
    const location = useContext(GeoContext);
    const mapRef = useRef(null)
    const { lat, lng } = {lat: item.geodata.coordinates[1], lng: item.geodata.coordinates[0]};
    const [ showCopiedSuccess, setShowCopiedSuccess ] = useState(false);
    const fadeAnim = useRef(new Animated.Value(0)).current;
    const [ curentZoomImage, setCurentZoomImage ] = useState(0)
    const [ zoomImageStatus, setZoomImageStatus] = useState(false)

    const copyToClipboard = () => {
        Clipboard.setString(`${lat}, ${lng}`);
        setShowCopiedSuccess(true);
    };

    useEffect(() => {
        Animated.timing(fadeAnim, {
            toValue: 1,
            duration: 150,
            useNativeDriver: true,
        }).start();

        const hideSuccessInterval = setInterval(() => {
            Animated.timing(fadeAnim, {
                toValue: 0,
                duration: 150,
                useNativeDriver: true,
            }).start();

            setShowCopiedSuccess(false);
        }, 1000);
        return () => clearInterval(hideSuccessInterval);
    }, [showCopiedSuccess])

    const getHumanAilability = (availability) => {
        const availabilityMap = {
            'open': 'место открыто, свободное посещение',
            'part_open': 'в составе тура  или маршрута биеннале',
            'closed': 'закрытое место',
            'fully_closed': 'разрешен только внешний осмотр',
        }
        return availabilityMap[availability];
    }
    
    return (
        <View style={styles.container}>
            <ImageModal
                images={[item.cover, ...item.images.map((item) => item.image )]}
                index={curentZoomImage}
                status={zoomImageStatus}
                setStatus={setZoomImageStatus}
            />
            <Text style={styles.header}>
                {item.title}
            </Text>
            <Text style={styles.underline}>
                {listItemDistanceString(
                    geoDistance(
                        {
                            from: { lat: location.latitude, lon: location.longitude},
                            to: { lat: lat, lon: lng }
                        },
                    ), true
                )}
            </Text>

            {item.biennial_choice ? (
                <View style={styles.biennialChoiceContainer}>
                    <View style={styles.biennialChoice}>
                        <Ionicons name={'star'} size={16} color={DEFAULT_COLOR} />
                        <Text style={styles.biennialChoiceText}>Выбор биеннале</Text>
                    </View>
                </View>
            ) : <></>}

            <View style={styles.gallery}>
                { item.cover ? <SliderBox
                    style={{aspectRatio: 4/3}}
                    imageLoadingColor={DEFAULT_COLOR}
                    images={[item.cover, ...item.images.map((item) => item.image )]}
                    onCurrentImagePressed={index => {
                        setCurentZoomImage(index);
                        setZoomImageStatus(true);
                    }}
                /> : null }
            </View>

            {item.cover_copyright ? (
                <Text style={styles.copyright}>{item.cover_copyright}</Text>
            ) : <></>}

            {item.description ? <Text style={styles.description}>{item.description}</Text> : <></>}
            <TextContent content={item.content} />

            <View style={styles.metadata}>
                <MetadataItem item={item.city?.title} title='Находится в городе' />
                <MetadataItem item={item.availability} title='Доступность:' wrapFun={getHumanAilability} />
                <MetadataItem item={item.address} title='Контакты и условия посещения:' />
                <MetadataItem item={item.opening_hours} title='Часы работы:' />
                <MetadataItem item={item.phones} title='Телефон(ы):' />
                <MetadataItem item={item.how_to_get} title='Как попасть:' />

                {item.contacts?.length ? 
                    <View style={styles.contact}>
                        {item.contacts?.map(ct => (
                            <View style={styles.contactRow} key={`${ct.id}`}>
                                <Text style={styles.contactKey}>{ct.title}:</Text>
                                <TouchableOpacity onPress={() => {
                                    if (ct.contact == 'email') {
                                        ct.link = `mailto:${ct.link}`
                                    }
                                    Linking.canOpenURL(ct.link).then(supported => {
                                        Linking.openURL(ct.link);
                                    });
                                }}>
                                    <Text style={styles.contactValue}>{ct.link}</Text>
                                </TouchableOpacity>
                            </View>
                        ))}
                    </View>
                : <></>}
            </View>

            <View style={styles.buttonBar}>
                {/* <ButtonShareObject item={item} /> */}
                <ButtonAddToMyRoute item={item} navigation={navigation} />
            </View>

            <MapboxGL.MapView
                styleURL={'mapbox://styles/mapbox/dark-v10'} 
                style={styles.map}
                localizeLabels={true}
                ref={mapRef}
                scrollEnabled={false}
            >
                <MapboxGL.Camera
                    zoomLevel={10}
                    centerCoordinate={[lng, lat]}
                />
                <PointMarker coord={{lng, lat}} id={'point-item-marker'} />
            </MapboxGL.MapView>
            
            <TouchableOpacity style={styles.coordinates}>
                <Text style={styles.coordinatesText}>
                    Координаты: <Text style={styles.coordinatesLink} onPress={copyToClipboard}>
                        {lng.toFixed(6)}, {lat.toFixed(6)}
                    </Text>
                </Text>
                {showCopiedSuccess ? (
                        <Animated.View style={{...styles.clipboardSuccess, opacity: fadeAnim}}>
                            <Text>Координаты скопированы</Text>
                        </Animated.View>
                    ) : <></>
                }
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        padding: 16,
        flex: 1,

    },
    header: {
        ...DefaultStyles.header,
    },
    underline: {
        color: '#999',
        fontSize: 12,
        marginBottom: 16,
    },
    distance: {},
    gallery: {
        marginBottom: 16,
        marginLeft: -16

    },
    lead: {},
    description: {
        ...DefaultStyles.fontBold,
        marginBottom: 24,
        fontSize: 20
    },
    copyright: {
        ...DefaultStyles.regularFont,
        fontSize: 12,
        marginTop: -8,
        marginBottom: 16,
        color: '#999',
    },
    biennialChoiceContainer: {
        height: 32,
        bottom: 16,
        marginBottom: -32,
        right: 0,
        zIndex: 10,
        alignItems: 'flex-end'
    },
    biennialChoice: {
        ...DefaultStyles.buttonContainer,
        flex: 1,
        backgroundColor: '#fff',
        width: 144,
        paddingHorizontal: 8,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    biennialChoiceText: {
        ...DefaultStyles.fontBold,
        fontSize: 12,
        color: DEFAULT_COLOR
    },
    metadata: {
        backgroundColor: `${DEFAULT_COLOR}50`,
        paddingHorizontal: 16,
        paddingTop: 16,
        marginBottom: 16
    },
    metadataItem: {
        marginBottom: 16,
        ...DefaultStyles.regularFont,
        fontSize: 16
    },
    metadataItemKey: {
        ...DefaultStyles.fontBold,
        paddingRight: 8
    },
    map: {
        height: 300,
        width: 'auto',
        marginTop: 12,
        marginBottom: 12,
        marginLeft: -16,
        marginRight: -16,
    },
    coordinates: {
        marginBottom: 12,
    },
    coordinatesText: {},
    coordinatesLink: {
        color: DEFAULT_COLOR
    },
    clipboardSuccess: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        width: 200,
        height: 40,
        left: '50%',
        marginLeft: -100,
        backgroundColor: '#fff',
        borderRadius: 8,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 8 },
        shadowOpacity: 0.29,
        elevation: 12,
        zIndex: 10,
    },
    buttonBar: {
        marginBottom: 16,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    contact: {
        marginBottom: 8
    },
    contactRow: {
        flexDirection: 'row',
        marginBottom: 16,
    },
    contactKey: {
        ...DefaultStyles.fontBold,
        fontSize: 16,
        marginRight: 5
    },
    contactValue: {
        flexWrap: 'wrap',
        width: Dimensions.get('window').width - 140,
        ...DefaultStyles.regularFont,
        fontSize: 16,
        textDecorationLine: 'underline'
    },
});

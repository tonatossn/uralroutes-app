import React, { useEffect, useRef, useState } from 'react';
import MapboxGL from '@react-native-mapbox-gl/maps';
import { View, StyleSheet } from 'react-native';

import { resetPropertyOfFeatures, updatePropertyOfFeature } from '../utils/featureTools';
import { PointMapModal } from './PointMapModal';
import { DEFAULT_COLOR } from '../constance/styles';

export const PointMap = ({ navigation, points = []}) => {
    const [ userLocation, setUserLocation ] = useState({});
    const [ modalIsVisible, setModalIsVisible ] = useState(false);
    const [ modalData, setModalData ] = useState({})
    const [ shape, setShape ] = useState(null);
    const [ followUserLocation, setFollowUserLocation ] = useState(true)
    
    const mapRef = useRef(null)
    const cameraRef = useRef(null)
    const shapeSourceRef = useRef(null);

    useEffect(() => {
        setShape({
            'type': 'FeatureCollection',
            'features': [
                ...points.map(point => (
                    {
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Point',
                            'coordinates': point.geodata.coordinates
                        },
                        'properties': {
                            ...point,
                            'iconSize': 0.4,
                        },
                    }
                ))
            ]
        });
    }, [points])

    useEffect(() => {
        if (!shape) {
            return;
        }

        if (!modalIsVisible) {
            setShape(resetPropertyOfFeatures(
                shape, { iconSize: 0.4 }
            ));
            setFollowUserLocation(true);
        }
    }, [modalIsVisible]);

    return (
        <View style={styles.container}>
            <PointMapModal
                navigation={navigation}
                isVisible={modalIsVisible}
                setIsVisible={setModalIsVisible}
                userLocation={userLocation.coords}
                item={modalData}
            />
            <MapboxGL.MapView
                styleURL={'mapbox://styles/mapbox/dark-v10'} 
                style={styles.map}
                localizeLabels={true}
                scrollEnabled={true}
                showUserLocation={true}
                logoEnabled={false}
                surfaceView={true}
                ref={mapRef}
            >
                <MapboxGL.UserLocation
                    visible={false}
                    onUpdate={setUserLocation}
                />
                <MapboxGL.Camera
                    zoomLevel={8}
                    followUserMode={'normal'}
                    followUserLocation={followUserLocation}
                    ref={cameraRef}
                />

                <MapboxGL.Animated.ShapeSource
                    id="symbolLocationSource"
                    ref={shapeSourceRef}
                    hitbox={{width: 20, height: 20}}
                    cluster
                    clusterRadius={50}
                    clusterMaxZoomLevel={10}
                    onPress={async point => {
                        setFollowUserLocation(false);
                        let coord = [];
                        let zoomLevel = 12;

                        if (point.features[0].properties.cluster) {
                            coord = point.features[0]['geometry']['coordinates'];
                            zoomLevel = parseInt(await mapRef.current.getZoom()) + 3;
                        } else {
                            const obj = point.features[0].properties;
                            
                            coord = obj['geodata']['coordinates']
                            navigation.setParams({visible: false})
                            
                            setModalData(obj)
                            setModalIsVisible(true);
                            
                            setShape(
                                updatePropertyOfFeature(
                                    resetPropertyOfFeatures(
                                        shape, { iconSize: 0.4 }
                                    ),
                                    obj.id,
                                    { iconSize: 0.7 }
                                )
                            )
                        }
                            
                        setTimeout(() => {
                            cameraRef.current.setCamera({
                                centerCoordinate: coord,
                                zoomLevel: zoomLevel,
                                animationDuration: 500
                            })    
                        }, 10);
                    }}
                    shape={shape ? shape : {}}
                >
                    <MapboxGL.Animated.SymbolLayer
                        id="pointCount"
                        style={mapBoxStyles.clusterCount}
                    />
                    <MapboxGL.Animated.CircleLayer
                        id="symbolClusters"
                        belowLayerID="pointCount"
                        filter={['has', 'point_count']}
                        style={mapBoxStyles.clusteredPoints}
                        paint={{"text-color": "#ffffff"}}
                    />
                    <MapboxGL.Animated.SymbolLayer
                        id="symbolLocationSymbols"
                        filter={['!', ['has', 'point_count']]}
                        style={mapBoxStyles.point}
                    />
                </MapboxGL.Animated.ShapeSource>
            </MapboxGL.MapView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        height: '100%',
        width: '100%',
        elevation: -1,
        zIndex: -1
    },
    map: {
        flex: 1
    },
});

const mapBoxStyles = {
    clusteredPoints: {
        circlePitchAlignment: 'map',

        circleColor: [
            'step',
            ['get', 'point_count'],
            DEFAULT_COLOR,
            5,
            DEFAULT_COLOR,
        ],

        circleRadius: [
            'step', ['get', 'point_count'],
            20, 5,
            35, 10,
            50
        ],

        circleOpacity: 0.30,
        circleStrokeWidth: 2,
        circleStrokeColor: DEFAULT_COLOR,
    },
    clusterCount: {
        textField: ['get', 'point_count'],
        textColor: DEFAULT_COLOR,
        textSize: 14,
        textPitchAlignment: 'map',
    },
    point: {
        iconImage: require('../../assets/point_marker.png'),
        iconSize: ['get', 'iconSize'],
        iconAllowOverlap: true,
    }
}
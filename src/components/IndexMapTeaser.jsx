import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity, Text, Image } from 'react-native';
import MapboxGL from '@react-native-mapbox-gl/maps';
import { PointMarker } from './PointMarker';

import DefaultStyles from '../constance/styles';


export const IndexMapTeaser = ({ navigation }) => {
    const [userLocation, setUserLocation] = useState({});
    const iconFile = require('../../assets/logo.png');

    return (
        <TouchableOpacity
            style={styles.container}
            onPress={() => navigation.navigate('Point', {
                    screen: 'PointIndex',
                    initial: false,
                    params: {
                        screen: 'PointMap'
                    }
                }
            )}
            activeOpacity={1}
        >
            <Text style={styles.header}>Посмотреть на карте {userLocation.longitude}</Text>
            <MapboxGL.MapView
                styleURL={'mapbox://styles/mapbox/dark-v10'} 
                style={styles.map}
                localizeLabels={true}
                scrollEnabled={false}
                zoomEnabled={false}
            >
                <MapboxGL.UserLocation
                    visible={false}
                    onUpdate={setUserLocation}
                />
                <MapboxGL.Camera
                    zoomLevel={11}
                    followUserMode={'normal'}
                    followUserLocation
                />
                {(userLocation.hasOwnProperty('coords')) ? (
                    <MapboxGL.MarkerView
                        id={'index-map-home'}
                        coordinate={[
                            userLocation.coords.longitude,
                            userLocation.coords.latitude
                        ]}
                    >
                        <Image
                            resizeMode='contain'
                            source={iconFile}
                            style={{
                                width: 100
                            }}
                        />
                    </MapboxGL.MarkerView>
                ) : <></>}
            </MapboxGL.MapView>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
	container: {
        // height: 400,
        marginBottom: 24,
        marginHorizontal: 16,
        flex: 1
	},
    header: {
        ...DefaultStyles.header,
        marginBottom: 24
    },
    map: {
        height: 300,
        marginRight: -16,
        marginLeft: -16,
    }
});

import React from 'react';
import { StyleSheet, TouchableOpacity, Text, View, ImageBackground } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';

import { geoDistance, listItemDistanceString } from '../utils/distance';
import { ButtonAddToBookmark  } from './elements/ButtonAddToBookmark';
import DefaultStyles from '../constance/styles';


const PointListItemWrapper = ({children, item, touch, navigation }) => {
    if (touch) {
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                style={styles.item}
                onPress={() => {
                    navigation.goBack();
                    return navigation.navigate('Point', {
                        screen: 'PointItem',
                        initial: false,
                        params: {
                            id: item.id
                        }
                    });
                }}
            >
                {children}
            </TouchableOpacity>
        );
    } else {
        return (
            <View
                activeOpacity={0.8}
                style={styles.item}
            >
                {children}
            </View>
        );
    }
}

export const PointListItem = ({ navigation, item, userLocation, touch = true }) => {
    const placeholder = require('../../assets/placeholder_point.png');
    return (
        <PointListItemWrapper
            item={item}
            touch={touch}
            navigation={navigation}
        >
		    <View style={styles.itemWrapper}>
                <ImageBackground
                    source={item.cover ? { uri: item.cover } : placeholder}
                    resizeMode="cover"
                    style={styles.image}>
                    <LinearGradient
                        colors={[`#00000000`, `#00000000`, '#000']}
                        start={{x: 0, y: 0}} end={{x: 0, y: 1}}
                        style={styles.gradient}
                    >
                        <ButtonAddToBookmark contentType={'points'} id={item.id} />
                        <Text style={styles.title}>{item.title}</Text>
                    </LinearGradient>
                </ImageBackground>
                <View style={styles.distance}>
                    <Ionicons name="time-outline" color='#000' size={16} style={styles.distanceIcon}/>
                    <Text style={styles.distanceText}>
                        {listItemDistanceString(
                            geoDistance(
                                {
                                    from: { lat: userLocation.latitude, lon: userLocation.longitude},
                                    to: { lat: item.geodata.coordinates[1], lon: item.geodata.coordinates[0] }
                                },
                            ), true
                        )}
                    </Text>
                </View>
            </View>
        </PointListItemWrapper>
    )
};

const styles = StyleSheet.create({
	item: {
        flex: 1,
        marginBottom: 40,
        marginRight: 12,
        marginLeft: 12,
        height: 200,
	},
    itemWrapper: {
        ...DefaultStyles.contextWrapper
    },
    image: {
        height: 200,
        ...DefaultStyles.contextShadowedImage,
    },
    title: {
        ...DefaultStyles.contextShadowedTitle,
        ...DefaultStyles.font,
        fontSize: 18,
        color: '#fff',
        marginBottom: 32,
        marginLeft: 16
    },
    gradient: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    distance: {
        ...DefaultStyles.contextDistanceBaloon,
    },
    distanceIcon: {
        marginRight: 6
    },
    distanceText: {
        ...DefaultStyles.font,
    }
});

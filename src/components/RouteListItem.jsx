import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, ImageBackground } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import moment from 'moment';
import 'moment/locale/ru';

import { ButtonAddToBookmark  } from './elements/ButtonAddToBookmark';
import { listItemDistanceString } from '../utils/distance';
import { countFormatter } from '../utils/countFormatter';
import DefaultStyles, { BACKGROUND_COLOR, DEFAULT_COLOR } from '../constance/styles';


export const RouteListItem = ({
    navigation,
    item,
    removeItemMargin = false,
    extraScheduleDay = null
}) => {
    const placeholder = require('../../assets/placeholder_point.png');
    
    if (extraScheduleDay) {
        item.schedules = [
            extraScheduleDay
        ];
    }

    const tour = () => {
        try {
            return item.tours[0]
        } catch (e) {
            return false;
        }
    }
    
    const getNearestTourDate = () => {
        try {
            return item.schedules.filter(day => new Date(day.start) > Date.now())[0];
        } catch (e) {
            return false;
        }
    }

    const hasTour = () => {
        return Boolean(tour()) && Boolean(getNearestTourDate());
    }
    
    return (
        <TouchableOpacity
            activeOpacity={0.9}
            style={{
                ...styles.item,
                ...(hasTour() ? styles.itemTour : {}),
                ...(removeItemMargin ? {
                    marginLeft: 0,
                    marginRight: 0,
                    marginBottom: 0,
                } : {})
            }}
            onPress={() => {
                // navigation.goBack();
                return navigation.navigate('Route', {
                    screen: 'RouteItem',
                    initial: false,
                    params: {
                        id: item.id
                    }
                });
            }}
        >
		    <View style={{
                ...styles.itemWrapper,
                ...(hasTour() ? styles.itemWrapperTour : {})
            }}>
                <ImageBackground
                    source={item.cover ? { uri: item.cover } : placeholder}
                    resizeMode="cover"
                    style={{
                        ...styles.image,
                        ...(hasTour() ? styles.imageTour : {})
                    }}>
                    <LinearGradient
                        colors={[`#00000000`, `#00000000`, '#000']}
                        start={{x: 0, y: 0}} end={{x: 0, y: 1}}
                        style={styles.gradient}
                    >
                        <ButtonAddToBookmark contentType={'routes'} id={item.id} />
                        <Text style={{
                            ...styles.title,
                            ...(hasTour() ? styles.titleTour : {})
                        }}>{item.title}</Text>
                        <Text style={styles.description}>{item.description}</Text>
                    </LinearGradient>
                </ImageBackground>
                
                {hasTour() ? (
                    <View style={styles.tourPlate}>
                        <Text style={styles.tourPlateTitle}>
                            Тур по этому маршруту вместе с экскурсией{'\n'}
                            <Text style={styles.tourPlatePlaces}>
                                Осталось {getNearestTourDate().available_places} {
                                    countFormatter(
                                        getNearestTourDate().available_places, {
                                            one: 'билет',
                                            two: 'билета',
                                            few: 'билетов',
                                        }
                                    ).toString()
                                }
                            </Text>
                        </Text>
                        <View style={styles.tourPlateBar}>
                            <TouchableOpacity
                                style={styles.scheduleButton}
                                onPress={() => {
                                    navigation.navigate(
                                        'Route', {
                                            screen: 'RoutePayment',
                                            initial: false,
                                            params: {
                                                title: 'Покупка тура',
                                                item: item,
                                                day: getNearestTourDate()
                                            }
                                        }
                                    )
                                }}
                            >
                                <Text style={DefaultStyles.buttonTitle}>Купить</Text>
                            </TouchableOpacity>
                            <View>
                                <Text style={styles.tourPlatePrice}>
                                    {new Intl.NumberFormat('ru-RU').format(tour().price)}₽
                                </Text>
                                <Text style={styles.tourPlateDate}>
                                    {moment(getNearestTourDate().start).format('D MMMM, в HH:MM')}
                                </Text>
                            </View>
                        </View>
                    </View>
                ) : <></>}

                <View style={{
                    ...styles.distance,
                    ...(hasTour() ? styles.distanceTour : {})
                }}>
                    <Ionicons name="time-outline" color='#000' size={16} style={styles.distanceIcon}/>
                    <Text style={styles.distanceText}>
                        {listItemDistanceString(item.distance)},
                        &nbsp;
                        {item.duration != 0 ? item.duration + ' ' + countFormatter(item.duration, {
                            one: 'день',
                            two: 'дня',
                            few: 'дней',
                        }) : 'меньше дня'},
                        &nbsp;
                        {item.points_count} {countFormatter(item.points_count, {
                            one: 'место',
                            two: 'места',
                            few: 'мест',
                        })}
                    </Text>
                </View>
            </View>
        </TouchableOpacity>
    )
};
const DAFULT_HEIGHT = 400;
const TOUR_HEIGHT = 500;
const styles = StyleSheet.create({
	item: {
        flex: 1,
        marginBottom: 40,
        marginRight: 12,
        marginLeft: 12,
        height: DAFULT_HEIGHT,
	},
    itemTour: {
        height: TOUR_HEIGHT,
    },
    itemWrapper: {
        ...DefaultStyles.contextWrapper,
        height: DAFULT_HEIGHT,
    },
    itemWrapperTour: {
        height: TOUR_HEIGHT,
    },
    image: {
        flex: 1,
        ...DefaultStyles.contextShadowedImage,
    },
    imageTour: {
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
    },
    gradient: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    title: {
        ...DefaultStyles.contextShadowedTitle,
        ...DefaultStyles.font,
        fontSize: 18,
        color: '#fff',
        marginLeft: 16,
        marginBottom: 8,
    },
    titleTour: {
        fontSize: 26,
    },
    description: {
        textShadowOffset: {width: 0, height: 2},
        textShadowRadius: 25,
        textShadowColor: '#000',
        marginHorizontal: 16,
        color: BACKGROUND_COLOR,
        marginBottom: 40,
    },
    distance: {
        ...DefaultStyles.contextDistanceBaloon,
    },
    distanceTour: {
        bottom: 158
    },
    distanceIcon: {
        marginRight: 6
    },
    distanceText: {
        ...DefaultStyles.font,
    },
    tourPlate: {
        backgroundColor: DEFAULT_COLOR,
        paddingHorizontal: 16,
        paddingTop: 40,
        paddingBottom: 16,
        borderBottomLeftRadius: 24,
        borderBottomRightRadius: 24,
    },
    tourPlateTitle: {
        ...DefaultStyles.font,
        fontSize: 18,
        lineHeight: 22,
        color: BACKGROUND_COLOR,
        marginBottom: 16
    },
    tourPlatePlaces: {
        ...DefaultStyles.regularFont,
        fontSize: 14,
        color: `${BACKGROUND_COLOR}70`,
    },
    tourPlateBar: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    tourPlatePrice: {
        ...DefaultStyles.fontBold,
        fontSize: 18,
        color: BACKGROUND_COLOR,
    },
    tourPlateDate: {
        color: BACKGROUND_COLOR,
    },
    scheduleButton: {
        ...DefaultStyles.buttonContainer,
        height: 42,
        borderRadius: 21,
        paddingHorizontal: 16,
        marginRight: 24
    },
});

import React, { useState } from 'react';
import { Ionicons } from '@expo/vector-icons';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';

import { DEFAULT_COLOR } from '../../constance/styles';
import { UserMenu } from '../UserMenu';


export const HeaderSection = ({ navigation, ...props }) => {
    const logo = require('../../../assets/adaptive-icon.png');
    const [ menuShow, setMenuShow ] = useState(false);

    return (
        <View style={styles.header}>
            <UserMenu navigation={navigation} menuShow={menuShow} setMenuShow={setMenuShow} />
            <TouchableOpacity onPress={() => {
                navigation.navigate('Search', { title: 'Поиск' });
            }}>
                <Ionicons name="search-outline" color={DEFAULT_COLOR} size={28} />
            </TouchableOpacity>
            <Image
                source={logo}
                style={styles.logo}
            />
            <TouchableOpacity onPress={() => {setMenuShow(true)}}>
                <Ionicons name="menu-outline" color={DEFAULT_COLOR} size={28} />
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    header: {
        height: 64,
        paddingHorizontal: 12,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    logo: {
        width: 100,
        height: 100
    }
});

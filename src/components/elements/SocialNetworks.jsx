import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Linking } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

import DefaultStyles, { DEFAULT_COLOR } from '../../constance/styles';


export const SocialNetworks = ({ navigation, data }) => {
    const servernameIconnameMap = {
        'vk': 'vk',
        'instagram': 'instagram',
        'facebook': 'facebook',
        'youtube': 'youtube',
        'telegram': 'telegram',
        'soundcloud': 'soundcloud',
    }
    
    if (!data) {
        return <></>
    }

    return (
        <>
            <Text style={styles.headerMiddle}>Социальные сети</Text>
            <View style={styles.container}>
                {data.map(item => (
                    <TouchableOpacity
                        onPress={() => {
                            Linking.openURL(item.link)
                        }}
                        style={styles.itemContainer}
                        key={`${item.id}`}
                    >
                        <FontAwesome
                            name={servernameIconnameMap[item.net]}
                            size={32}
                            style={styles.item}
                            color={DEFAULT_COLOR}
                        />
                    </TouchableOpacity>
                ))}
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    headerMiddle: {
        ...DefaultStyles.headerMiddle,
        textAlign: 'center',
        marginBottom: 16
    },
    itemContainer: {
        padding: 0,
        marginHorizontal: 16,
    },
    item: {
        justifyContent: 'center',
    }
})
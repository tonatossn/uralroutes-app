import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useSelector } from 'react-redux';

import { DEFAULT_COLOR } from '../../constance/styles';
import { removeBookmark, addBookmark } from '../../redux/actions/bookmark';
import { useDispatch } from 'react-redux';


export const ButtonAddToBookmark = ({contentType, id}) => {
    const dispatch = useDispatch();
    const isActive = useSelector(state => state.bookmark[contentType].find(el => el === id) !== undefined);
    const onPress = () => {
        if (isActive) {
            dispatch(removeBookmark(contentType, id));
        } else {
            dispatch(addBookmark(contentType, id));
        }
    }


    return (
        <TouchableOpacity
            style={{
                ...styles.container,
                ...(isActive ? styles.containerActive : {})
            }}
            onPress={onPress}
        >
            <Ionicons
                name={isActive ? 'heart' : 'heart-outline'}
                color={isActive ? DEFAULT_COLOR : '#fff'}
                size={24}
            />
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        width: 36,
        height: 36,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 18,
        position: 'absolute',
        top: 12,
        right: 12,
        backgroundColor: '#D7D7D7'
    },
    containerActive: {
        backgroundColor: '#fff',
    },
    icon: {},
})
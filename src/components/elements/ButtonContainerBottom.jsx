import React from 'react';
import { View, TouchableOpacity, StyleSheet, Text } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

import DefaultStyles, { BACKGROUND_COLOR, DEFAULT_COLOR } from '../../constance/styles';


export const ButtonContainerBottom = ({ items, activeIndex = null }) => {
    return (
        <View style={styles.plate}>
            <LinearGradient
                colors={[`${BACKGROUND_COLOR}00`, BACKGROUND_COLOR]}
                start={{x: 0, y: 0}} end={{x: 0, y: 1}}
                style={styles.gradient}
            >
                <View style={styles.container}>
                    {items.map((item, index) => {
                        return <TouchableOpacity
                            onPress={item.onPress}
                            style={{
                                ...styles.button,
                                borderRightWidth: index === items.length -1 ? 0 : 1
                            }}
                            activeOpacity={0.8}
                            key={'tab-' + index}
                        >
                            <Text style={{
                                ...styles.text,
                                ...(activeIndex === index ? styles.active : {}),
                            }}>
                                {item.title}
                            </Text>
                        </TouchableOpacity>
                    })}
                </View>
            </LinearGradient>
        </View>
    )
}

const styles = StyleSheet.create({
    plate: {
        position: 'absolute',
        bottom: 0,
        height: 54,
        width: '100%',
    },
    gradient: {
        height: '100%',
        alignItems: 'center',
    },
    container: {
        height: 42,
        elevation: 10,
        shadowOffset: { width: 0, height: 6},
        shadowOpacity: 0.25,
        shadowRadius: 5,
        borderRadius: 21,
        backgroundColor: BACKGROUND_COLOR,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        height: 42,
        borderRightWidth: 0,
        borderColor: '#ddd',
        paddingHorizontal: 16,
    },
    text: {
        ...DefaultStyles.font,
        lineHeight: 42,
        fontSize: 14,
        color: '#888',
    },
    active: {
        color: DEFAULT_COLOR
    },
});

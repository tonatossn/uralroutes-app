import React, { useState, useEffect } from 'react';
import { TouchableOpacity, StyleSheet, View, Text, ScrollView } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import DefaultStyles, { BACKGROUND_COLOR, DEFAULT_COLOR } from '../../constance/styles';
import { UniversalPicker } from './UniversalPicker';

const useForceUpdate = () => {
    const [value, setValue] = useState(0);
    return () => setValue(value => value + 1);
}

const SortWidget = ({ sortVariants, sortAction, sortActive }) => {
    const [pickerShow, setPickerShow] = useState(false);
    
    const getLabel = () => {
        try {
            return sortVariants.find(e => e.value == sortActive).label
        } catch (e) {
            return '';
        }
    }
    
    return (
        <>
            <TouchableOpacity
                onPress={() => setPickerShow(!pickerShow)}
                activeOpacity={0.5}
                style={styles.sort}
            >
                <Text style={styles.panelLabel}>
                    {getLabel()}
                </Text>
                <Ionicons name='swap-vertical-outline' size={20} />
            </TouchableOpacity>
            <UniversalPicker
                title={'Сортировка'}
                items={sortVariants}
                onValueChange={value => sortAction(value)}
                status={pickerShow}
                setStatus={setPickerShow}
                selectedValue={sortActive}
            />
        </>
    )
}

const FilterWidget = ({ filterTitle, filterAction, filterActive }) => {
    return (
        <TouchableOpacity onPress={() => filterAction()} activeOpacity={0.5} style={styles.filter}>
            <Text style={styles.panelLabel}>
                {filterTitle ? filterTitle : 'Фильтры'}
            </Text>
            <Ionicons name='options-outline' size={20} />
            {filterActive ? <View style={styles.filterActive}></View> : <></>}
        </TouchableOpacity>
    )
}

const TagsWidget = (
    { tagsData } = {
        items,
        onPressAction,
        selectedValues,
        needShadow
    }
) => {
    const forceUpdate = useForceUpdate();
    const [selectedValues, setSelectedValues] = useState(tagsData.selectedValues);
    useEffect(() => {
        setSelectedValues(tagsData.selectedValues);
    }, [tagsData.selectedValues]);

    return (
        <>
            <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                style={styles.horizontalFilter}
            >
                {tagsData?.items?.map((tag, i) => {
                    const isSelected = selectedValues != null 
                        ? selectedValues.find(el => el == tag.id)
                        : false;

                    return (
                        <TouchableOpacity
                            key={tag.id}
                            style={{
                                ...styles.horizontalFilterItem,
                                ...(isSelected ? {
                                    backgroundColor: DEFAULT_COLOR,
                                    color: '#fff'
                                } : {})
                            }}
                            onPress={() => {
                                forceUpdate()
                                return tagsData.onPressAction(tag)
                            }}>
                            <Text style={isSelected ? {color: '#fff'} : {color: '#000'}}>{tag.title}</Text>
                        </TouchableOpacity>
                    )
                })}
                <View style={styles.horizontalFilterTheEnd} />
            </ScrollView>
            {tagsData.needShadow ? (
                <LinearGradient
                    colors={[`${BACKGROUND_COLOR}00`, BACKGROUND_COLOR]}
                    start={{x: 0, y: 0}} end={{x: 0.8, y: 0}}
                    style={styles.horizontalFilterShadow}
                />
            ) : <></>}
        </>
    )
}

export const SortFilterBar = ({
    sortVariants = null,
    sortAction = null,
    sortActive = null,
    filterTitle = null,
    filterAction = null,
    filterActive = false,
    tagsData = null,
} = {}) => {
    return (
        <View style={styles.container}>
            <View style={styles.containerTop}>
                {Array.isArray(sortVariants) && sortAction != null && sortActive != null
                    ? (
                        <SortWidget
                            sortVariants={sortVariants}
                            sortAction={sortAction}
                            sortActive={sortActive}
                        />
                    ) 
                    : <></> 
                }
                {filterAction != null 
                    ? (
                        <FilterWidget
                            filterTitle={filterTitle}
                            filterAction={filterAction}
                            filterActive={filterActive}
                        />
                    ) : <></>
                }
            </View>
            <View style={styles.containerBottom}>
                {tagsData != null
                    ? (
                        <TagsWidget
                            tagsData={tagsData}
                        />
                    ) : <></>
                }
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {

    },
    containerTop: {
        paddingRight: 16,
        paddingLeft: 16,
        flexDirection: 'row',
        backgroundColor: BACKGROUND_COLOR,
    },
    containerBottom: {},
    panelLabel: {
        ...DefaultStyles.font,
        fontSize: 14,
        marginRight: 8
    },
    sort: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        height: 40
    },
    filter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        textAlign: 'right',
        alignItems: 'center',
        height: 40,
    },
    filterActive: {
        width: 12,
        height: 12,
        backgroundColor: DEFAULT_COLOR,
        borderWidth: 2,
        borderColor: BACKGROUND_COLOR,
        borderRadius: 8,
        top: 10,
        right: -2,
        position: 'absolute'
    },
    horizontalFilter: {
        marginTop: 8,
        paddingLeft: 12,
        paddingRight: 12,
        height: 40,
    },
    horizontalFilterShadow: {
        width: 30,
        height: 28,
        position: 'absolute',
        right: 0,
        top: 8
    },
    horizontalFilterItem: {
        ...DefaultStyles.font,
        borderRadius: 30,
        height: 28,
        lineHeight: 28,
        fontSize: 14,
        marginRight: 8,
        backgroundColor: '#eee',
        padding: 4,
        paddingRight: 12,
        paddingLeft: 12,
    },
    horizontalFilterTheEnd: {
        height: 28,
        width: 20,
    },
})
import React from 'react';
import { StyleSheet, View, Text } from 'react-native';


export const Bubble = ({}) => {
    return (
        <View style={styles.bubble}>
            <Text>bubble</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    bubble: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        height: 150,
        // width: '100%',
        backgroundColor: '#ffeeee',
    }
})
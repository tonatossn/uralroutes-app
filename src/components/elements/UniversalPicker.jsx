import React, { useRef } from 'react';
import { View, Platform, StyleSheet, Text } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import { Ionicons } from '@expo/vector-icons';
import Modal from "react-native-modal";

import DefaultStyles from '../../constance/styles';


const BottomModalheight = 300;

export const UniversalPicker = ({ title, items, onValueChange, status, setStatus, selectedValue }) => {
    const pickerRef = useRef();
    
    const PickerInstance = () => {
        return (
            <Picker
                ref={pickerRef}
                prompt={title}
                style={styles.sortPicker}
                selectedValue={selectedValue}
                onBlur={() => {
                    setStatus(false);
                }}
                onValueChange={(value) => {
                    if (selectedValue === value) {
                        return;
                    }
                    
                    if (Platform.OS === 'android') {
                        setStatus(false);
                    }

                    onValueChange(value);
                }}
            >
                {items.map(item => {
                    return <Picker.Item label={'  ' +item.label} value={item.value} key={'sort' + item.value} />
                })}
            </Picker>
        );
    }
    
    const IOSWrapper = ({ children }) => {
        return (
            <Modal
                swipeDirection="down"
                onSwipeComplete={() => setStatus(false)}
                hardwareAccelerated={true}
                isVisible={status}
                onModalWillHide={() => {
                    setStatus(!status);
                }}
                swipeThreshold={400}
                style={{margin: 0}}
            >
                <View style={styles.modalWrapper}>
                    <View style={styles.modalContainer}>
                        <View style={styles.modalHeader}>
                            <Text style={styles.modalHeaderText}>{title}</Text>
                            <Ionicons
                                name="close-circle-outline"
                                color={'#000'}
                                size={32}
                                onPress={() => setStatus(!status)}
                            />
                        </View>
                        {children}
                    </View>
                </View>
            </Modal>
        )
    }

    
    const AndroidWrapper = ({ children }) => {
        if (status) {
            setTimeout(() => pickerRef.current.focus(), 50);
        }
        return (
            <View style={styles.androidPicker}>
                {children}
            </View>
        )
    }

    return (
        Platform.OS === 'ios' ? (
            <IOSWrapper>
                <PickerInstance />
            </IOSWrapper>
        ) : (
            <AndroidWrapper>
                <PickerInstance />
            </AndroidWrapper>
        )
    )
}

const styles = StyleSheet.create({
    androidPicker: {
        height: 50,
        width: '50%',
        opacity: 0,
        position: 'absolute',
        left: -1000
    },
    modalWrapper: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        padding: 0,
        margin: 0,
    },
    modalContainer: {
        backgroundColor: '#fff',
        height: BottomModalheight,
        width: '100%',
        padding: 16,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        position: 'absolute',
    },
    modalHeader: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 50
    },
    modalHeaderText: {
        ...DefaultStyles.headerSmall
    }
})
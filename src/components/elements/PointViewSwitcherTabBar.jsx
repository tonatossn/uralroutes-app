import React, { useEffect, useState, useRef } from 'react';

import { BottomTabBar } from '@react-navigation/bottom-tabs';
import { View, Animated, StyleSheet } from 'react-native';

export const PointViewSwitcherTabBar = (props) => {
    const tabBarWrapperStyles = {
        position: 'absolute',
        width: 220,
        marginLeft: -110,
        left: '50%',
        height: 42,
        bottom: 12,
        elevation: 10,
        shadowOffset: { width: 0, height: 6},
        shadowOpacity: 0.25,
        shadowRadius: 5
    }
    const offsetPosition = useRef(new Animated.ValueXY()).current;
    const [ isVisible, setIsVisible ] = useState(true);
    
    const getRoute = () => props.state.routes[props.state.index];

    useEffect(() => {
        try {
            setIsVisible(
                getRoute().params.visible
            );
        } catch (e) {}
    });

    useEffect(() => {
        Animated.timing(
            offsetPosition,
            {
                toValue: {
                    x: 0,
                    y: isVisible ? 0 : 100
                },
                duration: 300,
                useNativeDriver: true
            }
        ).start();
    }, [isVisible])

    return (
        <Animated.View style={{
            ...tabBarWrapperStyles,
            transform: [
                {translateY: offsetPosition.y}
            ]
        }}>
            <BottomTabBar
                {...props}
            />
        </Animated.View>
    );
}
import React, { useState, useEffect } from 'react';
import { StyleSheet, TouchableOpacity, Text, Share } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import DefaultStyles, { DEFAULT_COLOR } from '../../constance/styles';


export const ButtonShareObject = ({ item }) => {
    const onShare = async () => {
        try {
        const result = await Share.share({
            message:
            'React Native | A framework for building native apps using React',
        });
        if (result.action === Share.sharedAction) {
            if (result.activityType) {
            // shared with activity type of result.activityType
            } else {
            // shared
            }
        } else if (result.action === Share.dismissedAction) {
            // dismissed
        }
        } catch (error) {
        alert(error.message);
        }
    };

    return (
        <TouchableOpacity
            style={{
                ...styles.buttonContainer
            }}
            onPress={onShare}
        >
            <Ionicons
                name={'share-social-outline'}
                color={DEFAULT_COLOR}
                size={24}
            />
            <Text style={styles.buttonTitle}>Поделиться</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    buttonContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    buttonTitle: {
        ...DefaultStyles.buttonTitle
    }
});

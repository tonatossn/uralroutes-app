import React from 'react';
import { Ionicons } from '@expo/vector-icons';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';

import DefaultStyles, { DEFAULT_COLOR } from '../../constance/styles';
import { ButtonAddToBookmark } from './ButtonAddToBookmark';


export const HeaderInner = ({ navigation, route, options }) => {
    const favoriteMap = {
        'PointItem': 'points',
        'RouteItem': 'routes'
    }
    
    return (
        <View style={styles.header}>
            <View style={styles.left}>
                <TouchableOpacity onPress={() => navigation.goBack()} style={styles.backIconTouchable}>
                    <Ionicons
                        name={'arrow-back-outline'}
                        size={28}
                        color={DEFAULT_COLOR}
                        style={styles.backIcon}
                    />
                    <Text style={styles.title}>{options.headerTitle}</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.center}></View>
            <View style={styles.right}>
                {favoriteMap[route.name] != undefined ?
                    <View style={styles.addToBookmark}>
                        <ButtonAddToBookmark
                            contentType={favoriteMap[route.name]}
                            id={route.params.id}
                        />
                    </View>
                : <></>}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    header: {
        height: 50,
        paddingHorizontal: 12,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    title: {
        ...DefaultStyles.fontBold,
        fontSize: 16,
        color: DEFAULT_COLOR
    },
    left: {
    },
    center: {},
    right: {},
    backIcon: {
        marginRight: 8,
    },
    backIconTouchable: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    addToBookmark: {
        top: -28,
        right: -12
    }
});

import React from 'react';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import DefaultStyles, { DEFAULT_COLOR } from '../../constance/styles';


export const ButtonAddToMyRoute = ({ item, navigation }) => {
    const onPress = () => {
        navigation.navigate('AddToMyRoute', {
            title: 'Добавить к моему маршруту',
            id: item.id
        })
    }

    return (
        <TouchableOpacity
            style={{
                ...styles.buttonContainer
            }}
            onPress={onPress}
        >
            <Ionicons
                name={'git-branch-outline'}
                color={DEFAULT_COLOR}
                size={24}
            />
            <Text style={styles.buttonTitle}>Добавить в маршрут</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    buttonContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    buttonTitle: {
        ...DefaultStyles.buttonTitle
    }
});

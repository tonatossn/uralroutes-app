import React, { useEffect, useState } from 'react';
import { View, TouchableOpacity, StyleSheet, Text, ScrollView } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Modal from "react-native-modal";
import Checkbox from 'expo-checkbox';
import { AppButton  } from './AppButton';

import DefaultStyles, { DEFAULT_COLOR } from '../../constance/styles';


const BottomModalheight = 500;

const useForceUpdate = () => {
    const [value, setValue] = useState(0);
    return () => setValue(value => value + 1);
}

const MultiSelectListItem = ({ item, active, onChange }) => {
    const onPress = () => {
        onChange(item, !active);
    }

    return (
        <TouchableOpacity
            style={styles.listItem}
            onPress={onPress}
            activeOpacity={0.8}
        >
            <View style={styles.listItemContainer}>
                <Text>{item.label}</Text>
                <Checkbox
                    value={active}
                    color={active ? DEFAULT_COLOR : undefined}
                />
            </View>
            <Text style={styles.listItemDescription}>{item.description}</Text>
        </TouchableOpacity>
    );
}

export const MultiSelectModal = ({
    items,
    title,
    onValueChange,
    status,
    setStatus,
    selectedValues
}) => {
    const forceUpdate = useForceUpdate();
    const [pressedCheckboxes, setPressedCheckboxes] = useState([]);
    useEffect(() => {
        if (Array.isArray(selectedValues)) {
            setPressedCheckboxes(selectedValues)
        }
    }, [selectedValues])

    return (
        <Modal
            swipeDirection="down"
            hardwareAccelerated={true}
            isVisible={status}
            onModalHide={() => onValueChange(pressedCheckboxes)}
            onSwipeComplete={() => setStatus(false)}
            swipeThreshold={200}
            propagateSwipe
            style={{margin: 0}}
        >
            <View style={styles.modalWrapper}>
                <View style={styles.modalContainer}>
                    <View style={styles.modalHeader}>
                        <Text style={styles.modalHeaderText}>{title}</Text>
                        <Ionicons
                            name="close-circle-outline"
                            color={'#000'}
                            size={32}
                            onPress={() => setStatus(!status)}
                        />
                    </View>
                    <ScrollView showsVerticalScrollIndicator={false} style={styles.modalFlatList}>
                        <View>
                            {items.map(item => {
                                const active = pressedCheckboxes.find(el => el == item.value) === undefined
                                                    ? false
                                                    : true;
                                
                                return (
                                    <MultiSelectListItem
                                        key={`multiselect-${item.value}`}
                                        item={item}
                                        active={active}
                                        onChange={(item, value) => {
                                            if (value) {
                                                setPressedCheckboxes([
                                                    ...pressedCheckboxes, item.value
                                                ]);
                                            } else {
                                                setPressedCheckboxes(
                                                    pressedCheckboxes.filter(el => el != item.value)
                                                );
                                            }
                                        }}
                                    />
                                )
                            })}
                        </View>
                    </ScrollView>
                    <AppButton
                        title={'Очистить фильтры'}
                        style={styles.clearFilterButton}
                        onPress={() => {
                            setPressedCheckboxes([]);
                            forceUpdate();
                        }}
                    />
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalWrapper: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        padding: 0,
        margin: 0,
    },
    modalContainer: {
        backgroundColor: '#fff',
        height: BottomModalheight,
        width: '100%',
        padding: 16,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    modalHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignContent: 'center',
        alignItems: 'center',
        height: 50,
    },
    modalHeaderText: {
        ...DefaultStyles.headerSmall
    },
    modalFlatList: {
        height: BottomModalheight - 200
    },
    listItem: {
        paddingVertical: 24,
        borderBottomWidth: 1,
        borderBottomColor: '#efefef',
    },
    listItemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    listItemDescription: {
        fontSize: 12,
        color: '#999',
        paddingRight: 40
    },
    clearFilterButton: {
        height: 60
    }
})
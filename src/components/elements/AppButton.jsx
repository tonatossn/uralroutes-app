import React from 'react';
import { TouchableOpacity, Text, View} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import DefaultStyles, { DEFAULT_COLOR } from '../../constance/styles';


export const AppButton = ({ title, onPress, rightArrow, style = {} }) => {
    return (
        <TouchableOpacity style={{...DefaultStyles.buttonContainer, ...style}} onPress={onPress}>
            {rightArrow ? (
                <Ionicons
                    name="arrow-forward-outline"
                    color={DEFAULT_COLOR}
                    size={16}
                    style={DefaultStyles.buttonIcon}/>
            ) : null}
            <Text style={DefaultStyles.buttonTitle}>{title}</Text>
        </TouchableOpacity>
    )
};

import React, { useState } from "react";
import { View, Text, StyleSheet, TextInput } from 'react-native';
import { useForm, Controller } from "react-hook-form";

import DefaultStyles, { FormStyles } from "../constance/styles";
import { postFeedbackForm } from '../api/feedback';
import { AppButton } from "./elements/AppButton";

const UserOfferPointForm = ({ onSubmit }) => {
    const { control, handleSubmit, formState: { errors } } = useForm();
    return (
        <>
            <View style={styles.field}>
                <Text style={styles.label}>Ваш e-mail</Text>
                <Controller
                    control={control}
                    rules={{
                        required: true,
                        pattern: {
                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                            message: 'Эл. почта указана некорректно'
                        }
                    }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            placeholder='name@host.domain'
                            style={styles.input}
                            autoComplete='email'
                            keyboardType='email-address'
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                        />
                    )}
                    name='email'
                />
                {errors.email && <Text style={styles.error}>Это обязательное поле, вы должны указать валидный адрес почты</Text>}
            </View>

            <View style={styles.field}>
                <Text style={styles.label}>Расскажите о месте</Text>
                <Controller
                    control={control}
                    rules={{
                    required: true,
                    }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.textarea}
                            multiline
                            numberOfLines={4}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder='Где оно находится, что в нем интересного и почему оно должно оказаться на индустриальной карте Урала?'
                        />
                    )}
                    name='text'
                />
                {errors.text && <Text style={styles.error}>Это обязательное поле</Text>}
            </View>
            <AppButton title={'Отправить предложение'} onPress={handleSubmit(onSubmit)} />
        </>
    )
}

const UserOfferPointSuccess = () => {
    return (
        <View style={styles.containerSuccess}>
            <Text style={styles.header}>
                Спасибо за ваше предложение!
            </Text>
            <Text style={styles.text}>
                Мы свяжемся с вами по указанному электронному адресу, чтобы побольше узнать об этом месте. Когда все будет готово, мы опубликуем информацию о нем в приложении и обязательно укажем ваше имя. 
            </Text>
        </View>
    )
}

export const UserOfferPoint = ({ navigation }) => {
    const [ submited, setSubmited ] = useState(false);
    const onSubmit = async (data) => {
        postFeedbackForm(data)
            .catch(e => {
                console.log(e)
            })
            .then(respones => {
                if (respones !== null) {
                    setSubmited(true);
                }
            });
    }
    
    return (
        <View style={styles.container}>
            <Text style={styles.header}>Добавьте место на карту</Text>
            <Text style={styles.text}>
                Знаете интересное место, но не нашли его на карте? Расскажите нам о нем! Мы свяжемся с вами и узнаем подробности, чтобы предложенный вами объект попал на карту.
            </Text>
            {submited ? <UserOfferPointSuccess /> : <UserOfferPointForm onSubmit={onSubmit} />}
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        padding: 16,
        flex: 1,
    },
    containerSuccess: {
        alignContent: 'center',
        justifyContent: 'center'
    },
    header: {
        ...DefaultStyles.header,
        marginBottom: 24
    },
    text: {
        ...DefaultStyles.regularFont,
        fontSize: 16,
        marginBottom: 24
    },
    ...FormStyles
})
import React, { useState } from 'react';
import { View, StyleSheet, FlatList, Text } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import moment from 'moment';
import 'moment/locale/ru';

import DefaultStyles, { BACKGROUND_COLOR, DEFAULT_COLOR } from '../constance/styles';
import { RouteListItem } from './RouteListItem';


const BuyedRoutesNotFound = () => {
    return (
        <View style={styles.notFound}>
            <Ionicons name="pricetags-outline" color={'#efefef'} size={100} />
            <Text style={styles.notFoundText}>
                Вы еще не купили ни одного тура) Дождитесь, когда организаторы анонсируют экскурсии и приобретайте билеты!
            </Text>
        </View>
    );
}

export const BuyedRoutes = ({ navigation, buyedroutes }) => {
    const notFoundList = [{id: 'notfound-404', title: 'Not found'}];
    
    const renderNotFound = ({ item }) => {
        return <BuyedRoutesNotFound />;
    }

    const getHumanStatus = (status) => {
        const statusMap = {
            'pending': {
                color: '#54B9D9',
                title: 'Ожидает оплаты'
            },
            'success': {
                color: '#97BE43',
                title: 'Успешно оплачен'
            },
            'failed': {
                color: '#D22B2B',
                title: 'Отменен'
            },
        }
        try {
            return statusMap[status]
        } catch (e) {
            return {title: '', color: '#000'};
        }
    }

    const renderItem = ({ item }) => {
        const itemOnPress = () => {
            navigation.goBack();
            return navigation.navigate('MyRoutes', {
                screen: 'MyRoutesItem',
                initial: false,
                params: {
                    id: item.id
                }
            });
        }
        cleandRoutObject = {
            ...item.tour.route,
            tours: [],
            schedules: [],
        }
        return (
            <View style={styles.ticketItem}>
                <View style={styles.ticketData}>
                    <View style={styles.ticketCell}>
                        <Text style={styles.ticketHint}>Дата и время тура:</Text>
                        <Text style={styles.ticketDataDate}>{moment(item.day.start).format('D MMMM, в HH:MM')}</Text>
                    </View>
                    <View style={styles.ticketCell}>
                        <Text style={styles.ticketHint}>Статус оплаты:</Text>
                        <Text style={{
                            ...styles.ticketDataStatus,
                            color: getHumanStatus(item.status).color
                        }}>
                            {getHumanStatus(item.status).title}
                        </Text>
                    </View>
                </View>
                <View style={styles.ticketData}>
                    <View style={styles.ticketCell}>
                        <Text style={styles.ticketHint}>Сумма заказа:</Text>
                        <Text style={styles.ticketDataAmount}>{new Intl.NumberFormat('ru-RU').format(item.amount)} ₽</Text>
                    </View>
                    <View style={styles.ticketCell}>
                        <Text style={styles.ticketHint}>Количество билетов:</Text>
                        <Text style={styles.ticketDataCount}>{item.count}</Text>
                    </View>
                </View>
                <View style={styles.ticketRoute}>
                    <RouteListItem
                        item={cleandRoutObject}
                        navigation={navigation}
                        onPress={itemOnPress}
                    />
                </View>
            </View>
        )
    }
    
    return (
        <View style={styles.container}>
            <Text style={styles.header}>Купленные туры</Text>
            <FlatList
                data={buyedroutes?.length > 0 ? buyedroutes : notFoundList}
                renderItem={buyedroutes?.length > 0 ? renderItem : renderNotFound}
                keyExtractor={(item, index) => item + index}
                style={styles.list}
                showsVerticalScrollIndicator={false}
            >
            </FlatList>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    list: {
        flex: 1,
    },
    header: {
        ...DefaultStyles.header,
        marginBottom: 24,
        marginHorizontal: 16
    },
    notFound: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        height: '100%',
        marginVertical: '30%'
    },
    notFoundText: {
        ...DefaultStyles.font,
        fontSize: 20,
        color: '#aaa',
        textAlign: 'center',
        marginVertical: 32,
        marginHorizontal: 32
    },
    ticketItem: {
        padding: 16,
        marginHorizontal: 16,
        borderRadius: 12,
        borderWidth: 1,
        borderColor: '#eee',
        backgroundColor: BACKGROUND_COLOR,
        shadowColor: '#eee',
        shadowOffset: { width: 0, height: 4},
        shadowOpacity: 0.5,
        marginBottom: 48,
    },
    ticketData: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 16,
    },
    ticketCell: {
        width: '50%'
    },
    ticketDataDate: {
        ...DefaultStyles.fontBold,
        fontSize: 16
    },
    ticketDataStatus: {
        ...DefaultStyles.fontBold,
    },
    ticketDataCount: {
        ...DefaultStyles.fontBold,
        fontSize: 16
    },
    ticketDataAmount: {
        ...DefaultStyles.fontBold,
        fontSize: 16
    },
    ticketHint: {
        color: '#999',
        marginBottom: 4
    },
    ticketRoute: {
        marginHorizontal: -28,
        marginBottom: -64
    },
});
import React from 'react';
import DefaultStyles from '../constance/styles';
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';


export const MyRoutesNotFound = () => {
    return (
        <View style={styles.notFound}>
            <Ionicons name={'git-branch-outline'} color={'#efefef'} size={100} />
            <Text style={styles.notFoundText}>
                Начните создавать собственные маршруты по Уралу!
                Тематические, семейные, короткие или длинные — используйте приложение как конструктор для любых ваших идей. 
            </Text>
        </View>
    );
}

export const MyRoutesListItem = ({ item, onPress = null, onLongPress = null }) => {
    return (
        <TouchableOpacity
            style={styles.item}
            onPress={onPress}
            onLongPress={onLongPress}
        >
            <Text style={styles.title}>{item.title}</Text>
            <Text style={styles.counter}>{item.points?.length}</Text>
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    item: {
        paddingVertical: 24,
        borderBottomWidth: 1,
        borderBottomColor: '#efefef',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    title: {
        ...DefaultStyles.regularFont,
        fontSize: 16
    },
    counter: {
        ...DefaultStyles.regularFont,
        fontSize: 16,
        color: '#999'
    },
    notFound: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        height: '100%',
        marginVertical: '30%'
    },
    notFoundText: {
        ...DefaultStyles.font,
        fontSize: 20,
        color: '#aaa',
        textAlign: 'center',
        marginVertical: 32,
        marginHorizontal: 32
    },
})
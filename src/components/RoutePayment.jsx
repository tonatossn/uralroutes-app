import React, { useEffect, useState } from 'react';
import { Ionicons } from '@expo/vector-icons';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Alert,
    ScrollView, 
    TouchableOpacity,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard,
    Platform,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useForm, Controller } from "react-hook-form";
import DeviceInfo from 'react-native-device-info';
import { WebView } from 'react-native-webview';
import moment from 'moment';
import 'moment/locale/ru';

import DefaultStyles, { BACKGROUND_COLOR, DEFAULT_COLOR, FormStyles } from '../constance/styles';
import { AppButton } from './elements/AppButton';
import { createTicket, clearPayment } from '../redux/actions/ticket';


const ResultWrapper = ({ onPress, icon, color, headerText, text }) => {
    return (
        <View style={styles.resultContainer}>
            <Ionicons name={icon} color={color} size={100} />
            <Text style={styles.resultHeader}>{headerText}</Text>
            <Text style={styles.resultText}>{text}</Text>
            <AppButton
                title={'Закрыть'}
                style={styles.resultButton}
                onPress={onPress}
            />
        </View>
    )
}

const SuccessResult = ({ onPress }) => {
    return (
        <ResultWrapper
            onPress={onPress}
            icon='checkmark-done-circle-outline'
            color='#97BE43'
            headerText="Оплата прошла успешно!"
            text="Поздравляем, ваш билет вы сможете найти в пользовательком меню под пунктом «Купленные туры». Так же, мы отправили подтверждение на вашу электронную почту."
        />
    )
}

const FailedResult = ({ onPress }) => {
    return (
        <ResultWrapper
            onPress={onPress}
            icon='alert-circle-outline'
            color='#D22B2B'
            headerText='Оплата не прошла'
            text='К сожалению, провести оплату не удалось. Если вы отменили ее самостоятельно, попробуйте оформить покупку заново. В противном случае, свяжитесь с администратором.'
        />
    )
}

const RoutePaymentForm = ({ submitTitle, onSubmit }) => {
    const { control, handleSubmit, formState: { errors } } = useForm();
    
    return (
        <>
            <View style={styles.field}>
                <Text style={styles.label}>Имя и фамилия</Text>
                <Controller
                    control={control}
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            placeholder='Как вас зовут?'
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            autoComplete='name'
                            value={value}
                        />
                    )}
                    name='name'
                />
                {errors.name && <Text style={styles.error}>Это обязательное поле</Text>}
            </View>
            <View style={styles.field}>
                <Text style={styles.label}>Телефон</Text>
                <Controller
                    control={control}
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            placeholder='Укажите телефон'
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            keyboardType='phone-pad'
                            autoComplete='tel'
                            value={value}
                        />
                    )}
                    name='phone'
                />
                {errors.name && <Text style={styles.error}>Это обязательное поле</Text>}
            </View>
            <View style={styles.field}>
                <Text style={styles.label}>Ваш e-mail</Text>
                <Controller
                    control={control}
                    rules={{
                        required: true,
                        pattern: {
                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                            message: 'Эл. почта указана некорректно'
                        }
                    }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            placeholder='name@host.domain'
                            style={styles.input}
                            autoComplete='email'
                            textContentType='emailAddress'
                            keyboardType='email-address'
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                        />
                    )}
                    name='email'
                />
                {errors.email && <Text style={styles.error}>Это обязательное поле, вы должны указать валидный адрес почты</Text>}
            </View>
            <AppButton
                title={submitTitle}
                onPress={handleSubmit(onSubmit)}
            />
        </>
    )
}

const PaymentView = ({ routeItem, day }) => {
    const dispatch = useDispatch();
    const [ amount, setAmount ] = useState(0);
    const [ ticketCount, setTicketCount ] = useState(1);

    const tour = () => {
        try {
            return routeItem.tours[0]
        } catch (e) {
            return false;
        }
    }

    useEffect(() => {
        setAmount(ticketCount * tour().price)
    }, [ticketCount]);

    const onSubmit = async (data) => {
        data = {
            ...data,
            count: ticketCount,
            day: day.id,
            user_device_id: DeviceInfo.getUniqueId()
        }
        dispatch(createTicket(data));
    }

    return (
        <ScrollView style={styles.containerInner}>
            <Text style={styles.header}>{routeItem?.title}</Text>
            <Text style={styles.subHeader}>
                Тур Уральской индустриальной биеннале современного искусства
            </Text>
            <Text style={styles.date}>{moment(day.start).format('D MMMM, в HH:MM')}{'\n'}</Text>
            
            <View style={styles.ticketCounter}>
                <View style={styles.ticketCounterPrice}>
                    <Text style={styles.ticketCounterPricePrice}>
                        {new Intl.NumberFormat('ru-RU').format(tour().price)}₽
                    </Text>
                    <Text style={styles.ticketCounterPriceSubtext}>
                        за каждый билет
                    </Text>
                </View>
                <Text style={styles.ticketCounterCount}>{ticketCount}</Text>
                <View style={styles.ticketCounterControl}>
                    <TouchableOpacity
                        style={styles.ticketCounterControlMinus}
                        onPress={() => setTicketCount(
                            ticketCount > 1 ? ticketCount - 1 : 1
                        )}
                    >
                        <Ionicons style={styles.itemIcon} name={'remove-outline'} color={DEFAULT_COLOR} size={24} />
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.ticketCounterControlPlus}
                        onPress={() => {
                            if (ticketCount >= day.available_places) {
                                Alert.alert('К сожалению, больше билетов нет')
                            }
                            setTicketCount(
                                ticketCount < day.available_places ? ticketCount + 1: day.available_places
                            )
                        }}
                    >
                        <Ionicons style={styles.itemIcon} name={'add-outline'} color={DEFAULT_COLOR} size={24} />
                    </TouchableOpacity>
                </View>
            </View>

            <View style={styles.form}>
                <RoutePaymentForm
                    submitTitle={`Оплатить ${new Intl.NumberFormat('ru-RU').format(amount)}₽`}
                    onSubmit={onSubmit}
                />
            </View>
        </ScrollView>
    )
}

export const RoutePayment = ({ navigation, routeItem, day}) => {
    const dispatch = useDispatch();
    const [ frameStatus, setFrameStatus ] = useState('form');
    const currentPayment = useSelector(state => state.ticket.currentPayment);
    const [ paymentGateUrl, setPaymentGateUrl ] = useState();

    const FRAME_STATE_LIST = {
        'form': (
            <PaymentView
                routeItem={routeItem}
                day={day}
            />
        ),
        'payment': (
            <WebView 
                style={styles.webview}
                source={{ uri: paymentGateUrl }}
                automaticallyAdjustContentInsets={false}
                onMessage={(e) => {
                    const PAYMENT_STATUS_MAP = {
                        'paymentSuccess': 'success',
                        'paymentFailed': 'failed',
                    }
                    setFrameStatus(
                        PAYMENT_STATUS_MAP[e.nativeEvent['data']]
                    );
                }}
            />
        ),
        'success': <SuccessResult onPress={() => navigation.goBack()} />,
        'failed': <FailedResult onPress={() => navigation.goBack()} />
    };

    useEffect(() => {
        dispatch(clearPayment());
    }, []);

    useEffect(() => {
        if (Object.keys(currentPayment).length === 0) {
            return;
        }

        setPaymentGateUrl(
            currentPayment['redirect_url']
        );
        setFrameStatus('payment');
    }, [dispatch, currentPayment]);

    return (
        <KeyboardAvoidingView
            enabled
            behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            keyboardVerticalOffset={100}
            style={styles.keyboardContainer}
        >
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                {FRAME_STATE_LIST[frameStatus]}
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
    )
}
const CONTROL_HEIGHT = 44;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 16,
    },
    keyboardContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    containerInner: {
        flex: 1,
        padding: 12,
    },
    header: {
        ...DefaultStyles.header,
        marginBottom: 12
    },
    subHeader: {
        marginBottom: 12
    },
    date: {
        ...DefaultStyles.fontBold,
        marginBottom: 12
    },
    ticketCounter: {
        paddingHorizontal: 12,
        paddingVertical: 24,
        borderRadius: 12,
        backgroundColor: DEFAULT_COLOR,
        shadowColor: '#eee',
        shadowOffset: { width: 0, height: 4},
        shadowOpacity: 0.5,
        marginBottom: 24,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    ticketCounterPrice: {},
    ticketCounterPricePrice: {
        color: BACKGROUND_COLOR,
        ...DefaultStyles.fontBold,
        fontSize: 24
    },
    ticketCounterPriceSubtext: {
        color: BACKGROUND_COLOR,
        fontSize: 12
    },
    ticketCounterCount: {
        height: CONTROL_HEIGHT,
        lineHeight: CONTROL_HEIGHT,
        width: 60,
        textAlign: 'center',
        borderRadius: 12,
        borderWidth: 1,
        borderColor: `${BACKGROUND_COLOR}80`,
        overflow: 'hidden',
        color: BACKGROUND_COLOR,
        ...DefaultStyles.fontBold,
        fontSize: 18,
        backgroundColor: `${BACKGROUND_COLOR}30`
    },
    ticketCounterControl: {
        flexDirection: 'row',
    },
    ticketCounterControlMinus: {
        width: 60,
        height: CONTROL_HEIGHT,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: BACKGROUND_COLOR,
        borderTopLeftRadius: 22,
        borderBottomLeftRadius: 22,
        borderRightWidth: 1,
        borderRightColor: `${DEFAULT_COLOR}30`,
    },
    ticketCounterControlPlus: {
        width: 60,
        height: CONTROL_HEIGHT,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: BACKGROUND_COLOR,
        borderTopRightRadius: 22,
        borderBottomRightRadius: 22,
    },
    form: {
        paddingBottom: 40
    },
    webview: {
        flex: 1,
    },
    resultContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    resultHeader: {
        ...DefaultStyles.font,
        fontSize: 20,
        color: '#aaa',
        marginTop: 12,
        marginBottom: 24,
        marginHorizontal: 24,
        textAlign: 'center',
    },
    resultText: {
        fontSize: 14,
        textAlign: 'center',
        marginHorizontal: 24,
        marginBottom: 32,
    },
    resultButton: {
        width: 200
    },
    ...FormStyles
});
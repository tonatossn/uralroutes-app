import React, { useContext } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, ScrollView, ImageBackground} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import DefaultStyles from '../constance/styles';
import { ButtonAddToBookmark  } from './elements/ButtonAddToBookmark';
import { geoDistance, listItemDistanceString } from '../utils/distance';
import { GeoContext } from '../containers/GeoWrapper';


const ContextPointItem = ({ navigation, item, index }) => {
    const location = useContext(GeoContext);
    const placeholder = require('../../assets/placeholder_point.png');

    return (
        <TouchableOpacity
            activeOpacity={0.8}
            style={index == 0 ? {...styles.item, marginLeft: 16} : styles.item}
            onPress={() => {
                return navigation.navigate('Point', {
                    screen: 'PointItem',
                    initial: false,
                    params: {
                        id: item.id
                    }
                })
            }}
        >
        <View style={styles.itemWrapper}>
            <ImageBackground
                source={item.cover ? { uri: item.cover } : placeholder}
                resizeMode={'cover'}
                style={styles.image}
            >
                <ButtonAddToBookmark contentType={'points'} id={item.id} />
                <Text style={styles.title}>{item.title}</Text>
            </ImageBackground>
            <View style={styles.distance}>
                <Ionicons name="time-outline" color='#000' size={16} style={styles.distanceIcon}/>
                <Text style={styles.distanceText}>
                    {listItemDistanceString(
                        geoDistance(
                            {
                                from: { lat: location.latitude, lon: location.longitude},
                                to: { lat: item.geodata.coordinates[1], lon: item.geodata.coordinates[0] }
                            },
                        ), true
                    )}
                </Text>
            </View>
        </View>
        </TouchableOpacity>
    )
}

export const ContextPoints = ({ title = null, navigation, points }) => {
    return (points && points.length > 0 ? (
        <View style={styles.container}>
            {title ? <Text style={styles.header}>{title}</Text> : ''}

            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={styles.scrollContainer}>
                {points.map((point, i) => {
                    return <ContextPointItem key={point.id} item={point} navigation={navigation} index={i} />
                })}
            </ScrollView>
        </View>
    ) : <></>);
}

const ITEM_WIDTH = 300;
const ITEM_HEIGHT = 200;

const styles = StyleSheet.create({
    container: {
        marginBottom: 16,
    },
    scrollContainer: {
    },
    header: {
        ...DefaultStyles.font,
        fontSize: 24,
        marginBottom: 16,
        marginLeft: 16
    },
    item: {
        width: ITEM_WIDTH,
        height: ITEM_HEIGHT,
        marginRight: 16,
        marginBottom: 16,
        marginTop: 16,
        top: -16
    },
    itemWrapper: {
        ...DefaultStyles.contextWrapper,
    },
    image: {
        width: ITEM_WIDTH,
        height: ITEM_HEIGHT,
        ...DefaultStyles.contextShadowedImage,
    },
    title: {
        ...DefaultStyles.contextShadowedTitle,
        marginBottom: 32,
        marginLeft: 16,
    },
    distance: {
        ...DefaultStyles.contextDistanceBaloon,
    },
    distanceIcon: {
        marginRight: 6
    },
    distanceText: {
        ...DefaultStyles.font,
    }
})
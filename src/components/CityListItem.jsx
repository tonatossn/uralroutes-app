import React from 'react';
import { useSelector } from 'react-redux';
import { StyleSheet, TouchableOpacity, Text, View, ImageBackground } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';

import { geoDistance, listItemDistanceString } from '../utils/distance';
import DefaultStyles from '../constance/styles';

export const CityListItem = ({ navigation, item, userLocation }) => {
    const placeholder = require('../../assets/placeholder_city.png');
    const cityGeodata = useSelector(state => state.location.addressGeodata[item.id]);
    
    return (
        <TouchableOpacity activeOpacity={0.8} style={styles.item} onPress={() => {
            navigation.goBack();
            return navigation.navigate('City', {
                screen: 'CityItem',
                initial: false,
                params: {
                    id: item.id
                }
            });
        }}>
		    <View style={styles.itemWrapper}>
                <ImageBackground
                    source={item.cover ? { uri: item.cover } : placeholder}
                    resizeMode="cover"
                    style={styles.image}>
                    <LinearGradient
                        colors={[`#00000000`, `#00000000`, '#000']}
                        start={{x: 0, y: 0}} end={{x: 0, y: 1}}
                        style={styles.gradient}
                    >
                        <Text style={styles.title}>{item.title}</Text>
                    </LinearGradient>
                </ImageBackground>
                <View style={styles.distance}>
                    <Ionicons name="time-outline" color='#000' size={16} style={styles.distanceIcon}/>
                    <Text style={styles.distanceText}>
                        {cityGeodata ? listItemDistanceString(
                            geoDistance(
                                {
                                    from: { lat: userLocation.latitude, lon: userLocation.longitude},
                                    to: { lat: cityGeodata.latitude, lon: cityGeodata.longitude }
                                },
                            ), true
                        ) : '...'}
                    </Text>
                </View>
            </View>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
	item: {
        flex: 1,
        marginBottom: 40,
        marginRight: 12,
        marginLeft: 12,
        height: 200,
	},
    itemWrapper: {
        ...DefaultStyles.contextWrapper
    },
    image: {
        height: 200,
        ...DefaultStyles.contextShadowedImage,
    },
    title: {
        ...DefaultStyles.font,
        fontSize: 18,
        color: '#fff',
        marginBottom: 32,
        marginLeft: 16
    },
    distance: {
        ...DefaultStyles.contextDistanceBaloon,
    },
    gradient: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    distanceIcon: {
        marginRight: 6
    },
    distanceText: {
        ...DefaultStyles.font,
    }
});

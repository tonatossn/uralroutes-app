import React, { useRef, useState, useEffect } from 'react';
import MapboxGL from '@react-native-mapbox-gl/maps';
import humanizeDuration from 'humanize-duration';
import getGmapDirections from '../utils/getGmapDirections';
import { StyleSheet, Text, View } from 'react-native';
import { getDirectionRequest } from '../api/mapbox';

import { resetPropertyOfFeatures, updatePropertyOfFeature } from '../utils/featureTools';
import DefaultStyles, { DEFAULT_COLOR } from '../constance/styles';
import { PointMapModal } from './PointMapModal';
import { AppButton } from './elements/AppButton';


export const RouteMap = ({ navigation, points, height=500 }) => {
    const mapRef = useRef(null);
    const [ userLocation, setUserLocation ] = useState({});
    const [ directionData, setDirectionData ] = useState(null);
    const [ directionFeatures, setDirectionFeatures ] = useState(null);
    const [ startEndFeatures, setStartEndFeatures ] = useState(null);
    const [ pointFeatures, setPointFeatures ] = useState(null);
    const [ modalIsVisible, setModalIsVisible ] = useState(false);
    const [ modalData, setModalData ] = useState({})
    
    const firstPoint = points[0].geodata.coordinates;
    const lastPoint = points[ points.length -1 ].geodata.coordinates;
    const waypoints = points.filter((point, index) => (
        index != 0 && index != points.length - 1
    )).map(point => point.geodata.coordinates);
    
    const layerStyles = {
        points: {
            circleRadius: ['get', 'circleRadius'],
            circleColor: DEFAULT_COLOR,
        },
        route: {
            lineColor: DEFAULT_COLOR,
            lineCap: 'round',
            lineJoin: 'round',
            lineWidth: 5,
            lineOpacity: 0.50,
        },
    };

    const getDirectionFeatures = async (points) => {
        if (points === undefined) {
            return {};
        }
        const routeData = await getDirectionRequest(points);
        setDirectionData(routeData);
    }
    
    useEffect(() => {
        if (!directionData) return;

        setDirectionFeatures({
            type: 'Feature',
            properties: {},
            geometry:{
                type: 'LineString',
                coordinates: directionData.coordinates,
            },
        });
    }, [directionData])

    useEffect(() => {
        getDirectionFeatures(points);
        setStartEndFeatures({
            'type': 'FeatureCollection',
            'features': [
                ...[points[0], points[ points.length -1 ]].map(point => (
                    {
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Point',
                            'coordinates': point.geodata.coordinates
                        },
                        'properties': {
                            ...point,
                            'iconSize': 0.4,
                        },
                    }
                ))
            ]
        });
        
        setPointFeatures({
            'type': 'FeatureCollection',
            'features': [
                ...points.filter((point, index) => (
                    index != 0 && index != points.length - 1
                )).map(point => (
                    {
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Point',
                            'coordinates': point.geodata.coordinates
                        },
                        'properties': {
                            ...point,
                            'circleRadius': 6,
                        },
                    }
                ))
            ]
        })
    }, [points]);

    useEffect(() => {
        if (!startEndFeatures || !pointFeatures) {
            return;
        }

        if (!modalIsVisible) {
            setStartEndFeatures(resetPropertyOfFeatures(
                startEndFeatures, { iconSize: 0.4 }
            ));
            setPointFeatures(resetPropertyOfFeatures(
                pointFeatures, { circleRadius: 6 }
            ));
        }
    }, [modalIsVisible]);

    return (
        <View>
            <PointMapModal
                navigation={navigation}
                isVisible={modalIsVisible}
                setIsVisible={setModalIsVisible}
                userLocation={userLocation.coords}
                item={modalData}
                hasBackdrop={true}
                coverScreen={true}
            />
            
            <MapboxGL.MapView
                styleURL={'mapbox://styles/mapbox/dark-v10'}
                style={{
                    ...styles.map,
                    height: height
                }}
                localizeLabels={true}
                ref={mapRef}
            >
                <MapboxGL.Camera
                    zoomLevel={9}
                    centerCoordinate={firstPoint}
                />
                <MapboxGL.UserLocation
                    visible={false}
                    onUpdate={setUserLocation}
                />

                {directionFeatures ? <MapboxGL.ShapeSource
                    id="directionRouteSource"
                    shape={directionFeatures}
                >
                    <MapboxGL.LineLayer
                        id="directionRouteSource"
                        minZoomLevel={1}
                        style={layerStyles.route}
                    />
                </MapboxGL.ShapeSource> : <></>}

                {startEndFeatures ? <MapboxGL.ShapeSource
                    id="startEndPoint"
                    hitbox={{width: 20, height: 20}}
                    shape={startEndFeatures}
                    onPress={point => {
                        const obj = point.features[0].properties;
                        navigation.setParams({visible: false})
                        setModalData(obj)
                        setModalIsVisible(true);
                        setStartEndFeatures(
                            updatePropertyOfFeature(
                                resetPropertyOfFeatures(
                                    startEndFeatures, { iconSize: 0.4 }
                                ),
                                obj.id,
                                { iconSize: 0.7 }
                            )
                        )
                    }}
                >
                    <MapboxGL.SymbolLayer
                        id="startEndPoint"
                        style={{
                            iconImage: require('../../assets/point_marker.png'),
                            iconSize: ['get', 'iconSize'],
                            iconAllowOverlap: true,
                        }}
                    />
                </MapboxGL.ShapeSource> : <></>}
                
                {pointFeatures ? <MapboxGL.ShapeSource
                    id="points"
                    shape={pointFeatures}
                    onPress={point => {
                        const obj = point.features[0].properties;
                        navigation.setParams({visible: false})
                        setModalData(obj)
                        setModalIsVisible(true);
                        setPointFeatures(
                            updatePropertyOfFeature(
                                resetPropertyOfFeatures(
                                    pointFeatures, { circleRadius: 6 }
                                ),
                                obj.id,
                                { circleRadius: 10 }
                            )
                        )
                    }}
                >
                    <MapboxGL.CircleLayer
                        id="points"
                        style={layerStyles.points}
                    />
                </MapboxGL.ShapeSource> : <></>}
            </MapboxGL.MapView>
            {directionData ? <Text style={styles.realMetadata}>
                <Text style={styles.realMetadataDistance}>
                    {Math.round(directionData.meta.distance / 1000 * 100) / 100} км,{' '}
                </Text>
                {humanizeDuration(directionData.meta.duration * 1000, {
                    language: 'ru',
                    units: ["h", "m"],
                    round: true
                })}
            </Text> : <></>}
            <View style={styles.startRouteButton}>
                <AppButton
                    title='Открыть в навигаторе'
                    onPress={() => {
                        getGmapDirections({
                            destination: {
                                latitude: lastPoint[1],
                                longitude: lastPoint[0]
                            },
                            params: [
                                {
                                    key: "travelmode",
                                    value: "driving"
                                },
                            ],
                            waypoints: [
                                {
                                    latitude: firstPoint[1],
                                    longitude: firstPoint[0]
                                },
                                ...waypoints.map(item => (
                                    {
                                        latitude: item[1],
                                        longitude: item[0],
                                    }
                                ))
                            ]
                        })
                    }}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    subheader: {
        ...DefaultStyles.headerMiddle,
        marginBottom: 24
    },
    map: {
        width: 'auto',
        marginBottom: 24,
        marginLeft: -16,
        marginRight: -16,
    },
    realMetadata: {
        marginBottom: 24,
        marginTop: -16,
        ...DefaultStyles.font,
        color: '#999'
    },
    realMetadataDistance: {
        color: 'black'
    },
    startRouteButton: {
        marginBottom: 24
    },
});

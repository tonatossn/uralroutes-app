import React from 'react';
import Modal from "react-native-modal";
import { StyleSheet, View, Dimensions } from 'react-native';
import { SliderBox } from "react-native-image-slider-box";
import ImageViewing from "react-native-image-viewing";
import { Ionicons } from '@expo/vector-icons';

import { DEFAULT_COLOR } from '../constance/styles';


export const ImageModal = ({ images, index, status, setStatus }) => {
    return (
        <ImageViewing
            images={images.map(i => {return {'uri': i}})}
            imageIndex={index}
            visible={status}
            onRequestClose={() => setStatus(!status)}
        />
    )
}

const styles = StyleSheet.create({
});

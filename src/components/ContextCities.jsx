import React, { useContext, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { StyleSheet, TouchableOpacity, Text, View, ScrollView, ImageBackground } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import DefaultStyles from '../constance/styles';
import { geoDistance, listItemDistanceString } from '../utils/distance';
import { collectCachedLocationByData } from '../redux/actions/location';
import { GeoContext } from '../containers/GeoWrapper';


const ContextCityItem = ({ navigation, item, userLocation, index }) => {
    const cityGeodata = useSelector(state => state.location.addressGeodata[item.id]);
    
    return (
        <TouchableOpacity
            activeOpacity={0.8}
            style={index == 0 ? {...styles.item, marginLeft: 16} : styles.item}
            onPress={() => {
                return navigation.navigate('City', {
                    screen: 'CityItem',
                    initial: false,
                    params: {
                        id: item.id
                    }
                })
            }}
        >
            <View style={styles.itemWrapper}>
                <ImageBackground source={item.cover ? { uri: item.cover } : placeholder} resizeMode="cover" style={styles.image}>
                    <Text style={styles.title}>{item.title}</Text>
                </ImageBackground>
                <View style={styles.distance}>
                    <Ionicons name="time-outline" color='#000' size={16} style={styles.distanceIcon}/>
                    <Text style={styles.distanceText}>
                        {cityGeodata ? listItemDistanceString(
                            geoDistance(
                                {
                                    from: { lat: userLocation.latitude, lon: userLocation.longitude},
                                    to: { lat: cityGeodata.latitude, lon: cityGeodata.longitude }
                                },
                            ), true
                        ) : '...'}
                    </Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}

export const ContextCities = ({ title = null, navigation, cities }) => {
    const dispatch = useDispatch();
    const locations = useSelector(state => state.location.addressGeodata);
    const userLocation = useContext(GeoContext);

    useEffect(() => {
        collectCachedLocationByData(dispatch, cities, locations);
    }, [cities]);

    return cities.length ? (
        <View style={styles.container}>
            {title ? <Text style={styles.header}>{title}</Text> : ''}

            <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                style={styles.scrollContainer}
            >
                {cities.map((city, i) => {
                    return(
                        <ContextCityItem
                            key={city.id}
                            item={city}
                            navigation={navigation}
                            index={i}
                            userLocation={userLocation}
                        />
                    )
                })}
            </ScrollView>
        </View>
    ) : null;
}

const ITEM_WIDTH = 300;
const ITEM_HEIGHT = 200;

const styles = StyleSheet.create({
    container: {
        marginBottom: 16,
    },
    scrollContainer: {
    },
    header: {
        ...DefaultStyles.font,
        fontSize: 24,
        marginBottom: 16,
        marginLeft: 16
    },
    item: {
        width: ITEM_WIDTH,
        height: ITEM_HEIGHT,
        marginRight: 16,
        marginBottom: 16,
        marginTop: 16,
        top: -16
    },
    itemWrapper: {
        ...DefaultStyles.contextWrapper,
    },
    image: {
        width: ITEM_WIDTH,
        height: ITEM_HEIGHT,
        ...DefaultStyles.contextShadowedImage,
    },
    title: {
        ...DefaultStyles.contextShadowedTitle,
        marginBottom: 32,
        marginLeft: 16,
    },
    distance: {
        ...DefaultStyles.contextDistanceBaloon,
    },
    distanceIcon: {
        marginRight: 6
    },
    distanceText: {
        ...DefaultStyles.font,
    }
})
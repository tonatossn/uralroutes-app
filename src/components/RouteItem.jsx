import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { SliderBox } from "react-native-image-slider-box";
import moment from 'moment';
import 'moment/locale/ru';

import DefaultStyles, { BACKGROUND_COLOR, DEFAULT_COLOR } from '../constance/styles';
import { listItemDistanceString } from '../utils/distance';
import { countFormatter } from '../utils/countFormatter';
import { TextContent } from './content/TextContent';
import { ImageModal } from './ImageModal';
import { RouteMap } from './RouteMap';


export const RouteItem = ({ navigation, item }) => {
    const [ curentZoomImage, setCurentZoomImage ] = useState(0)
    const [ zoomImageStatus, setZoomImageStatus] = useState(false)

    return (
        <View style={styles.container}>
            <ImageModal
                images={[item.cover, ...item.images.map((item) => item.image )]}
                index={curentZoomImage}
                status={zoomImageStatus}
                setStatus={setZoomImageStatus}
            />
            <Text style={styles.header}>{item.title}</Text>
            <Text style={styles.underline}>
                {listItemDistanceString(item.distance)},
                &nbsp;
                {item.duration != 0 ? countFormatter(item.duration, {
                    one: 'день',
                    two: 'дня',
                    few: 'дней',
                }) : 'меньше дня'},
                &nbsp;
                {item.points.length} {countFormatter(item.points.length, {
                    one: 'место',
                    two: 'места',
                    few: 'мест',
                })}
            </Text>

            <View style={styles.gallery}>
                <SliderBox
                    style={{ aspectRatio: 4 / 3 }}
                    imageLoadingColor={DEFAULT_COLOR}
                    images={[item.cover, ...item.images.map((item) => item.image)]}
                    onCurrentImagePressed={index => {
                        setCurentZoomImage(index);
                        setZoomImageStatus(true);
                    }}
                />
            </View>

            {item.cover_copyright ? (
                <Text style={styles.copyright}>{item.cover_copyright}</Text>
            ) : <></>}

            {item.description ? <Text style={styles.description}>{item.description}</Text> : <></>}
            <TextContent content={item.content} />

            {item.points.length > 0 ? (
                <>
                    <Text style={styles.subheader}>План маршрута</Text>
                    <RouteMap points={item.points} navigation={navigation} />
                </>
            ) : <></>}
            {item.schedules.filter(day => new Date(day.start) > Date.now()).length > 0 ? (
                <View style={styles.schedule}>
                    <View style={styles.scheduleHeader}>
                        <Text style={styles.scheduleTitle}>
                            По этому маршруту есть тур Уральской индустриальной биеннале
                        </Text>
                        <View style={styles.price}>
                            <Text style={styles.priceTitle}>Цена билета на одного человека:</Text>
                            <Text style={styles.priceValue}>
                                {new Intl.NumberFormat('ru-RU').format(item.tours[0]?.price)}₽
                            </Text>
                        </View>
                    </View>

                    {item.schedules.filter(day => new Date(day.start) > Date.now()).map((day, index) =>
                        <View
                            style={{
                                ...styles.scheduleItem,
                                borderBottomWidth: index === item.schedules.length -1 ? 0 : 1
                            }}
                            key={'schedule-' + index}
                        >
                            <Text style={styles.scheduleDate}>
                                {moment(day.start).format('D MMMM, в HH:MM')}{'\n'}
                                <Text style={styles.scheduleDatePlaces}>
                                    Осталось {day.available_places} {countFormatter(day.available_places, {
                                        one: 'билет',
                                        two: 'билета',
                                        few: 'билетов',
                                    }).toString()}
                                </Text>
                            </Text>
                            <TouchableOpacity
                                style={styles.scheduleButton}
                                onPress={() => {
                                    navigation.navigate(
                                        'Route', {
                                            screen: 'RoutePayment',
                                            initial: false,
                                            params: {
                                                title: 'Покупка тура',
                                                day: day,
                                                item: item
                                            }
                                        }
                                    )
                                }}
                            >
                                <Text style={DefaultStyles.buttonTitle}>Купить</Text>
                            </TouchableOpacity>
                        </View>
                    )}
                </View>
            ) : <></>}

            {item.tours.length > 0 ? (
                <>
                    <Text style={styles.subheader}>Программа тура</Text>
                    <View style={styles.program}>
                        <TextContent content={item.tours[0].plan} collapsable={false} />
                    </View>
                </>
            ) : <></>}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        padding: 16,
        flex: 1,
    },
    header: {
        ...DefaultStyles.header,
    },
    subheader: {
        ...DefaultStyles.headerMiddle,
        marginBottom: 24
    },
    underline: {
        color: '#999',
        fontSize: 12,
        marginBottom: 16,
    },
    copyright: {
        ...DefaultStyles.regularFont,
        fontSize: 12,
        marginTop: -8,
        marginBottom: 16,
        color: '#999',
    },
    distance: {},
    gallery: {
        marginBottom: 16,
        marginLeft: -16
    },
    lead: {},
    description: {
        fontFamily: 'Rubik_700Bold',
        marginBottom: 24,
        fontSize: 20
    },
    content: {

    },
    program: {
        // borderRadius: 16,
        backgroundColor: `${DEFAULT_COLOR}50`,
        paddingHorizontal: 16,
        paddingTop: 24,
        marginBottom: 16
    },

    schedule: {
        backgroundColor: DEFAULT_COLOR,
        marginHorizontal: -16,
        paddingHorizontal: 16,
        paddingVertical: 24,
        marginTop: 16,
        marginBottom: 32
    },
    scheduleHeader: {
        flexDirection: 'row',
        marginBottom: 24,
    },
    scheduleTitle: {
        ...DefaultStyles.headerMiddle,
        fontSize: 20,
        lineHeight: 22,
        flex: 3,
        color: BACKGROUND_COLOR,
        paddingRight: 24,
    },
    scheduleItem: {
        borderBottomColor: `${BACKGROUND_COLOR}30`,
        borderBottomWidth: 1,
        paddingVertical: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    scheduleDate: {
        ...DefaultStyles.font,
        fontSize: 16,
        color: BACKGROUND_COLOR
    },
    scheduleDatePlaces: {
        ...DefaultStyles.regularFont,
        fontSize: 14,
        color: `${BACKGROUND_COLOR}70`,
    },
    scheduleButton: {
        ...DefaultStyles.buttonContainer,
        height: 32,
        paddingHorizontal: 16,
    },
    price: {
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        flex: 2
    },
    priceTitle: {
        color: BACKGROUND_COLOR,
        textAlign: 'right'
    },
    priceValue: {
        color: BACKGROUND_COLOR,
        ...DefaultStyles.fontBold,
        fontSize: 32
    },
});


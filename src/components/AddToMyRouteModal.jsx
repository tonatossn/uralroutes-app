import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import DefaultStyles, { FormStyles } from '../constance/styles';
import {
    View,
    Text,
    FlatList,
    StyleSheet,
    TextInput,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard,
    Vibration,
    Alert,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Modal from "react-native-modal";
import { useForm, Controller } from "react-hook-form";

import { AppButton } from './elements/AppButton';
import { MyRoutesNotFound, MyRoutesListItem } from './MyRoutesListItem';
import { createRoute, addPointToRoute, removeRoute } from '../redux/actions/myroutes';


const CreateMyRouteForm = ({ onSubmit }) => {
    const { control, handleSubmit, formState: { errors } } = useForm();
    return (
        <View style={styles.createRouteForm}>
            <View style={styles.field}>
                <Text style={styles.label}>Придумайте название</Text>
                <Controller
                    control={control}
                    rules={{
                        required: true
                    }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            placeholder='Напр., «Лучшие места Урала»'
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                        />
                    )}
                    name='title'
                />
                {errors.email && <Text style={styles.error}>
                    Это обязательное поле, вы должны указать название своего маршрута
                </Text>}
            </View>
            <AppButton title={'Создать маршрут'} onPress={handleSubmit(onSubmit)} />
        </View>
    )
}

const CreateMyRouteModal = ({
    isVisible,
    setIsVisible,
}) => {
    const [ submited, setSubmited ] = useState(false);
    const dispatch = useDispatch();
    const onSubmit = (data) => {
        dispatch(createRoute(data['title']))
        setSubmited(true);
        setIsVisible(false);
    }

    useEffect(() => {
        setSubmited(false);
    })

    return (
        <Modal
            swipeDirection={'down'}
            hardwareAccelerated={true}
            coverScreen={true}
            hasBackdrop={true}
            isVisible={isVisible}
            onSwipeComplete={() => setIsVisible(!isVisible)}
            style={styles.modal}
        >
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                style={styles.modalKeyboardContainer}
            >
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={styles.modalContainer}>
                        <View style={styles.modalHeader}>
                            <Text style={styles.modalHeaderText}>Создание нового маршрута</Text>
                            <Ionicons
                                name="close-circle-outline"
                                color={'#000'}
                                size={32}
                                onPress={() => setIsVisible(!isVisible)}
                            />
                        </View>
                        {submited ? <></> : <CreateMyRouteForm onSubmit={onSubmit} />}
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        </Modal>
    )
}

export const AddToMyRouteModal = ({ navigation, addedPoint, myroutes }) => {
    const [ createModalisVisible, setCreateModalIsVisible ] = useState(false);
    const notFoundList = [{id: 'notfound-404', title: 'Not found'}];
    const dispatch = useDispatch();
    
    const renderNotFound = ({ item }) => {
        return <MyRoutesNotFound />;
    }

    const renderItem = ({ item }) => {
        return (
            <MyRoutesListItem
                item={item}
                onPress={() => {
                    if (item.points.filter(point => point.id == addedPoint.id).length > 0) {
                        Alert.alert('В этом маршруте уже есть такой объект');
                        return
                    }
                    dispatch(
                        addPointToRoute(addedPoint, item.id, item.points.length)
                    );
                    navigation.goBack();
                }}
                onLongPress={() => {
                    Vibration.vibrate();
                    Alert.alert(
                        'Удалить маршрут',
                        'Вы уверены, что хотите удалить этот маршрут?',
                        [
                            {
                                text: 'Отмена',
                                style: 'cancel',
                            },
                            {
                                text: 'Да',
                                onPress: () => {
                                    dispatch(removeRoute(item.id))
                                }
                            },
                        ]
                    );
                }}
            />
        )
    }

    return (
        <View style={styles.container}>
            <CreateMyRouteModal
                isVisible={createModalisVisible}
                setIsVisible={setCreateModalIsVisible}
            />
            <FlatList
                data={myroutes?.length > 0 ? myroutes : notFoundList}
                renderItem={myroutes?.length > 0 ? renderItem : renderNotFound}
                keyExtractor={(item, index) => item + index}
                style={styles.list}
            />
            <AppButton
                title={'Создать новый маршрут'}
                style={styles.clearFilterButton}
                onPress={() => {
                    setCreateModalIsVisible(true)
                }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 16,
        paddingBottom: 24,
        flex: 1,
        flexDirection: 'column',
    },
    list: {
        flex: 1,
        flexGrow: 1,
        flexBasis: 'auto'
    },
    clearFilterButton: {
        // flex: 1,
        flexBasis: 60,
    },

    modal: {
        margin: 0,
        padding: 0,
        justifyContent: 'flex-end',
    },
    modalKeyboardContainer: {
        margin: 0,
    },
    modalContainer: {
        backgroundColor: '#fff',
        height: 300,
        width: '100%',
        padding: 16,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        overflow: 'hidden',
    },
    modalHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignContent: 'center',
        alignItems: 'center',
        height: 50,
    },
    modalHeaderText: {
        ...DefaultStyles.headerSmall
    },
    
    ...FormStyles
})
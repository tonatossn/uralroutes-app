import React, { useState, useEffect, useContext } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { SliderBox } from "react-native-image-slider-box";
import { useSelector, useDispatch } from 'react-redux';

import DefaultStyles, { DEFAULT_COLOR } from '../constance/styles';
import { getAddressLocation } from '../redux/actions/location';
import { GeoContext } from '../containers/GeoWrapper';
import { TextContent } from './content/TextContent';
import { geoDistance, listItemDistanceString } from '../utils/distance';
import { ImageModal } from './ImageModal';


export const CityItem = ({ navigation, item }) => {
    const dispatch = useDispatch();
    const userLocation = useContext(GeoContext);
    const [ curentZoomImage, setCurentZoomImage ] = useState(0)
    const [ zoomImageStatus, setZoomImageStatus] = useState(false)

    useEffect(() => {
        dispatch(getAddressLocation(item.id, item.title));
    }, [dispatch, item.id])
    
    const cityGeodata = useSelector(state => state.location.addressGeodata[item.id])
    
    return (
        <View style={styles.container}>
            <ImageModal
                images={[item.cover, ...item.images.map((item) => item.image )]}
                index={curentZoomImage}
                status={zoomImageStatus}
                setStatus={setZoomImageStatus}
            />
            <Text style={styles.header}>{item.title}</Text>
            <Text style={styles.underline}>
                {cityGeodata ? listItemDistanceString(
                    geoDistance(
                        {
                            from: { lat: userLocation.latitude, lon: userLocation.longitude},
                            to: { lat: cityGeodata.latitude, lon: cityGeodata.longitude }
                        },
                    ), true
                ) : '...'}
            </Text>

            { item.cover ? <View style={styles.gallery}>
                <SliderBox
                    style={{aspectRatio: 4/3}}
                    images={[item.cover, ...item.images.map((item) => item.image )]}
                    imageLoadingColor={DEFAULT_COLOR}
                    onCurrentImagePressed={index => {
                        setCurentZoomImage(index);
                        setZoomImageStatus(true);
                    }}
                />
            </View> : <></> }

            {item.cover_copyright ? (
                <Text style={styles.copyright}>{item.cover_copyright}</Text>
            ) : <></>}

            {item.description ? <Text style={styles.description}>{item.description}</Text> : <></>}
            <TextContent content={item.content} />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        padding: 16,
        flex: 1,

    },
    header: {
        ...DefaultStyles.header,
    },
    underline: {
        color: '#999',
        fontSize: 12,
        marginBottom: 16,
    },
    copyright: {
        ...DefaultStyles.regularFont,
        fontSize: 12,
        marginTop: -8,
        marginBottom: 16,
        color: '#999',
    },
    distance: {},
    gallery: {
        marginBottom: 16,
        marginLeft: -16
    },
    lead: {},
    description: {
        fontWeight: 'bold',
        marginBottom: 24,
        fontSize: 20
    },
    metadata: {},
    map: {},
});


import AppLoading from "expo-app-loading";
import { Asset } from "expo-asset";
import Constants from "expo-constants";
import * as SplashScreen from "expo-splash-screen";
import React, { useEffect, useMemo, useState } from "react";
import {
	Animated,
	StyleSheet,
	View,
	ImageBackground,
	Dimensions,
    Easing,
} from "react-native";
import { useFonts, 
    Rubik_400Regular,
	Rubik_500Medium,
    Rubik_700Bold,
	Rubik_400Regular_Italic,
} from '@expo-google-fonts/rubik';
import { useDispatch } from 'react-redux';
import { fetchRoutes, fetchRoutesOnIndex } from '../redux/actions/route';
import { fetchPoints, fetchPointsBiennaleChoice, fetchCategories } from '../redux/actions/point';
import { fetchAboutData } from '../redux/actions/about';
import { fetchCities } from '../redux/actions/city';

// Instruct SplashScreen not to hide yet, we want to do this manually
SplashScreen.preventAutoHideAsync().catch(() => {
	/* reloading the app might trigger some race conditions, ignore them */
});

const [
	parallelogramHeight,
	parallelogramWidth,
	parallelogramHeightSkew,
	backgroundColor,
	padding,
	cartWidth
] = [
	Dimensions.get('window').height / 3,
	'100%',
	40,
	Constants.manifest.splash.backgroundColor,
	16,
	34
];

export const AnimatedAppLoader = ({ children }) => {
    const splashImage = require('../../assets/splash.png');
	const [isSplashReady, setSplashReady] = React.useState(false);

	const startAsync = React.useMemo(
		() => () => Asset.fromModule(splashImage).downloadAsync(),
		[splashImage]
	);
	
	const onFinish = React.useMemo(() => setSplashReady(true), []);

	if (!isSplashReady) {
		return (
			<AppLoading
				autoHideSplash={false}
				startAsync={startAsync}
				onError={console.error}
				onFinish={onFinish}
			/>
		);
	}

	return (
        <AnimatedSplashScreen children={children} image={splashImage}>
            {children}
        </AnimatedSplashScreen>
    );
}

const AnimatedSplashScreen = ({ children, image }) => {
	const cartImage = require('../../assets/cart.png')
    const dispatch = useDispatch();

	const [isAppReady, setAppReady] = useState(false);
	const [isSplashAnimationComplete, setAnimationComplete] = useState(
		false
	);

	const animationTop = useMemo(() => new Animated.Value(0), []);
	const animationMiddle = useMemo(() => new Animated.Value(0), []);
	const animationBottom = useMemo(() => new Animated.Value(0), []);
	const animationCart = useMemo(() => new Animated.Value(0), []);
	const animationLine = useMemo(() => new Animated.Value(0), []);
    const animationContent = useMemo(() => new Animated.Value(0), []);

    const [fontLoaded] = useFonts({
        Rubik_400Regular,
        Rubik_500Medium,
        Rubik_700Bold,
        Rubik_400Regular_Italic,
    });

	useEffect(() => {
        dispatch(fetchRoutes(1));
        dispatch(fetchRoutesOnIndex());
        dispatch(fetchPoints(1));
        dispatch(fetchPointsBiennaleChoice());
        dispatch(fetchCategories());
        dispatch(fetchCities(1));
		dispatch(fetchAboutData());

		SplashScreen.hideAsync();
		setAppReady(true);
	}, []);

	useEffect(() => {
		if (!isAppReady) {
			return;
		}

		const animatedParams = {
			duration: 1000,
			useNativeDriver: true,
		}
		Animated.parallel([
			Animated.timing(animationTop, {
				...animatedParams,
				toValue: 500,
				delay: 0,
			}),
			Animated.timing(animationMiddle, {
				...animatedParams,
				toValue: -500,
				delay: 200,
			}),
			Animated.timing(animationBottom, {
				...animatedParams,
				toValue: 500,
				delay: 400,
			}),
			Animated.timing(animationCart, {
				...animatedParams,
				duration: 2000,
				delay: 400,
				toValue: (
					Dimensions.get('window').width - padding * 2 - cartWidth
				),
			}),
			Animated.timing(animationLine, {
				...animatedParams,
				duration: 2000,
				delay: 400,
				toValue: Dimensions.get('window').width,
			})
		]).start(() => setAnimationComplete(true));
	}, [isAppReady]);

    useEffect(() => {
        Animated.timing(animationContent, {
			useNativeDriver: true,
            duration: 700,
            delay: 2700,
            toValue: 1,
            easing: Easing.bounce
        }).start()
    }, [])

	const Parallelogram = () => {
		return (
			<>
				<View style={styles.parallelogramRight} />
				<View style={styles.parallelogramInner} />
				<View style={styles.parallelogramLeft} />
			</>
		)
	}
	
	return (
		<>
			{!isSplashAnimationComplete ? (
				<Animated.View
                    style={{
                        ...styles.splashContainer,
                    }}
                >
					<View style={styles.progressBar}>
						<Animated.Image
							source={cartImage}
							style={{
								...styles.progressBarCart,
								transform: [
									{translateX: animationCart}
								]
							}}
						/>
						<View style={styles.progressBarLine}>
							<Animated.View style={{
								...styles.progressBarLineActive,
								transform: [
									{scaleX: animationLine},
									{translateX: 0.5}
								]
							}} />
						</View>
					</View>
					<ImageBackground
						source={image}
						resizeMode={'cover'}
						style={styles.cover}
					>
						<Animated.View
							style={{
								...styles.parallelogram,
								transform: [{ translateX: animationTop }]
							}}
						>
							<Parallelogram />
						</Animated.View>

						<Animated.View
							style={{
								...styles.parallelogram,
								transform: [{ translateX: animationMiddle }]
							}}
						>
							<Parallelogram />
						</Animated.View>

						<Animated.View
							style={{
								...styles.parallelogram,
								transform: [{ translateX: animationBottom }]
							}}
						>
							<Parallelogram />
						</Animated.View>
					</ImageBackground>
				</Animated.View>
			) : <Animated.View
                style={{
                    ...styles.content,
                    transform: [{ scale : animationContent }],
                    opacity: animationContent,
                    borderRadius: animationContent.interpolate({
                        inputRange: [0, 1],
                        outputRange: [1000, 0]
                    })
                }}
            >
                {children}
            </Animated.View>}
		</>
	);
}

const triangleStyle = {
	width: 0,
	height: 0,
	backgroundColor: "transparent",
	borderStyle: "solid",
	borderRightColor: "transparent",
	top: 0,
	position: 'absolute',
	borderTopColor: backgroundColor,
	borderRightWidth: parallelogramHeightSkew,
	borderTopWidth: parallelogramHeight,
};

const styles = StyleSheet.create({
	splashContainer: {
		flex: 1,
	},
	cover: {
		height: '100%',
		width: '100%'
	},
	parallelogram: {
		width: parallelogramWidth,
		height: parallelogramHeight,
	},
	parallelogramInner: {
		position: 'absolute',
		left: 0,
		top: 0,
		backgroundColor: backgroundColor,
		width: parallelogramWidth,
		height: parallelogramHeight,
	},
	parallelogramRight: {
		...triangleStyle,
		right: -parallelogramHeightSkew,
	},
	parallelogramLeft: {
		...triangleStyle,
		left: -parallelogramHeightSkew,
		transform: [{
			rotate: '180deg'
		}]
	},
	progressBar: {
		padding: padding,
		position: 'absolute',
		bottom: padding,
		width: '100%',
		elevation: 10,
        zIndex: 10,
	},
	progressBarCart: {
		width: cartWidth,
		height: 47,
		bottom: 8,
	},
	progressBarLine: {
		backgroundColor: '#C4C4C4',
		height: 4,
		overflow: 'hidden',
	},
	progressBarLineActive: {
		width: 1,
		height: 4,
		left: 0,
		position: 'absolute',
		backgroundColor: 'white',
	},
    content: {
        flex: 1,
        overflow: 'hidden'
    }
});
import React, { useContext } from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text,
    Vibration,
    Alert
} from 'react-native';
import { useDispatch } from 'react-redux';
import { Ionicons } from '@expo/vector-icons';
import { GeoContext } from '../containers/GeoWrapper';

import DefaultStyles, { DEFAULT_COLOR } from '../constance/styles';
import { PointListItem } from '../components/PointListItem';
import { RouteMap } from './RouteMap';
import { removeRoute } from '../redux/actions/myroutes';

export const MyRoutesItem = ({ navigation, item }) => {
    const dispatch = useDispatch();
    const userLocation = useContext(GeoContext);

    const removeRouteAction = () => {
        Vibration.vibrate();
        Alert.alert(
            'Удалить маршрут',
            'Вы уверены, что хотите удалить этот маршрут?',
            [
                {
                    text: 'Отмена',
                    style: 'cancel',
                },
                {
                    text: 'Да',
                    onPress: () => {
                        navigation.goBack()
                        dispatch(removeRoute(item.id));
                    }
                },
            ]
        );
    }

    return (
        <View style={styles.container}>
            <View style={styles.head}>
                <Text style={styles.header}>{item.title}</Text>
                <TouchableOpacity
                    style={styles.removeIcon}
                    onPress={removeRouteAction}
                >
                    <Ionicons
                        name={'trash-outline'}
                        color={DEFAULT_COLOR}
                        size={24}
                    />
                </TouchableOpacity>
            </View>

            {item.points.length > 0 ? (
                <RouteMap points={item.points} navigation={navigation} height={300} />
            ) : <></>}
            
            <View style={styles.list}>
                {item.points.map(point => 
                    <PointListItem
                        key={point.id}
                        item={point}
                        navigation={navigation}
                        userLocation={userLocation}
                    />
                )}
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 12,
        flexDirection: 'column'
    },
    head: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignContent: 'center',
        marginBottom: 24
    },
    header: {
        ...DefaultStyles.header,
    },
    subheader: {
        ...DefaultStyles.headerMiddle,
        marginBottom: 24
    },
    removeIcon: {

    },
    list: {
        marginHorizontal: -12,
        padding: 0
    },
})

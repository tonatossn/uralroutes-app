import React from 'react';
import { View, StyleSheet, SafeAreaView, TouchableOpacity, Text } from 'react-native';
import { useSelector } from 'react-redux';
import { Ionicons } from '@expo/vector-icons';
import Modal from "react-native-modal";

import DefaultStyles from '../constance/styles';
import { SocialNetworks } from './elements/SocialNetworks';


export const UserMenu = ({ navigation, menuShow, setMenuShow }) => {
    const data = useSelector(state => state.about.data);
    const userMenuItems = [
        {
            'title': 'Избранное',
            'route': 'Bookmark',
        },
        {
            'title': 'Мои маршруты',
            'route': 'MyRoutes',
        },
        {
            'title': 'Купленные туры',
            'route': 'BuyedRoutes',
        },
        {
            'title': 'Предложить место',
            'route': 'UserOfferPoint',
        },
        {
            'title': 'О нас',
            'route': 'Aboute',
        }
    ];

    const getContactByType = (data, type) => {
        typeTitleMap = {
            'email': 'эл. почта',
            'phone': 'телефон',
            'addr': 'адрес',
            'site': 'cайт',
        }
        if (typeTitleMap[type] == undefined) {
            return ''
        }
        contact = data?.contacts?.filter(item => item.title.toLowerCase().includes(typeTitleMap[type]))
        return contact.length ? contact[0]['value'] : '';
    }

    return (
        <Modal
            swipeDirection="down"
            hardwareAccelerated={true}
            isVisible={menuShow}
            onSwipeComplete={() => setMenuShow(false)}
            swipeThreshold={200}
            propagateSwipe
            style={{margin: 0}}
            animationIn={'slideInDown'}
            animationOut={'slideOutUp'}
        >
            <View style={styles.modalWrapper}>
                <View style={styles.modalContainer}>
                    <SafeAreaView style={{flex: 1}}>
                        <View style={styles.modalHeader}>
                            <Text style={styles.modalHeaderText}></Text>
                            <Ionicons
                                    name="close-circle-outline"
                                    color={'#000'}
                                    size={32}
                                    onPress={() => setMenuShow(false)}
                                />
                        </View>
                        <View style={styles.modalBody}>
                            <View style={styles.top}>
                                {userMenuItems.map((item, index) => (
                                    <TouchableOpacity
                                        onPress={() => {
                                            navigation.navigate(item.route);
                                            setMenuShow(false);
                                        }}
                                        style={styles.menuItem}
                                        key={item.route}
                                    >
                                        <Text style={styles.menuItemText}>{item.title}</Text>
                                        {index + 1 != userMenuItems.length ? (
                                            <View style={styles.splitter}></View>
                                        ) : <></>}
                                    </TouchableOpacity>
                                ))}
                            </View>
                            {data != null ? (
                                <View style={styles.bottom}>
                                    <View style={styles.social}>
                                        <SocialNetworks data={data.socials} />
                                    </View>
                                    <Text style={styles.contacts}>
                                        {getContactByType(data, 'addr')}{'\n'}
                                        {getContactByType(data, 'email')}{'\n'}
                                    </Text>
                                </View>
                            ) : <></>}
                        </View>
                    </SafeAreaView>
                </View>
            </View>
        </Modal>
    );
}

const styles = StyleSheet.create({
    modalWrapper: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        padding: 0,
        margin: 0,
    },
    modalContainer: {
        backgroundColor: '#fff',
        height: '100%',
        width: '100%',
        padding: 16,
    },
    modalHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignContent: 'center',
        alignItems: 'center',
        height: 50,
    },
    modalHeaderText: {
        ...DefaultStyles.headerSmall
    },
    modalBody: {
        flex: 1,
        justifyContent: 'space-between'
    },
    top: {},
    bottom: {},
    menuItem: {
        alignItems: 'center'
    },
    menuItemText: {
        paddingVertical: 32,
        ...DefaultStyles.font,
        fontSize: 24,
    },
    splitter: {
        borderBottomWidth: 1,
        borderColor: '#ddd',
        width: 120
    },
    social: {
        marginBottom: 32,
    },
    contacts: {
        ...DefaultStyles.font,
        fontSize: 14,
        textAlign: 'center',
        color: '#999'
    }
})
import React from 'react';
import { StyleSheet, Text, View} from 'react-native';

import DefaultStyles from '../constance/styles';
import { AppButton } from './elements/AppButton';


export const IndexOfferPoint = ({ navigation }) => {
    return (
        <View style={styles.container}>
		    <View>
                <Text style={styles.header}>
                    Знаете интересное место, которого еще нет в приложении?
                    Расскажите нам о нем - и мы расскажем о нем всем.
                </Text>
                <AppButton onPress={() => {
                        return navigation.navigate('UserOfferPoint')
                    }}
                    rightArrow={true}
                    title="Предложить место"
                />
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
	container: {
        marginBottom: 32,
        marginHorizontal: 16
	},
    header: {
        ...DefaultStyles.header,
        marginBottom: 24
    }
});

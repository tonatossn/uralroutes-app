import React from 'react';
import { View, StyleSheet, FlatList, Text } from 'react-native';

import DefaultStyles from '../constance/styles';
import { MyRoutesNotFound, MyRoutesListItem } from '../components/MyRoutesListItem';

export const MyRoutesList = ({ navigation, myroutes }) => {
    const notFoundList = [{id: 'notfound-404', title: 'Not found'}];
    
    const renderNotFound = ({ item }) => {
        return <MyRoutesNotFound />;
    }

    const renderItem = ({ item }) => {
        const itemOnPress = () => {
            navigation.goBack();
            return navigation.navigate('MyRoutes', {
                screen: 'MyRoutesItem',
                initial: false,
                params: {
                    id: item.id
                }
            });
        }
        return (
            <MyRoutesListItem
                item={item}
                navigation={navigation}
                onPress={itemOnPress}
            />
        )
    }
    
    return (
        <View style={styles.container}>
            <Text style={styles.header}>Мои маршруты</Text>
            <FlatList
                data={myroutes?.length > 0 ? myroutes : notFoundList}
                renderItem={myroutes?.length > 0 ? renderItem : renderNotFound}
                keyExtractor={(item, index) => item + index}
                style={styles.list}
                showsVerticalScrollIndicator={false}
            >
            </FlatList>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 16,
    },
    list: {
        flex: 1,
    },
    header: {
        ...DefaultStyles.header,
        marginBottom: 24
    },
})

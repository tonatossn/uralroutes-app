import config from '../constance/config';
import { fetchWrapper } from '.';


export const postFeedbackForm = async (data) => {
    return await fetchWrapper(config.API_ENDPOINT.FEEDBACK, {
        method: 'POST',
        data: {
            subject: 'point',
            text: data.text,
            email: data.email,
            images: []
        }
    });
}
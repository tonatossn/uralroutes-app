import config from '../constance/config';
import { pageToOffsetLimit, fetchWrapper } from '.';

export const getRouteItem = async (id) => {
    return await fetchWrapper(config.API_ENDPOINT.ROUTE, {itemId: id});
}

export const getRouteList = async (params = {}) => {
    return await fetchWrapper(config.API_ENDPOINT.ROUTE, {
        params: {
            ...pageToOffsetLimit(params.page ? params.page : 1),
            ...params
        }
    });
}

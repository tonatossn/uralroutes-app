import config from '../constance/config';

export const pageToOffsetLimit = (page = 1) => {
    return {
        limit: config.LIMIT_BY_PAGE,
        offset: config.LIMIT_BY_PAGE * page - config.LIMIT_BY_PAGE
    }
}

export const fetchWrapper = async (endpoint, {
    method = 'GET',
    itemId = null,
    params = null,
    data = null
} = {}) => {
    if (!endpoint) {
        return null;
    }

    const headers = {
        'Content-Type': 'application/json'
    };
    endpoint = `${config.API_BASE_URL}/${endpoint}/`;
    let url = itemId != null ? `${endpoint}${itemId}/` : `${endpoint}`;
    
    if (params != null) {
        const queryParams = new URLSearchParams(params).toString();
        url = `${url}?${queryParams}`;
    }

    console.log(`Fetch ${method} request to URL: ${url}`);
    
    let fetchParams = {
        method: method,
        headers: headers,
    }
    if (method === 'POST') {
        fetchParams.body = JSON.stringify(data)
    }
    try {
        const response = await fetch(url, fetchParams);
        return await response.json();  
    } catch (e) {
        console.error(`Error request: `, e);
        return e;
    }
}
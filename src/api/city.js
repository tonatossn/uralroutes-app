import config from '../constance/config';
import { pageToOffsetLimit, fetchWrapper } from '.';

export const getCityItem = async (id) => {
    return await fetchWrapper(config.API_ENDPOINT.CITY, {itemId: id});
}

export const getCityList = async (params = {}) => {
    return await fetchWrapper(config.API_ENDPOINT.CITY, {
        params: {
            ...pageToOffsetLimit(params.page ? params.page : 1),
            ...params
        }
    });
}

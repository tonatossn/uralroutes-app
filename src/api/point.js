import config from '../constance/config';
import { pageToOffsetLimit, fetchWrapper } from '.';

export const getPointItem = async (id) => {
    return await fetchWrapper(config.API_ENDPOINT.POINT, {itemId: id});
}

export const getPointList = async (params = {}) => {
    return await fetchWrapper(config.API_ENDPOINT.POINT, {
            params: {
                ...pageToOffsetLimit(params.page ? params.page : 1),
                ...params
            }
        }
    );
}

export const getCategoriesList = async (params) => {
    return await fetchWrapper(config.API_ENDPOINT.CATEGORIES, {
        params: params
    });
}

import config from '../constance/config';
import { fetchWrapper, pageToOffsetLimit } from '.';

export const postTicket = async (data) => {
    return await fetchWrapper(config.API_ENDPOINT.TICKET, {
        method: 'POST',
        data: data
    });
}

export const getTicketList = async (params) => {
    return await fetchWrapper(config.API_ENDPOINT.TICKET, {
            params: {
                ...pageToOffsetLimit(params ? params.page : 1),
                ...params
            }
        }
    );
}

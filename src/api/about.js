import config from '../constance/config';
import { fetchWrapper } from '.';

export const getAbout = async () => {
    return await fetchWrapper(config.API_ENDPOINT.ABOUTE);
}

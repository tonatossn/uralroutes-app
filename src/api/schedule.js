import config from '../constance/config';
import { fetchWrapper, pageToOffsetLimit } from '.';

export const getScheduleList = async (params) => {
    return await fetchWrapper(config.API_ENDPOINT.SCHEDULE, {
            params: {
                ...pageToOffsetLimit(params ? params.page : 1),
                ...params
            }
        }
    );
}

import config from '../constance/config';


export const getDirectionRequest = async (points = null) => {
    if (!points || points.length < 2) {
        return null;
    }

    const coordStr = points.map((point) => [
        point.geodata.coordinates.join(',')
    ]).join(';');

    const url = config.API_ENDPOINT.MAPBOX_DIRECTION
                        .replace('{0}', 'driving')
                        .replace('{1}', coordStr)
                        .replace('{2}', config.MAPBOX_ACCESS_TOKEN);

    const query = await fetch(url, { method: 'GET' });
    console.log(`Fetch request to URL: ${url}`);
    
    const json = await query.json();
    
    return {
        coordinates: json.routes[0].geometry.coordinates,
        meta: {
            distance: json.routes[0].distance,
            duration: json.routes[0].duration,
        }
    };
}
